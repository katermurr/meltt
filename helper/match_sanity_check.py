# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 20:47:32 2020

@author: benedikt
"""

import pandas as pd

df = pd.read_csv("../data/match_info/S100_T0_ssa-integration_match-info.csv", dtype=object)

total_count = df.shape[0]
print('total count: ' + str(total_count))

count = df[df['prec_match'] == 'exact town, area, city, village'].shape[0]
print('exact town, area, city, village: ' + str(count) + " (" + str(round(count/total_count*100,1)) + "%)")

count = df[df['actor_match'] == 'armed group'].shape[0]
print('armed group: ' + str(count) + " (" + str(round(count/total_count*100,1)) + "%)")

count = df[df['event_match'] == 'Violent Attack'].shape[0]
print('Violent Attack: ' + str(count) + " (" + str(round(count/total_count*100,1)) + "%)")