# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 13:02:22 2020

@author: benedikt
"""
# Remove all columns from the data sets that are not necessary for the visualization
import pandas as pd 

#%%
df_events = pd.read_csv("../data/all_events.csv", dtype=object)

remove_headers = [colheader for colheader in df_events.columns if "_tax" in colheader]

df_events_cropped = df_events.drop(columns=remove_headers)

df_events_cropped.to_csv("../data/all_events_cropped.csv", index=False)

#%%
df_matches = pd.read_csv("../data/all_matches.csv", dtype=object)

remove_headers = [colheader for colheader in df_matches.columns if "_match" in colheader]

df_matches_cropped = df_matches.drop(columns=remove_headers)

df_matches_cropped.to_csv("../data/all_matches_cropped.csv", index=False)