#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 12:38:07 2020

@author: bmayer
"""
import numpy as np
from decimal import Decimal

def signum(number):
    if number > 0: return 1
    elif number < 0: return -1
    else: return 0

class Tree(object):
    "Generic tree node."
    def __init__(self, name='root', weight=0, neighbors=None):
        self.name = name
        self.neighbors = []
        self.weight = weight
        self.visited_rendering = False
        self.visited = False
        # When iterating over parts of the tree inside an overall function, a second visited value is necessary
        self.visited_tmp = False
        if neighbors is not None:
            for neighbor in neighbors:
                neighbor.add_neighbor(self)
                self.add_neighbor(neighbor)
                
    def __repr__(self):
        return self.name
    
    def add_neighbor(self, node):
        assert isinstance(node, Tree)        
        # Check if already denoted as neighbor
        self.neighbors.append(node)
        
    def pre(self):
        print(self.name)
        self.visited = True
        for neighbor in self.neighbors:
            if not neighbor.visited:
                neighbor.pre()
        self.visited = False
        
    def render(self, depth=0):
        depth_string = ""
        # Add as many branch symbols as indicated by depth
        for _ in range(depth):
            depth_string += "-"
        print(depth_string + self.name + "({})".format(self.weight))
        self.visited_rendering = True
        for neighbor in self.neighbors:
            if not neighbor.visited_rendering:
                neighbor.render(depth+1)
        self.visited_rendering = False
        
    def get_weight(self, name):
        return_weight = None
        if self.name == name:
            return_weight = self.weight
        else:
            self.visited = True
            for neighbor in self.neighbors:
                if not neighbor.visited:
                    neighbor_weight = neighbor.get_weight(name)
                    if neighbor_weight is not None:
                        self.visited = False
                        return neighbor_weight
        self.visited = False
        return return_weight
    
    def equalize(self):
        total_cost = 0
        print("Equalizing", self.name, "with mass", self.weight)
        self.visited = True
        if self.weight != 0:
            total_cost += self.push_mass()
            print("Graph after equalizing", self.name, ":")
            graph.render()
        for neighbor in self.neighbors:
            if not neighbor.visited:
                total_cost += neighbor.equalize()
        self.visited = False
        return total_cost
        
    def push_mass(self):
        absorption_cost = 0
        self.visited_tmp = True
        for neighbor in self.neighbors: # Also iterate over same-level-neighbors here, maybe get them from an extra list/function/whatever
            # Aber dann müsste ich zuerst alle außen rum auf besucht setzen, bevor ich bei einzelnen in die Tiefe gehe.
            # Ansonsten lege ich moeglicherweise einen Weg zurueck, der weiter als noetig ist
            if not neighbor.visited_tmp and self.weight != 0: # Is the first question even necessary here?
                # How much mass is missing from the subtree induced by the neighbor (special treatment when considering nodes on same level as neighbors)
                missing_mass = neighbor.missing_mass()
                print("Missing mass of branch {}-{}-...: {}".format(self.name, neighbor.name, missing_mass))
                if signum(missing_mass) == - signum(self.weight):     
                    # The subtree can absorb at least some of the node's weight  
                    """
                    weight_sum = self.weight + missing_mass
                    if signum(self.weight) == - signum(weight_sum):
                        # The weight to absorb exceeded the node's weight
                        # Pass on -weight_sum
                        # ...
                    else:
                        # The weight can be fully absorbed by the node's weight
                        self.weight = weight_sum
                    """
                    if np.abs(missing_mass) > np.abs(self.weight):
                        # The subtree can absorb all of the node's weight
                        absorption_cost += neighbor.absorb(dist_curr = 0, weight_curr = self.weight)
                        self.weight = 0
                    else:
                        # The subtree can absorb only part of the node's weight
                        absorption_cost += neighbor.absorb(dist_curr = 0, weight_curr = -missing_mass)
                        self.weight += missing_mass
        self.visited_tmp = False
        print("Absorption cost", absorption_cost)
        return absorption_cost
    
    def missing_mass(self):
        missing_mass = self.weight
        self.visited_tmp = True
        for neighbor in self.neighbors:
            if not neighbor.visited_tmp:
                missing_mass += neighbor.missing_mass()
        self.visited_tmp = False
        return missing_mass
                
                
    def absorb(self, dist_curr, weight_curr):
        dist_curr += 1
        self.visited_tmp = True
        if signum(weight_curr) == - signum(self.weight):
            # The current node can take some of the weight that is to be equalized
            if np.abs(weight_curr) <= np.abs(self.weight):
                # The current node can take all of the weight that is to be equalized
                # Absorb weight
                self.weight += weight_curr
                # Return absorption cost
                self.visited_tmp = False
                return np.abs(weight_curr * dist_curr)
            else:
                # The current node can only take a part of the weight that is to be equalized
                absorption_cost = np.abs(self.weight * dist_curr)
                # Pass the exceeding equalization weight on to the neighbors
                weight_curr = weight_curr + self.weight
                for neighbor in self.neighbors:
                    if not neighbor.visited_tmp:
                        # How much mass is missing from the subtree induced by the neighbor (special treatment when considering nodes on same level as neighbors)
                        missing_mass = neighbor.missing_mass()
                        if signum(missing_mass) == - signum(weight_curr):     
                            # The subtree can absorb at least some of the node's weight
                            if np.abs(missing_mass) > np.abs(weight_curr):
                                # The subtree can absorb all of the node's weight
                                absorption_cost += neighbor.absorb(dist_curr, weight_curr)
                                weight_curr = 0
                            else:
                                # The subtree can absorb only part of the node's weight
                                absorption_cost += neighbor.absorb(dist_curr, -missing_mass)
                                weight_curr += missing_mass
                # Reduce the weight of the current node to 0 since it was fully used to absorb part of the weight to be equalized
                self.weight = 0
                self.visited_tmp = False
                return absorption_cost
        else:
            # The current node can take none of the weight that is to be equalized
            absorption_cost = 0
            # Pass the full equalization weight on to the neighbors
            for neighbor in self.neighbors:
                if not neighbor.visited_tmp and weight_curr != 0:
                    # How much mass is missing from the subtree induced by the neighbor (special treatment when considering nodes on same level as neighbors)
                    missing_mass = neighbor.missing_mass()
                    if signum(missing_mass) == - signum(weight_curr):     
                        # The subtree can absorb at least some of the node's weight
                        if np.abs(missing_mass) > np.abs(weight_curr):
                            # The subtree can absorb all of the node's weight
                            absorption_cost += neighbor.absorb(dist_curr, weight_curr)
                            weight_curr = 0
                        else:
                            # The subtree can absorb only part of the node's weight
                            absorption_cost += neighbor.absorb(dist_curr, -missing_mass)
                            weight_curr += missing_mass
            self.visited_tmp = False
            return absorption_cost
        
                
                
"""
graph = Tree('a', 0, 
             [Tree('b', 2, 
                   [Tree('b1', 3), 
                    Tree('b2', -3)]), 
              Tree('c', 2, 
                   [Tree('c1', 0), 
                    Tree('c2', -1), 
                    Tree('c2', -3)])])
"""

graph = Tree('a', Decimal('0.0'), 
             [Tree('b', Decimal('0.2'), 
                   [Tree('b1', Decimal('0.3')), 
                    Tree('b2', Decimal('0.2')), 
                    Tree('b3', Decimal('-0.1'), [Tree('b31', Decimal('-0.4'))])]), 
              Tree('c', Decimal('0.2'), 
                   [Tree('c1', Decimal('0.0')), 
                    Tree('c2', Decimal('-0.1')), 
                    Tree('c2', Decimal('-0.3'))])])
"""
graph = Tree('a', 0, 
             [Tree('b', 1, 
                   [Tree('b1', 0), 
                    Tree('b2', -1)]), 
              Tree('c', 0, 
                   [Tree('c1', 0), 
                    Tree('c2', 0)])])
"""

graph.render()
#name = "b2"
#print("weight of {} is".format(name), graph.get_weight("b2"))

equalization_cost = graph.equalize()
print("Final graph after equalization cost of", equalization_cost)
graph.render()