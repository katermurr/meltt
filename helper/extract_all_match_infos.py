# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

"""
/home/bmayer/Documents/data/meltt/data
S0_T0_ssa-integration.csv
"""
import pandas as pd

# Goal: Create complete list of match info

s_all = [0, 5, 10, 25, 50, 100, 250]
t_all = [0, 1, 2]

# Load first match_info and later attach remaining entries to it
df_for_colheaders = pd.read_csv("../match_info_data/S{}_T{}_ssa-integration_match-info.csv".format(0, 0), dtype=object)
colheaders = list(df_for_colheaders.columns)
colheaders.extend(["parameters", "actor_path", "event_path", "precision_path", "date", "latitude", "longitude", "size"])
df_all = pd.DataFrame(columns = colheaders)
#To fill up rest after index fail
#df_all = pd.read_csv("../data/all_matches.csv", dtype=object)
df_events = pd.read_csv("../data/all_events.csv", dtype=object)

for s in s_all:
    for t in t_all:
        print("s", s, "t", t)
        df = pd.read_csv("../match_info_data/S{}_T{}_ssa-integration_match-info.csv".format(s, t), dtype=object)
        for idx, row in df.iterrows():
            #if idx > 23365:
            row["parameters"] = "s"+str(s)+"t"+str(t)
            if row['event'] == "1e+05":
                row['event'] = '100000'
            # Get row of primary events from all events
            prime_event_row = df_events.loc[(df_events['dataset'] == row['data']) & (df_events['event'] == row['event'])]
            # Extract txonomy paths to the levels on which the match was identified
            path = ""
            for colname in ["event"]:
                for lvl in range(4, int(row[colname+"_match_level"])-1, -1):
                    # strip for trailing whitespaces, values[0] for actual content
                    path = path + prime_event_row[colname+"_level_"+str(lvl)].str.strip().values[0] + "#"
                row[colname+"_path"] = path
            # Special case for actor
            path = ""
            for lvl in range(3, int(row["actor_match_level"])-1, -1):
                path = path + prime_event_row["actor_level_"+str(lvl)].str.strip().values[0] + "#"
            row["actor_path"] = path
            # Special case for precision
            path = ""
            for lvl in range(4, int(row["prec_match_level"])-1, -1):
                path = path + prime_event_row["precision_level_"+str(lvl)].str.strip().values[0] + "#"
            row["precision_path"] = path
            
            # Add some info from the primary event as additional description of the match
            row["date"] = prime_event_row["date"].values[0]
            row["latitude"] = prime_event_row["latitude"].values[0]
            row["longitude"] = prime_event_row["longitude"].values[0]
            row["size"] = df.loc[(df['data'] == row['data']) & (df['event'] == row['event'])].shape[0] + 1
            #tbd have to add that correctly, initiate the first one all the way up correctly
            df_all = df_all.append(row, ignore_index=True)
            if idx%10000==0: print(idx)
        df_all.to_csv("all_matches.csv", index=False) 
            
df_all.to_csv("all_matches.csv", index=False) 
        