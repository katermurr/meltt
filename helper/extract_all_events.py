# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

"""
/home/bmayer/Documents/data/meltt/data
S0_T0_ssa-integration.csv
"""
import pandas as pd

# Goal: Create complete list of events

# Load merged file as new dataframe
df = pd.read_csv("S0_T0_ssa-integration.csv", dtype=object)
#df = pd.DataFrame(columns = cols)
#df_old = pd.read_csv("/home/bmayer/Documents/data/meltt/data/S0_T0_ssa-integration.csv")
dupf = pd.read_csv("S0_T0_ssa-integration_duplicates.csv", dtype=object)

# Iterate over duplicates file
dsets = ["A", "G", "GT", "S"]
cols = list(df.columns)

# For each row, for each dataset, go over all informations from the corresponding
# dataset and add them to the complete list of events, if the crsp combination of 
# dataset and event(number) do not exist yet
for idx, row in dupf.iterrows():
    # Iterate over the datasets
    for dset in dsets:
        # Check if dataset participates at match
        #aentry = row[dset+"_dataset"]
        #bis_not_contained = row[dset+"_dataset"] == "0"
        is_contained = row[dset+"_dataset"] != "0"
        if (is_contained):
            # Check if entry already exists in df
            if not ((df["dataset"] == dset) & (df['event'] == row[dset+"_event"])).any():
                # Extract filtered row
                row_dset = pd.DataFrame(row[[dset +"_"+ s for s in cols]]).T
                # Rename columns to match df
                row_dset.columns = cols
                # Set the correct dataset (until here only a number)
                row_dset["dataset"] = dset
                # Append
                df = df.append(row_dset, ignore_index=True)
                
df.to_csv("all_events.csv", index=False)
#df_small = df[df.index % 20000 == 0]
#df_small.to_csv("/home/bmayer/Documents/data/meltt/data/all_20000th_events.csv")