#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 22 16:46:25 2019

@author: bmayer
"""

from math import pi
from matplotlib import pyplot as plt
import numpy as np


x = np.arange(0, 20000)

sig = 1.8

#y = 3/(x*0.00001) * (1/(sig*np.sqrt(2*pi))) * np.exp(-np.power(np.log((x*0.00001)),2)/(2*np.power(sig,2))) + 0.2


k = 20000

# * 1/k - k
#y = ( np.arctan(-x/(k/10) + 1)  + 1.6 ) *1

#y = 2.5 * (1.1 - (np.power(x, 1/3))/np.power(200000, 1/3))


x = [70, 388, 878, 1110, 5300, 8900, 14000, 27000]
y = [0.3, 0.2, 0.1, 0.08, 0.03, 0.01, 0.007, 0.006]

c = 0.001
m = 20000
b = np.array(2500)
k = -1.5

x_hat = np.arange(0, 27000, 100)
y_hat = np.minimum(c + m * np.float_power(x_hat+b, k), np.full((len(x_hat)), 30))
y_hat_c = np.minimum(+0.05 + m * np.float_power(x_hat+b, k), np.full((len(x_hat)), 30))
y_hat_m = np.minimum(c + 30000 * np.float_power(x_hat+b, k), np.full((len(x_hat)), 30))
y_hat_b = np.minimum(c + m * np.float_power(x_hat+np.array(3500), k), np.full((len(x_hat)), 30))
y_hat_k = np.minimum(c + m * np.float_power(x_hat+b, -1.6), np.full((len(x_hat)), 30))

print("c:", c, ", m:", m, ", b:", b, ", k:", k)

fig = plt.figure(figsize=(8,5))
plt.plot(x,y, label='measured')
#plt.plot(x_hat,y_hat_c, "--", label='c = 5*10^(-2)')
#plt.plot(x_hat,y_hat_m, "--", color="grey", label='m = 3*10^4')
#plt.plot(x_hat,y_hat_b, "--", label='b = 3.5*10^3')
#plt.plot(x_hat,y_hat_k, "--", label='k = 1.6')
plt.plot(x_hat,y_hat, color="orange", label='fit')      
plt.xlabel('Number of Events', fontsize=14)
plt.xticks(fontsize=14)
plt.ylabel('alpha', fontsize=14)
plt.yticks(fontsize=14)
plt.title('Fitting of Alpha Function', fontsize=16)
plt.legend()
fig.savefig('fit.jpg')

'''
c: 0 , m: 2000000 , b: 2500 , k: -1.5 nicht ganz hoch genug bei den min werten
c: 0 , m: 2000000 , b: 1000 , k: -1.6 recht tief bei den niedrigen tausendern
'''