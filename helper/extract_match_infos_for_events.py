# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

"""
/home/bmayer/Documents/data/meltt/data
S0_T0_ssa-integration.csv
"""
import pandas as pd

# Goal: Create complete list of events

# Load merged file as new dataframe
df_events = pd.read_csv("../data/all_events.csv", dtype=object)
df_matches = pd.read_csv("../data/all_matches.csv", dtype=object)
df_out = pd.DataFrame()

s_all = [0, 5, 10, 25, 50, 100, 250]
t_all = [0, 1, 2]

for s in s_all:
    for t in t_all:
        match_info = []
        paracomb = "s"+str(s)+"t"+str(t)
        print(paracomb)
        df_paracomb = df_matches[df_matches["parameters"] == paracomb]
        match_ids = []
        for idx, row in df_paracomb.iterrows():
            match_ids.append(row["dataset"] + row["event"])
            match_ids.append(row["duplicate_dataset"] + row["duplicate_event"])
        for idx, row in df_events.iterrows():
            match_info.append(int((row["dataset"]+row["event"]) in match_ids))
            if idx%50000==0: print(idx)
        df_out[paracomb] = match_info
        df_out.to_csv("../data/match_info.csv", index=False)