#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 15:16:49 2019

@author: bmayer
"""

'''
Goal: Collect the match quality information from all different combinations of ds and dt
'''

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
#import random


s_all = [0, 5, 10, 25, 50, 100, 250]
t_all = [0, 1, 2]
nr_bins = 10
#bandwidth = 1/nr_bins
#x = np.arange(0, 1+bandwidth, bandwidth)

df_all = pd.DataFrame(columns = ["s", "t", "x", "y", "count"])

for s in s_all:
    for t in t_all:
        df = pd.read_csv("../match_info_data/S{}_T{}_ssa-integration_match-info.csv".format(s, t))
        # Calculate histogram with 100 bins
        h = plt.hist(df["score"], bins=nr_bins, range=(0,1))
        count = df["score"].size
        x = h[1][:nr_bins]
        y = h[0]/count
        #y = np.random.rand(x.size)
        #y = y / sum(y)
        #amount = [random.random()] * x.size
        appen_df = pd.DataFrame({"s": [s]*nr_bins, "t": [t]*nr_bins, "x": x, "y": y, "count": [count]*nr_bins}) #, "amount": amount
        df_all = df_all.append(appen_df, ignore_index=True)
        
df_all.to_csv("../data/para_data.csv", index=False)