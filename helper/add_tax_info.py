#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 16:39:22 2020

@author: bmayer
"""
import pandas as pd
import numpy as np


# Run over event list and note for every event which branches of the event taxonomy it falls into


# Load all events as dataframe
df = pd.read_csv("../data/all_events.csv")

#%%
# Event taxonomy - contains "text" in the col headers
tax = pd.read_csv("../data/event_taxonomy.csv")

dset_dict = {
        "A": "ACLED",
        "G": "GED",
        "GT": "GTD",
        "S": "SCAD"}

for lvl in range(1,5):
    #lvl=2
    append_list = []
    for i in range(len(df)):
    #i = 0
        # Get index of the row where the dataset of the event matches and the base category
        #dset_dict[df["dataset"][i]]
        append_value = tax["Level_{}".format(lvl)][np.logical_and(df["event_tax"][i]==tax["base.categories"].str.strip(), dset_dict[df["dataset"][i]]==tax["data.source"].str.strip())].values[0]
        assert type(append_value)==str
        append_list.append(append_value)
        if i%10000==0: print(i)
    df["event_level_{}".format(lvl)] = append_list
#append_list.append()
    
df.to_csv("../data/all_events.csv", index=False)

#%%
import pandas as pd
import numpy as np
df = pd.read_csv("../data/all_events.csv")
print("precision")
# Precision taxonomy
tax = pd.read_csv("../data/precision_taxonomy.csv")

dset_dict = {
        "A": "ACLED",
        "G": "GED",
        "GT": "GTD",
        "S": "SCAD"}

for lvl in range(1,5):
    #lvl=2
    append_list = []
    for i in range(len(df)):
    #i = 0
        # Get index of the row where the dataset of the event matches and the base category
        #dset_dict[df["dataset"][i]]
        append_value = tax["Level_{}".format(lvl)][np.logical_and(df["prec_tax"][i]==tax["base.categories"], dset_dict[df["dataset"][i]]==tax["data.source"].str.strip())].values[0]
        assert type(append_value)==str
        append_list.append(append_value)
        if i%10000==0: print(i)
    df["precision_level_{}".format(lvl)] = append_list
#append_list.append()
    
df.to_csv("../data/all_events.csv", index=False)

#%%
import pandas as pd
import numpy as np
df = pd.read_csv("../data/all_events.csv")
print("actor")
# Actor taxonomy - goes only till level 3
tax = pd.read_csv("../data/actor_taxonomy.csv")

dset_dict = {
        "A": "ACLED",
        "G": "GED",
        "GT": "GTD",
        "S": "SCAD"}

dset_dict = {
        "A": "ACLED",
        "G": "GED",
        "GT": "GTD",
        "S": "SCAD"}

for lvl in range(1,4):
    #lvl=2
    append_list = []
    for i in range(len(df)):
    #i = 0
        # Get index of the row where the dataset of the event matches and the base category
        #dset_dict[df["dataset"][i]]
        append_value = tax["Level_{}".format(lvl)][np.logical_and(df["actor_tax"][i]==tax["base.categories"].str.strip(), dset_dict[df["dataset"][i]]==tax["data.source"].str.strip())].values[0]
        assert type(append_value)==str
        append_list.append(append_value)
        if i%10000==0: print(i)
    df["actor_level_{}".format(lvl)] = append_list
#append_list.append()
    
df.to_csv("../data/all_events.csv", index=False)