function backgroundMap() {
  /*
  This function returns the object for drawing the background map of the TempMap component
  */
  var o, fn, projection, path;

  o = {
    w: 0,
    h: 0,
    radialPadding: 60,
    fill: "rgb(50, 50, 50)",
    highfill: "rgb(100, 100, 100)",
    stroke: "black",
    strokewidth: 1,
    highstrokewidth: 2,
    scale: 200,
    centerLon: 0,
    centerLat: 0,
    filterfunctions: null
  }

  fn = {
    // Sum over all values of an array
    sumArr: arr => arr.reduce((a,b)=> a+b),
    // Sum over all values of an object
    sumObj: obj => sumArr(Object.values(obj))
  }

  // Define the rendering
  function chart(selection) {
    selection.each(function(data, idx) {

      if (!data) {return;}

      // Select the svg element
      var svg = d3.select(this);
      o.w = svg.attr("width");
      o.h = svg.attr("height");

      // Define projection path generator, using an appropriate projection
      // To center the projection correctly, calculate the centers here
      projection = d3.geoMercator()
            .translate([o.w/2, o.h/2])
            .center([o.centerLon,o.centerLat])
            .scale(o.scale);

      path = d3.geoPath()
             .projection(projection);

      // Bind data and create one path per GeoJSON feature
      svg.append("g")
          .attr("id", "map-boundary")
          .selectAll("path")
          .data(data)
          .join("path")
          .attr("class", "backgroundmap-path")
          .attr("clip-path","url(#backgroundmap-clip)")
          .attr("d", path)
          .attr("fill", o.fill)
          .attr("stroke", o.stroke)
          .attr("stroke-width", o["stroke-width"]);
    });
  }

  chart.highlight = function(d) {
    // @desc: Highlight the selected country
    d3.select(d)
      .attr("fill", o.highfill)
      .attr("stroke-width", o.highstrokewidth);
  }

  chart.lowlight = function(d) {
    // @desc: Remove highlight from selected country
    d3.select(d)
      .attr("fill", o.fill)
      .attr("stroke-width", o.strokewidth);
  }

  chart.brush = function(centerLon, centerLat, scale) {
    // @desc: Update displayed region of map to regionn indicated by brushing region
    o.centerLon = centerLon;
    o.centerLat = centerLat;
    o.scale = scale;
    // Update projection and path
    projection.center([o.centerLon,o.centerLat])
          .scale(o.scale);
    path.projection(projection);
    // Update visuals
    d3.selectAll(".backgroundmap-path").attr("d", path);
  }

  chart.getLonLat = function(x, y) {
    // @desc: For a given position on the screen, return the projected position on the map
    return projection.invert([x, y]);
  }

  // Allow external setting and getting of the options 'o'
  chart.w = function(value) {
      if (!arguments.length) return o.w;
      o.w = value;
      return chart;
  };
  chart.h = function(value) {
      if (!arguments.length) return o.h;
      o.h = value;
      return chart;
  };
  chart.radialPadding = function(value) {
      if (!arguments.length) return o.radialPadding;
      o.radialPadding = value;
      return chart;
  };
  chart.fill = function(value) {
      if (!arguments.length) return o.fill;
      o.fill = value;
      return chart;
  };
  chart.stroke = function(value) {
      if (!arguments.length) return o.stroke;
      o.stroke = value;
      return chart;
  };
  chart.strokewidth = function(value) {
      if (!arguments.length) return o.strokewidth;
      o.strokewidth = value;
      return chart;
  };
  chart.scale = function(value) {
      if (!arguments.length) return o.scale;
      o.scale = value;
      return chart;
  };
  chart.centerLon = function(value) {
      if (!arguments.length) return o.centerLon;
      o.centerLon = value;
      return chart;
  };
  chart.centerLat = function(value) {
      if (!arguments.length) return o.centerLat;
      o.centerLat = value;
      return chart;
  };
  chart.filterfunctions = function(value) {
      if (!arguments.length) return o.filterfunctions;
      o.filterfunctions = value;
      return chart;
  };

  return chart;
}
