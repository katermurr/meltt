function filterBlur(selection) {
	// https://jsfiddle.net/w8r/yx0y1jLc/
	var filter = selection.append("defs")
		.append("filter")
			.attr("id", "innershadow")
			.attr("x0", "-50%")
			.attr("y0", "-50%")
			.attr("width", "200%")
			.attr("height", "200%")
	filter.append("feGaussianBlur")
		.attr("in", "SourceAlpha")
		.attr("stdDeviation", 3)
		.attr("result", "blur")
	filter.append("feOffset")
		.attr("dy", 2)
		.attr("dx", 3)
	filter.append("feComposite")
		.attr("in2", "SourceAlpha")
		.attr("operator", "arithmetic")
		.attr("k2", -1)
		.attr("k3", 1)
		.attr("result", "shadowDiff")
	filter.append("feFlood")
		.attr("flood-color", "#444444")
		.attr("flood-opacity", 0.95)
	filter.append("feComposite")
		.attr("in2", "shadowDiff")
		.attr("operator", "in")
	filter.append("feComposite")
		.attr("in2", "SourceGraphic")
		.attr("operator", "over")
		.attr("result", "firstfilter")
	filter.append("feGaussianBlur")
		.attr("in", "firstfilter")
		.attr("stdDeviation", 3)
		.attr("result", "blur2")
	filter.append("feOffset")
		.attr("dy", -2)
		.attr("dx", -3)
	filter.append("feComposite")
		.attr("in2", "firstfilter")
		.attr("operator", "arithmetic")
		.attr("k2", -1)
		.attr("k3", 1)
		.attr("result", "shadowDiff")
	filter.append("feFlood")
		.attr("flood-color", "#444444")
		.attr("flood-opacity", 0.95)
	filter.append("feComposite")
		.attr("in2", "shadowDiff")
		.attr("operator", "in")
	filter.append("feComposite")
		.attr("in2", "firstfilter")
		.attr("operator", "over")
}
/*
function buttonDeactivate(selection) {
	var strokeColor = "#a6a6a6";
	selection
		//.attr("filter", "none")
		.attr("fill", d3.select("body").attr("bgcolor"))
		.attr("stroke", strokeColor)
		.classed("inactive", true)
		.classed("active", false);
}

function buttonActivate(selection) {
	var strokeColor = "#a6a6a6";
	selection
		//.attr("filter", "url(#innershadow)")
		.attr("fill", strokeColor)
		.attr("stroke", strokeColor)
		.classed("active", true)
		.classed("inactive", false);
}
*/
