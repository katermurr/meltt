/*
The overall structure is inspired by: https://blog.beezwax.net/2017/11/08/large-d3-js-application-development/#inside
The canvas rendering is inspired by: https://www.freecodecamp.org/news/d3-and-canvas-in-3-steps-8505c8b27444/
*/

function gradientLines() {
  var o, fn, minDate, maxDate, tScale; // options, (helper)functions

  o = {
    w: 0,
    h: 0,
    radialPadding: 60,
    colors: {"A": "rgb(75,174,121)", "G": "rgb(0,162,219)", "GT": "rgb(255,152,35)", "S": "rgb(183,41,177)"},
    colors_end: {"A": "rgba(75,174,121,0.1)", "G": "rgba(0,162,219,0.1)", "GT": "rgba(255,152,35,0.1)", "S": "rgba(183,41,177,0.1)"},
    colors_mid: {"A": "rgba(75,174,121,0.01)", "G": "rgba(0,162,219,0.01)", "GT": "rgba(255,152,35,0.01)", "S": "rgba(183,41,177,0.01)"},
    opacityMultiplyer: 1,
    // Length of the lines
    linelength: 10,
    // The base values of the different stop values of the gradients
    midOpacityBase: 0.01,
    endOpacityBase: 0.1,
    // Length of line close to the actual geographic position
    innerLineLength: 0.03,
    // Length of line close to the temporal position
    outerLineLength: 0.75,
    paddingOuter: 0,
    centerLon: 0,
    centerLat: 0,
    scale: 200,
    context: null,
    custom: null
  }

  var inPolygon = function(polygon, point) {
    // Check if point inside of the main polygon
    var pointContained = d3.polygonContains(polygon[0],[point[0],point[1]]);
    if (polygon.length > 1) {
      // Case: There are holes in the polygon
      for (var i = 1; i < polygon.length; i++) {
        // If point lies within a hole, it is not contained in main polygon
        if (d3.polygonContains(polygon[i][0],[point[0],point[1]]))
        pointContained = false;
      }
    }
    return pointContained;
  };

  // For a given position on a line, return the corresponding rgba value based
  // on a plain rgb value
  var makeAlpha = function(rgbVal, position) {
    // tbd no check for whether valid rgb code is fed.
    var alpha;
    // Determine where on the line the alpha value should be calculated
    if (position == "mid") alpha = o.midOpacityBase;
    else if (position == "end") alpha = o.endOpacityBase;
    else {
      console.log("Invalid position on line: " + position);
      alpha = 0;
    }
    // Adjust according to the opacityMultiplyer
    alpha = alpha * o.opacityMultiplyer;
    // Remove bracket in the end of rgb(X,Y,Z) and rgb in beginning
    var rgbaVal = rgbVal.substring(3, rgbVal.length - 1);
    // Append the alpha value after comma and close off with bracket
    rgbaVal = "rgba" + rgbaVal + "," + alpha + ")";

    return rgbaVal;
  }

  var highlightLines = function(selectedLines) {
    /*
    // Extract all events associated with the selected lines
    // -> necessary to select the gradients referenced by the
    // 		selected lines via url
    // 			-> gradients are identifiable by their id: dataset+event
    var eventList = [];
    for(var i=0; i<selectedLines.length; i++) {
      eventList[i] = selectedLines[i].dataset+selectedLines[i].event;
    }
    // Get all gradients corresponding to the lines inside the polygon data
    // to increase their opacity
    var grads = d3.selectAll("linearGradient")
      .filter(function() {return eventList.includes(d3.select(this).attr("id"))});
    // Increase opacity at the ends of the lines/gradients resp.
    grads.selectAll(".gradientEnd")
      .attr("stop-opacity", 3*o.endOpacity);
    // Increase opacity the middle of the lines/gradients resp.
    grads.selectAll(".gradientMid")
      .attr("stop-opacity", 3*o.midOpacity);
    // Get all gradients NOT corresponding to the lines inside the polygon data
    // to reduce their opacity
    grads = d3.selectAll("linearGradient")
      .filter(function() {return !eventList.includes(d3.select(this).attr("id"))});
    // Reduce opacity at the ends of the lines/gradients resp.
    grads.selectAll(".gradientEnd")
      .attr("stop-opacity", 0);
    // Reduce opacity the middle of the lines/gradients resp.
    grads.selectAll(".gradientMid")
      .attr("stop-opacity", 0);
    */
  };

  /*
  Append data to custom (non-DOM) elements. Called before chart rendering.
  Now this is actually not necessary for static canvas rendering. In that case,
  you could just follow the instructions here
  (https://medium.com/@xoor/implementing-charts-that-scale-with-d3-and-canvas-3e14bbe70f37)
  where you don't bind the data to any elements but instead just iterate through
  the data array and call the draw() function for each element.
  What the present solution allows in contrast (while trading off for a little higher
  complexity) is to use the d3 update/merge and exit pattern in combination
  with transitions on the custom, non-DOM elements, that each have a piece of
  data appended to them.
  */
  chart.databind = function(data) {
    // First date in table
    minDate = d3.min(data, function(d) { return d.date; });
    // Last date in table
    maxDate = d3.max(data, function(d) { return d.date; });

    tScale = d3.scaleTime()
      .domain([minDate, maxDate])
      .range([o.paddingOuter, 2*Math.PI-o.paddingOuter]);

    // Get position on outer band based on date
    bandPos = function(date) {
      var x = o.linelength * Math.cos(-tScale(date) + Math.PI/2 ) + o.w/2;
      var y = o.w - (o.linelength * Math.sin(-tScale(date) + Math.PI/2 ) + o.h/2);
      return [x,y];
    }

    // Define the projection function
    projection = d3.geoMercator()
          .translate([o.w/2, o.h/2])
          .center([o.centerLon,o.centerLat])
          .scale(o.scale);

    // The parent of all other (virtual) elements that get the data appended to them.
    // Corresponds to the svg element in the standard case.
    var customBase = document.createElement('custom');
    custom = d3.select(customBase); // This is your SVG replacement and the parent of all other elements

    // Append the placeholders for the new elements
    var join = custom.selectAll('custom.line')
      .data(data);

    // Define what should happen to the elements that are entering the scene
    var enterSel = join.enter()
      .append('custom')
      .attr('class', 'line')
      .attr("x1", d => projection([+d.longitude, +d.latitude])[0])
      .attr("y1", d => projection([+d.longitude, +d.latitude])[1])
      .attr("x2", d => bandPos(d.date)[0])
      .attr("y2", d => bandPos(d.date)[1])
      .attr("mid-color", d => makeAlpha(o.colors[d.dataset], "mid"))
      .attr("end-color", d => makeAlpha(o.colors[d.dataset], "end"))
      .attr("stroke-width", 1)
      .attr("pointer-events", "none")
      .attr("selected", true); // Is translated to string automatically
    /*
    // Define what should happen to the elements that get updated/leave the scene
    mergeSel
    exitSel
    */
  };

  /*
  Define the rendering:
  Here, we select the virtual line elements holding the data and draw the
  corresponding line for each element/datum onto the canvas
  */
  function chart() {
    o.context.clearRect(0, 0, o.w, o.h); // Clear the canvas.
    var elements = custom.selectAll('custom.line');// Grab all elements you bound data to in the databind() function.
    // Draw on canvas
    elements.each(function(d) {
      /*
      d is the actual datum (not necessary anymore, since the display-space
      coordinates have already been calculated and attached to the virtual line elements
      in databind()). Hence, we just iterate over the line elements, holding
      the ready-to-go information for rendering.
      */
      var node = d3.select(this);   // This is each individual line element in the loop.
      if (node.attr("selected")=="true") {
        // Create linear gradient
        var gradient = o.context.createLinearGradient(node.attr('x1'), node.attr('y1'), node.attr('x2'), node.attr('y2'));
        // Add four color stops
        gradient.addColorStop(0, node.attr('end-color'));
        gradient.addColorStop(o.innerLineLength, node.attr('mid-color'));
        gradient.addColorStop(o.outerLineLength, node.attr('mid-color'));
        gradient.addColorStop(1, node.attr('end-color'));
        // Set the fill style and draw a line
        o.context.strokeStyle = gradient;
        o.context.beginPath();       // Start a new path
        o.context.moveTo(node.attr('x1'), node.attr('y1'));    // Move the pen to mapPoint
        o.context.lineTo(node.attr('x2'), node.attr('y2'));  // Draw a line to bandPoint
        o.context.stroke();          // Render the path
      }
    });
  };




  chart.highlightSpatial = function(d) {
    // Get all polygon data for the current country (including holes
    // and multiple polygons)
    var polygons = d3.select(d).data()[0].geometry.coordinates;
    // Type of the polygon, either "Polygon" or "MultiPolygon"
    var polygonType = d3.select(d).data()[0].geometry.type;
    // Filter for all lines in the given polygon data
    var debugg = custom.selectAll("custom.line").attr("selected", function(d) {
      if(polygonType == "Polygon") {
        return inPolygon(polygons,[d.longitude,d.latitude]);
      }
      else if (polygonType == "MultiPolygon") {
        // Go through all polygons and check whether the line begins
        // in them
        var pointContained = false;
        for(var i=0; i<polygons.length; i++) {
          if(inPolygon(polygons[i],[d.longitude,d.latitude]))
            pointContained = true;
        }
        return pointContained;
      }
      else {
        console.log("Unknown polygon type: "+polygonType);
        return null;
      }
    });
    //highlightLines(selectedLines);
    chart();
  }

  chart.lowlight = function() {
    o.context.clearRect(0, 0, o.w, o.h); // Clear the canvas.
  }

  chart.highlightTemporal = function(p, nrBins) {
    /*
    // Get number of highlighted bin
    var highlightBin = d3.select(p).data()[0];
    // Filter for all lines in the given temporal bin
    var tScaleAbsRange = tScale.range()[1] - tScale.range()[0];
    var selectedLines = d3.selectAll("line")
      .filter(function(d) {
        if (highlightBin == nrBins) {
          // Include very last event
          return tScale(d.date) >= tScale.range()[0] + tScaleAbsRange * highlightBin/nrBins
        }
        else {
          return (tScale(d.date) >= tScale.range()[0] + tScaleAbsRange * highlightBin/nrBins &&
                  tScale(d.date) < tScale.range()[0] + tScaleAbsRange * (highlightBin+1)/nrBins)
        }
      })
      .data();
    highlightLines(selectedLines);
    */
  }

  chart.w = function(value) {
      if (!arguments.length) return o.w;
      o.w = value;
      return chart;
  };
  chart.h = function(value) {
      if (!arguments.length) return o.h;
      o.h = value;
      return chart;
  };
  chart.radialPadding = function(value) {
      if (!arguments.length) return o.radialPadding;
      o.radialPadding = value;
      return chart;
  };
  chart.colors = function(value) {
      if (!arguments.length) return o.colors;
      o.colors = value;
      return chart;
  };
  chart.opacityMultiplyer = function(value) {
      if (!arguments.length) return o.opacityMultiplyer;
      o.opacityMultiplyer = value;
      return chart;
  };
  chart.linelength = function(value) {
      if (!arguments.length) return o.linelength;
      o.linelength = value;
      return chart;
  };
  chart.midOpacity = function(value) {
      if (!arguments.length) return o.midOpacity;
      o.midOpacity = value;
      return chart;
  };
  chart.endOpacity = function(value) {
      if (!arguments.length) return o.endOpacity;
      o.endOpacity = value;
      return chart;
  };
  chart.innerLineLength = function(value) {
      if (!arguments.length) return o.innerLineLength;
      o.innerLineLength = value;
      return chart;
  };
  chart.outerLineLength = function(value) {
      if (!arguments.length) return o.outerLineLength;
      o.outerLineLength = value;
      return chart;
  };
  chart.paddingOuter = function(value) {
      if (!arguments.length) return o.paddingOuter;
      o.paddingOuter = value;
      return chart;
  };
  chart.centerLon = function(value) {
      if (!arguments.length) return o.centerLon;
      o.centerLon = value;
      return chart;
  };
  chart.centerLat = function(value) {
      if (!arguments.length) return o.centerLat;
      o.centerLat = value;
      return chart;
  };
  chart.context = function(value) {
      if (!arguments.length) return o.context;
      o.context = value;
      return chart;
  };

  return chart;
}
