function unDo() {
  var o, div, dropdown, counter, recentState; // options, (helper)functions

  o = {
    history: [],
    helpbuttons: null
  }

  // Define the rendering
  function chart(container) {
    counter = 0;
    div = container;
    // Append undo image
    div.append("img").attr("src", "undo_bg_line.png").attr("width", 44).attr("height", 40).style("float", "left");
    // Append help button
    div.call(o.helpbuttons.addButton, "undo");
    // Append text labels
    var labelContainer = div.append("div").attr("id", "undo-label-container");
    labelContainer.append("label").text("Undo/Redo spatial, temporal, and categorical Filtering:").attr("class", "label-header");
    labelContainer.append("br");
    labelContainer.append("label").text("Select filtering state to go back to: ").style("margin-left", "20px");
    dropdown = labelContainer.append("select")
      .attr("id", "select-undo");
    dropdown.append("option")
      .attr("id", "select-undo-option-"+counter)
      .attr("value", counter)
      .text("Initial");
    recentState = counter;
    counter++;
    labelContainer.append("button")
      .attr("id", "undo-reset-button")
      .text("Reset Filtering");
    o.history.push({
      temporal: null,
      spatial: null,
      "spatial (country)": null,
      "categorical (barcharts)": null,
      "categorical (match-tree)": null
    });

  }

  function deleteStates(keepState) {
    keepState = +keepState;
    for (var i=keepState+1; i<counter; i++)
    {
      // Remove dropdown options
      div.selectAll("#select-undo-option-"+i).remove();
    }
    // Remove entries from the history
    o.history = o.history.slice(0,keepState+1);
    counter = keepState + 1;
  }

  function getCategoricalState(state, stateTypeToCheck, previousEntry) {
    previousEntry[stateTypeToCheck];
    if (state.type == stateTypeToCheck) {
      if (previousEntry[stateTypeToCheck] == null) {
        // No merging necessary
        return state.constraints;
      }
      else {
        // Merge previous filter constraints with new ones
        var dimensions = Object.keys(state.constraints);
        dimensions.forEach(dim => {
          state.constraints[dim] = state.constraints[dim].concat(previousEntry[stateTypeToCheck][dim])
        })
        return state.constraints;
      }
    }
    else {
      return previousEntry[stateTypeToCheck];
    }
  }

  chart.addState = function(state) {
    // temporal: {minDate, maxDate}
    // spatial: {centerLon, centerLat, scale, bbox}
    if (recentState < counter-1) {
      // If a new state is added while the current reset(ted) state is behind the newest one, drop all entries after the recent state
      deleteStates(recentState);
    }
    var previousEntry = o.history[o.history.length-1];
    o.history.push({
      temporal: state.type == "temporal" ? state.constraints : previousEntry.temporal,
      spatial: state.type == "spatial" ? state.constraints : state.type == "spatial (country)" ? state.constraints : previousEntry.spatial,
      "spatial (country)": state.type == "spatial (country)" ? state.constraints : previousEntry["spatial (country)"],
      // Actually works like that because we check for everything that should be kept and anything that was removed
      // in previous steps is not added to the "selected" selection since it does not exist anymore as dom element
      "categorical (barcharts)": state.type == "categorical (barcharts)" ? state.constraints : previousEntry["categorical (barcharts)"],
      // Here we need to consider the previous states since we only know what should be excluded from this filtering but not from the previous ones
      "categorical (match-tree)": getCategoricalState(state, "categorical (match-tree)", previousEntry)
    });
    dropdown.append("option")
      .attr("id", "select-undo-option-"+counter)
      .attr("value", counter)
      .text(state.type);
    dropdown.property("value", counter)
    recentState = counter;
    console.log(counter);
    counter++;
    console.log(o.history);
  };

  chart.getFilteringState = function() {
    if (typeof dropdown !== "undefined") {
      var resetIdx = dropdown.property("value");
      return o.history[resetIdx];
    }
    else {
      return null;
    }
  };

  chart.resetRecentIndex = function() {
    // Update the index indicating the current filtering state
    var resetIdx = dropdown.property("value");
    // Adjust recent state to reset state
    recentState = resetIdx;
  };

  chart.deleteHistory = function() {
    deleteStates(0);
  };

  chart.history = function(value) {
      if (!arguments.length) return o.history;
      o.history = value;
      return chart;
  };

  chart.helpbuttons = function(value) {
      if (!arguments.length) return o.helpbuttons;
      o.helpbuttons = value;
      return chart;
  };

  return chart;
}
