function buildTaxonomies(taxData) {
  var taxonomies = {};
  var dimensions = Object.keys(taxData);

  var tagify = function(array) {
    return array.reduce((a,b) => a + "#" + b) + "#";
  };

  var inBinRange = function(val, intervalString) {
    // Split the interval string to check if val lies inbetween its bounds
    var sepIdx = intervalString.indexOf(",");
    var lowerBound = +intervalString.slice(1, sepIdx);
    var upperBound = +intervalString.slice(sepIdx+1, intervalString.length-1);
    if (intervalString[intervalString.length-1] == "]") {
      // Last bin should include the upper bound
      return (lowerBound <= val && val <= upperBound)
    }
    else {
      // Upper bound should be excluded
      return (lowerBound <= val && val < upperBound)
    }
  };

  dimensions.forEach(function(dim) {
    var data = taxData[dim];
    var tree = {};
    // Get depth aka max nr of levels of the dimension:
    // Get all columns that start with "Level_", extract the actual level values as ints and get their max
    var depth = d3.max(data.columns.filter(d => d.startsWith("Level_")).map(d => d.replace("Level_", "")).map(d => +d))
    // Merge all relevant columns together (not the base categories)
    var mergedData = data.map(d => {
      var mergedDatum =  [d["data.source"]];
      // Go through all existing levels and append the correpsonding value
      d3.range(1, depth+1).forEach(lvl => mergedDatum.push(d["Level_"+lvl]))
      return mergedDatum;
    });
    // Get all unique combinations of the different levels
    var uniqueData = [...new Set(mergedData)];
    // Iterate over all unique combinations
    // Based on https://stackoverflow.com/questions/26005610/initialize-a-javascript-object-tree-to-any-depth-nested-objects
    uniqueData.forEach(function(path) {
      var parent = tree;
      parent["depth"] = depth;
      var child = dim;
      for (var lvl = depth; lvl > 0; lvl--) {
        // Set up general properties of parent node
        // opened will later be used to check how far the taxonomy is unfolded in the barcharts
        parent["opened"] = false;
        parent["name"] = child;
        parent["count"] = 0;
        if (typeof parent["children"] === "undefined") {
          parent["children"] = {};
        }
        // For each node on a unique path, append the preceeding path to the taxonomy
        child = path.pop();
        var child = typeof child === "undefined" ? "undefined" : child;
        if (typeof parent["children"][child] === "undefined") {
          parent["children"][child] = {};
        }
        parent = parent["children"][child];
      }
      // For the leaf nodes
      parent["opened"] = false;
      parent["name"] = child;
      parent["count"] = 0;
    });
    taxonomies[dim] = tree;
  });

  taxonomies.getNodePaths = function(dim, content, pathSoFar, allPaths) {
    // Append current name to current path and add to overall path list
    var path = [...pathSoFar];
    path.push(content.name);
    if (content.name == "") console.log(content);
    allPaths.push({"dim": dim, "path": path, "count": content.count});

    // Propagate to children
    if (typeof content.children !== "undefined") {
      _.each(content.children, (value, key) => {
        if (key !== "undefined") {
          content = value;
          taxonomies.getNodePaths(dim, content, path, allPaths);
        }
      });
    }
  };

  taxonomies.getAllNodePaths = function() {
    var pathSoFar = [];
    var allPaths = [];
    _.each(taxonomies, (content, dim) => {
      if (typeof content !== "function") {
        // Propagate to children
        if (typeof content.children !== "undefined") {
          _.each(content.children, (value, key) => {
            if (key != "undefined") {
              content = value;
              taxonomies.getNodePaths(dim, content, pathSoFar, allPaths);
            }
          });
        }
      }
    });
    return allPaths;
  };

  taxonomies.openPath = function(dim, path) {
    var parent = taxonomies[dim];
    for (var i=0; i<path.length; i++) {
      parent = parent["children"][path[i]];
      parent["opened"] = true;
    }
  };

  taxonomies.closeNode = function(dim, path) {
    var node = taxonomies[dim];
    for (var i=0; i<path.length; i++) {
      node = node["children"][path[i]];
    }
    node["opened"] = false;
  };

  taxonomies.getChildren = function(dim, path) {
    var parent = taxonomies[dim];
    for (var i=0; i<path.length; i++) {
      parent = parent["children"][path[i]];
    }
    return typeof parent["children"] !== "undefined" ? parent["children"] : null;
  };

  taxonomies.updateCounts = function(dim, content, pathSoFar, matchData = null) {
    // Filtering and counting stuff here
    var level = pathSoFar.length + 1;
    var path = [...pathSoFar];
    path.push(content.name);
    var count = 0;

    var filteredData = matchData; // DUMMY
    count = Math.floor(Math.random()*30); // DUMMY
    content.count = count;

    // Propagate to children
    if (typeof content.children !== "undefined") {
      _.each(content.children, (value, key) => {
        if (key != "undefined") {
          content = value;
          taxonomies.updateCounts(dim, content, path, filteredData)
        }
      });
    }
  }

  taxonomies.updateTaxonomyCounts = function(matchData = null) {
    var pathSoFar = [];
    _.each(taxonomies, (content, dim) => {
      // Propagate to children
      if (typeof content.children !== "undefined") {
        _.each(content.children, (value, key) => {
          content = value;
          taxonomies.updateCounts(dim, content, pathSoFar, matchData)
        });
      }
    });
  };

  taxonomies.getCount = function(dim, path) {
    var parent = taxonomies[dim];
    for (var i=0; i<path.length; i++) {
      parent = parent["children"][path[i]];
    }
    return parent.count;
  };

  taxonomies.calculateCounts = function(dim, content, pathSoFar, filteredData) {
    // Filtering and counting stuff here
    var level = pathSoFar.length + 1;
    var path = [...pathSoFar];
    path.push(content.name);
    var pathtag = tagify(path);
    // Filter matchData according to path and store in count
    if (dim == "dataset") {
      // Filter for matches that have the corresponding dataset either in their primary or duplicate event
      content.count = filteredData.filter(d => (d[dim] == path || d["duplicate_"+dim] == path)).length;
    }
    else if (dim == "size") {
      // Filter for matches that have the corresponding length/size
      content.count = filteredData.filter(d => d[dim] == path).length;
    }
    else if (dim == "score") {
      // Filter for matches whose score falls into the corresponding bin given by path string
      content.count = filteredData.filter(d => inBinRange(d[dim], path[0])).length;
    }
    else {
      // Just filter for matches having the current path
      content.count = filteredData.filter(d => d[dim+"_path"] == pathtag).length;///filteredData.length;
    }

    // Propagate to children
    if (typeof content.children !== "undefined") {
      _.each(content.children, (value, key) => {
        if (key != "undefined") {
          content = value;
          taxonomies.calculateCounts(dim, content, path, filteredData)
        }
      });
    }
  };

  taxonomies.cumulateCounts = function(dim, content) {
    // Propagate to children
    if (typeof content.children !== "undefined") {
      _.each(content.children, (value, key) => {
        if (key != "undefined") {
          //content = value;
          content.count += 1/2 * taxonomies.cumulateCounts(dim, value);
        }
      });
    }
    return content.count;
  };

  taxonomies.updateTaxonomyCountsReal = function(updateFn, filteredData = null) {
    // matchData is expected to already be filtered for the correct parameter combination
    var pathSoFar = [];
    _.each(taxonomies, (content, dim) => {
      // Propagate to children
      if (typeof content.children !== "undefined") {
        _.each(content.children, (value, key) => {
          content = value;
          updateFn(dim, content, pathSoFar, filteredData)
        });
      }
    });
  };

  return taxonomies;
}
