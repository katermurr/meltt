function helpButtons() {
  var getText;
  var operations = {};
  var texts = {};

  getText = function(id) {
    var returnText = texts[id];
    if (typeof returnText === "undefined")
      alert('No help text could be found for help button "'+id+'".')
    return returnText;
  };

  operations.addButton = function(selection, id) {
    var button = selection.append("button")
      .attr("id", id+"-help-button")
      .attr("class", "help-button")
      .text("?");
    var text = getText(id);
    if (typeof text !== "undefined") {
      var alertFn = d => alert(text);
      button.on("mousedown", alertFn);
    }
  }

  texts["paramultiples"] =
    "This view provides an overview of the match distributions for the different parameter combinations of \u0394s and \u0394t.\n"
    +"For each combination, a relative histogram is displayed, showing the frequency of the scores of the identified matches.\n"
    +"Between the different histograms, bars depict how dissimilar the distributions of the matches across the taxonomy trees between the corresponding parameter combinations are.\n"
    +"The larger the bar, the more dissimilar are the distributions.";

  texts["scopetools"] =
    "To select the scope, first select a parameter combination to inspect more closely by clicking on one of the above histograms.\n"
    +"Afterwards, choose between analyzing all events or matches only, represented by tuples of events.\n"
    +"In case you want to analyze all events, select which attribute should be the primary attribute. Its most high-level categories will be represented in distinct colors.\n"
    +"The scope can be updated at all times. The update can take up to a minute, depending on how many matches need to be processed due to the parameter combination.\n"
    +'To export the data from the current view, hit "Export Data".';

  texts["tempmap"] =
    'In this view you can inspect the events (resp. the primary event of each match if analyzing "Matches Only"), each represented through a single line.\n'
    +'Each line connects the position on the map of the corresponding event with the time it took place at on the temporal ring surrounding the map.\n'
    +'The temporal ring is a stacked barchart plot, summarizing the distribution of the events over time.\n'
    +'Interactions allow zooming and brushing of the map. At all times, an overview is given though the minified version of the map view on top, including white reference frames of the currently brushed area.\n'
    +'Possible interactions are the following:\n'
    +'\u2022 "Country Highlighting": Show only the events from the country you hover over.\n'
    +'\u2022 "Spatial Brushing": Brush and zoom into box-shaped areas of the map. Hold and drag the mouse button to perform.\n'
    +'\u2022 "Select line appearance": Choose how strong the lines should be displayed. If "None" is selected, only points are displayed.\n'
    +'\u2022 Temporal brushing and zooming is possible at all times by holding and dragging outside of the temporal ring, where the date labels are placed.\n';

  texts["undo"] =
    'This component allows you to undo and redo certain filterings that you have performed.\n'
    +'To jump back to a previous state, select it in the dropdown menu and click "Reset Filtering".\n'
    +'Once you perform a new filtering after having jumped back to a previous state, the subsequent filtering states originally following the state you have jumped back to are overwritten.\n'
    +'The filtering history is cleared when updating the scope.\n';

  texts["barcharts"] =
    'In this view you can analyze the distribution of the events across different categories. The number of events falling into each overarching category is represented by the width of the corresponding bar.\n'
    +'The following interaction modes are possible:\n'
    +'\u2022 "Info": Inspect the total amount of events for each bar. Additionally, align the bars depending on one of the primary colors by clicking the corresponding circle on the left.\n'
    +'\u2022 "Drill Down": If possible, drill one level deeper into the child categories of a given category.\n'
    +'\u2022 "Drill Up": If possible, drill one level up from a set of child categories.\n'
    +'\u2022 "Filter": Filter out certain events by de-selecting the corresponding bar stacks by clicking on the "plus"-symbol. Entire axes can be (de-)selected at once.\n'
    +'Additionally, the labels can be hidden and the axes can be reorderd.\n'
    +'By reordering the attribute axes, those are placed first next to the primary attribute axis which allow the best separation of the primary highest-level attribute values. The order depends on how deep you have drilled into the different taxonomies.';

  texts["matchtree"] =
    'In this view you can analyze the distribution of the matches across different categories. The number of matches identified in each category is represented by the color of the corresponding node as well as the length of the surrounding arc.\n'
    +'The maximum displayed value corresponds to the largest amount of matches identified in a single category.\n'
    +'In addition to the taxonomy trees used for matching, further information is displayed:\n'
    +'\u2022 "Size": The amount of events participating in a match.\n'
    +'\u2022 "Score": The interval that the matching scores of the different matches fall into.\n'
    +'\u2022 "Dataset": The participating datasets for each match.\n'
    +'Two types of interaction modes are available:\n'
    +'\u2022 "Structure": Collapse and expand subtrees of the taxonomies, displaying cumulative counts when collapsed.\n'
    +'\u2022 "Filter": Filter out matches identified in certain categories by de-selecting the corresponding categories before clicking "Apply Filter". Entire attribute trees can be (de-)selected by clicking on their root node.\n'
    +'After filtering, the change of the relative counts for each category is highlighted:\n'
    +'\u2022 "Loss" (/"Gain") arc section in red (/green): If a category'+"'"+'s relative count has reduced (/increased). If the length of the loss arc would exceed the limit given by one cycle rotation, it is cut off (this can happen if the previous maximum count was larger than the new one).\n';

  return operations;
}
