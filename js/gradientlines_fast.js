/*
The overall structure is inspired by: https://blog.beezwax.net/2017/11/08/large-d3-js-application-development/#inside
The canvas rendering is inspired by: https://www.freecodecamp.org/news/d3-and-canvas-in-3-steps-8505c8b27444/
*/

function gradientLines() {
  var o, fn, minDate, maxDate, tScale, colors, projection; // options, (helper)functions
  var firstRun = true;
  o = {
    w: 0,
    h: 0,
    radialPadding: 60,
    primaryAttr: "dataset",
    primaryVals: ["A", "G", "GT", "S"],
    colors: {"A": "rgb(75,174,121)", "G": "rgb(0,162,219)", "GT": "rgb(255,152,35)", "S": "rgb(183,41,177)"},
    opacityMultiplyer: 0.1,
    // Length of the lines
    linelength: 10,
    // The base values of the different stop values of the gradients
    midOpacityBase: 0.002,
    endOpacityBase: 0.01,
    // Length of line close to the actual geographic position
    innerLineLength: 0.2,
    // Length of line close to the temporal position
    outerLineLength: 0.75,
    paddingOuter: 0,
    centerLon: 0,
    centerLat: 0,
    scale: 200,
    context: null,
    contextAllSelected: null,
    dataModified: [],
    nrBins: 0, // Could decouple to pass just when highlighting
    mapClipRadius: 0,
    custom: null,
    filterfunctions: null,
    taxonomies: null
  };

  // Extract Bounding box of a given (Multi)Polygon
  var getPolygonBbox = function(polygons, polygonType) {
    var bbox = {minLon: 90, minLat: 180, maxLon: -90, maxLat: -180};
    if (polygonType==="Polygon") {
      // If new value is smaller/greater than the current min/max, replace the current value
      var newVal = d3.min(polygons[0], d => d[0]); // d[0] corresponds to longitude
      if (newVal < bbox.minLon) bbox.minLon = newVal;
      newVal = d3.min(polygons[0], d => d[1]); // d[1] corresponds to latitude
      if (newVal < bbox.minLat) bbox.minLat = newVal;
      newVal = d3.max(polygons[0], d => d[0]); // d[0] corresponds to longitude
      if (newVal > bbox.maxLon) bbox.maxLon = newVal;
      newVal = d3.max(polygons[0], d => d[1]); // d[1] corresponds to latitude
      if (newVal > bbox.maxLat) bbox.maxLat = newVal;
    }
    // Iterate through the multiple polygons
    else if (polygonType==="MultiPolygon") {
      for (var i=0; i<polygons.length; i++) {
        var newVal = d3.min(polygons[i][0], d => d[0]); // d[0] corresponds to longitude
        if (newVal < bbox.minLon) bbox.minLon = newVal;
        newVal = d3.min(polygons[i][0], d => d[1]); // d[1] corresponds to latitude
        if (newVal < bbox.minLat) bbox.minLat = newVal;
        newVal = d3.max(polygons[i][0], d => d[0]); // d[0] corresponds to longitude
        if (newVal > bbox.maxLon) bbox.maxLon = newVal;
        newVal = d3.max(polygons[i][0], d => d[1]); // d[1] corresponds to latitude
        if (newVal > bbox.maxLat) bbox.maxLat = newVal;
      }
    }
    else {
      console.log("Unknown polygon type: "+polygonType);
    }
    return bbox;
  };

  // For a given position on a line, return the corresponding rgba value based
  // on a plain rgb value
  var makeAlpha = function(rgbVal, position) {
    // tbd no check for whether valid rgb code is fed.
    var alpha;
    // Determine where on the line the alpha value should be calculated
    if (position == "mid") alpha = o.midOpacityBase;
    else if (position == "end") alpha = o.endOpacityBase;
    else {
      console.log("Invalid position on line: " + position);
      alpha = 0;
    }
    // Adjust according to the opacityMultiplyer
    alpha = alpha * o.opacityMultiplyer;
    // Adjust according to transfer function
    alpha = (Math.round((alpha * o.opacityAdjustmentFactor * 10000)) / 10000);
    // Remove bracket in the end of rgb(X,Y,Z) and rgb in beginning
    var rgbaVal = rgbVal.substring(3, rgbVal.length - 1);
    // Append the alpha value after comma and close off with bracket
    rgbaVal = "rgba" + rgbaVal + "," + alpha + ")";

    return rgbaVal;
  };

  var adjustAlpha = function(rgbaVal) {
    var lastCommaIdx = rgbaVal.lastIndexOf(",");
    var alpha = rgbaVal.slice(lastCommaIdx+1, rgbaVal.length).replace(' ','').replace(')','')
    return rgbaVal.slice(0, lastCommaIdx+1) + (Math.round((alpha * o.opacityAdjustmentFactor * 10000)) / 10000) + ")";
  };

  var colHeader = function(dimName, lvl) {
    if (["dataset", "parameters"].includes(dimName)) {
      return dimName;
    }
    else {
      return dimName+"_level_"+lvl;
    }
  };

  // Append data to a persistent data array.
  chart.databind = function(data) {
    // First date in table
    minDate = d3.min(data, function(d) { return d.date; });
    // Last date in table
    maxDate = d3.max(data, function(d) { return d.date; });

    tScale = d3.scaleTime()
      .domain([minDate, maxDate])
      .range([o.paddingOuter/2, 2*Math.PI-o.paddingOuter/2]);

    alphaScale = d3.scaleLinear()
      .domain([0, data.length])
      .range([2, 0.1])

    // Get position on outer band based on date
    bandPos = function(date) {
      var x = o.linelength * Math.cos(-tScale(date) + Math.PI/2 ) + o.w/2;
      var y = o.h - (o.linelength * Math.sin(-tScale(date) + Math.PI/2 ) + o.h/2);
      return [x,y];
    }

    // Define the projection function
    projection = d3.geoMercator()
          .translate([o.w/2, o.h/2])
          .center([o.centerLon,o.centerLat])
          .scale(o.scale);


    data.forEach(function(d) {
      var row = {...d};
      // Get depth of primary attribute if it is defined (not the case for match
      // view because primary attr then is "parameters", not included in tax, only used for uniform color)
      var depthPrimaryAttr = o.taxonomies[o.primaryAttr] ? o.taxonomies[o.primaryAttr]["depth"] : null;
      row["color"] = o.colors[row[colHeader(o.primaryAttr, depthPrimaryAttr)]];
      row["selected"] = true;
      o.dataModified.push(row);
    });
  };

  /*
  Define the rendering:
  Here, we select the virtual line elements holding the data and draw the
  corresponding line for each element/datum onto the canvas
  */
  function chart(showLines, clearCanvas) {
    if (clearCanvas) o.context.clearRect(0, 0, o.w, o.h); // Clear the canvas.
    var dataSelected = _.filter(o.dataModified, d => d.selected);
    // Adjust transparency
    o.opacityAdjustmentFactor = dataSelected.length<100 ? 30 : Math.max(0.1 + Math.min(2000000 * (dataSelected.length+2500)**(-1.5), 30), 0.2);
    console.log(o.opacityAdjustmentFactor);
    console.log(dataSelected.length);
    dataSelected.forEach(function(d) {
      if (d.selected) {
        // Get begin and end positions of lines
        var startpoint = projection([d.longitude, d.latitude]);
        var endpoint = bandPos(d.date);
        // Render points
        o.context.beginPath();
        var endColor, midColor;
        if (o.opacityMultiplyer == 0) {
          // In this case, only the points will be drawn (see the check later on)
          // Since they also use the makeAplha function, adjust opacityMultiplyer for a second
          o.opacityMultiplyer = 1;
          endColor = makeAlpha(d.color, "end");
          midColor = makeAlpha(d.color, "mid");
          o.opacityMultiplyer = 0;
        }
        else {
          endColor = makeAlpha(d.color, "end");
          midColor = makeAlpha(d.color, "mid");
        }
        o.context.fillStyle = endColor;
        o.context.arc(startpoint[0], startpoint[1], 2, 0, 2 * Math.PI, true);
        o.context.fill();
        // Dont draw any lines if opacityMultiplyer is 0
        if (o.opacityMultiplyer > 0) {
          // Create linear gradient
          var gradient = o.context.createLinearGradient(startpoint[0], startpoint[1], endpoint[0], endpoint[1]);
          // Add four color stops
          gradient.addColorStop(0, endColor);
          gradient.addColorStop(o.outerLineLength, midColor);
          gradient.addColorStop(o.innerLineLength, midColor);
          gradient.addColorStop(1, endColor);
          // Set the fill style and draw a line
          o.context.strokeStyle = gradient;
          o.context.beginPath();       // Start a new path
          o.context.moveTo(startpoint[0], startpoint[1]);    // Move the pen to mapPoint
          o.context.lineTo(endpoint[0], endpoint[1]);  // Draw a line to bandPoint
          o.context.stroke();          // Render the path
        }
      }
    });
    if (dataSelected.length == o.dataModified.length) {
      o.contextAllSelected.clearRect(0, 0, o.w, o.h); // Clear the canvas.
      o.contextAllSelected.drawImage(o.context.canvas, 0, 0);
    }
    // Save allselected context
  };

  chart.highlightSpatial = function(filterFn, showLines) {
    // Filter for all lines in the given polygon data
    o.dataModified.forEach(function(d) {
      d.selected = filterFn(d);
    });
    var drawLines = true;
    var clearCanvas = true;
    chart(drawLines, clearCanvas);
  }

  chart.lowlight = function() {
    o.context.clearRect(0, 0, o.w, o.h); // Clear the canvas.
    // The selectedAll canvas is displayed instead externally, so this suffices
  }

  chart.highlightTemporal = function(filterFn) {
    // Filter for all lines in the given temporal bin
    o.dataModified.forEach(function(d) {
      d.selected = filterFn(d);
    });
    var drawLines = true;
    var clearCanvas = true;
    chart(drawLines, clearCanvas);
  }

  chart.brush = function(brushSelection) {
    // Get the lon and lat coordinates of the brushSelection
    const brushP0 = projection.invert([brushSelection[0][0], brushSelection[0][1]]);
    const brushP1 = projection.invert([brushSelection[1][0], brushSelection[1][1]]);
    const brushBbox = {
      minLon: brushP0[0],
      minLat: brushP1[1],
      maxLon: brushP1[0],
      maxLat: brushP0[1]
    };
    // Update centerLon anc centerLat
    o.centerLon = (brushBbox.maxLon + brushBbox.minLon) / 2;
    o.centerLat = (brushBbox.maxLat + brushBbox.minLat) / 2;
    // Update the scale, assumes w==h
    // Goal: Have the (quadratic) brushed region fitted into the clipped area of the map
    var a = Math.max(brushSelection[1][0]-brushSelection[0][0], brushSelection[1][1]-brushSelection[0][1]);
    console.log(o.w-3*o.radialPadding);
    console.log((o.w-3*o.radialPadding )/ Math.sqrt(2));
    console.log(a);
    o.scale = o.scale * ((2*o.mapClipRadius) / Math.sqrt(2)) / a;
    // Update projection
    projection
      .center([o.centerLon,o.centerLat])
      .scale(o.scale);
    // Determine which events lie inside the brushed square
    o.dataModified = _.filter(o.dataModified, d => o.filterfunctions.inBbox(brushBbox, d));
    // Draw chart
    var drawLines = true;
    var clearCanvas = true;
    chart(drawLines, clearCanvas);
    // Return these values for other functions to reduce computational effort
    return [o.centerLon, o.centerLat, o.scale, brushBbox];
  }

  chart.countryFilter = function(filterFn, polygonBbox) {

    // Get the lon and lat coordinates of the brushSelection
    const displayP0 = projection([polygonBbox.minLon, polygonBbox.minLat]);
    const displayP1 = projection([polygonBbox.maxLon, polygonBbox.maxLat]);

    // Update centerLon anc centerLat
    o.centerLon = (polygonBbox.maxLon + polygonBbox.minLon) / 2;
    o.centerLat = (polygonBbox.maxLat + polygonBbox.minLat) / 2;
    // Update the scale, assumes w==h
    // Goal: Have the (quadratic) brushed region fitted into the clipped area of the map
    var a = Math.max(Math.abs(displayP1[0]-displayP0[0]), Math.abs(displayP1[1]-displayP0[1]));
    o.scale = o.scale * ((2*o.mapClipRadius) / Math.sqrt(2)) / a;
    // Update projection
    projection
      .center([o.centerLon,o.centerLat])
      .scale(o.scale);

      ///*
    // Determine which events lie inside the brushed square
    o.dataModified = _.filter(o.dataModified, filterFn);
    // Draw chart
    var drawLines = true;
    var clearCanvas = true;
    chart(drawLines, clearCanvas);
    //*/


    // Return these values for other functions to reduce computational effort
    return [o.centerLon, o.centerLat, o.scale];
  }

  chart.filter = function(filterFn, dateRange = null) {
    // Determine which events lie inside the brushed time interval
    o.dataModified = _.filter(o.dataModified, d => filterFn(d));
    if (dateRange != null) {
      // Update temporal scale
      minDate = dateRange[0];
      maxDate = dateRange[1];
      tScale.domain(dateRange);
    }
    // Draw chart
    var drawLines = true;
    var clearCanvas = true;
    chart(drawLines, clearCanvas);
  };

  chart.setAllSelected = function() {
    // Set all events as selected
    o.dataModified.forEach(function(d) { d.selected = true; });
  };

  chart.clearData = function() {
    o.dataModified = [];
  };

  chart.w = function(value) {
      if (!arguments.length) return o.w;
      o.w = value;
      return chart;
  };
  chart.h = function(value) {
      if (!arguments.length) return o.h;
      o.h = value;
      return chart;
  };
  chart.radialPadding = function(value) {
      if (!arguments.length) return o.radialPadding;
      o.radialPadding = value;
      return chart;
  };
  chart.primaryVals = function(value) {
      if (!arguments.length) return o.primaryVals;
      o.primaryVals = value;
      return chart;
  };
  chart.primaryAttr = function(value) {
      if (!arguments.length) return o.primaryAttr;
      o.primaryAttr = value;
      return chart;
  };
  chart.colors = function(value) {
      if (!arguments.length) return o.colors;
      o.colors = value;
      return chart;
  };
  chart.opacityMultiplyer = function(value) {
      if (!arguments.length) return o.opacityMultiplyer;
      o.opacityMultiplyer = value;
      return chart;
  };
  chart.linelength = function(value) {
      if (!arguments.length) return o.linelength;
      o.linelength = value;
      return chart;
  };
  chart.midOpacity = function(value) {
      if (!arguments.length) return o.midOpacity;
      o.midOpacity = value;
      return chart;
  };
  chart.endOpacity = function(value) {
      if (!arguments.length) return o.endOpacity;
      o.endOpacity = value;
      return chart;
  };
  chart.innerLineLength = function(value) {
      if (!arguments.length) return o.innerLineLength;
      o.innerLineLength = value;
      return chart;
  };
  chart.outerLineLength = function(value) {
      if (!arguments.length) return o.outerLineLength;
      o.outerLineLength = value;
      return chart;
  };
  chart.paddingOuter = function(value) {
      if (!arguments.length) return o.paddingOuter;
      o.paddingOuter = value;
      return chart;
  };
  chart.centerLon = function(value) {
      if (!arguments.length) return o.centerLon;
      o.centerLon = value;
      return chart;
  };
  chart.centerLat = function(value) {
      if (!arguments.length) return o.centerLat;
      o.centerLat = value;
      return chart;
  };
  chart.scale = function(value) {
      if (!arguments.length) return o.scale;
      o.scale = value;
      return chart;
  };
  chart.context = function(value) {
      if (!arguments.length) return o.context;
      o.context = value;
      return chart;
  };
  chart.contextAllSelected = function(value) {
      if (!arguments.length) return o.contextAllSelected;
      o.contextAllSelected = value;
      return chart;
  };
  chart.nrBins = function(value) {
      if (!arguments.length) return o.nrBins;
      o.nrBins = value;
      return chart;
  };
  chart.mapClipRadius = function(value) {
      if (!arguments.length) return o.mapClipRadius;
      o.mapClipRadius = value;
      return chart;
  };
  chart.filterfunctions = function(value) {
      if (!arguments.length) return o.filterfunctions;
      o.filterfunctions = value;
      return chart;
  };
  chart.taxonomies = function(value) {
      if (!arguments.length) return o.taxonomies;
      o.taxonomies = value;
      return chart;
  };

  return chart;
}
