function matchTree() {
  var o, svg, dimensions, depths, nrDimLeafNodes, nrLeafNodes, rScale, aScale, xPos, yPos, xPosRoot, yPosRoot, lineStart, lineEnd, highlightAncestorLines, cScale, arc, max; 
  var leafNodes = [];
  var updateRadius, getRadiusScale, getAngleScale, getColorScale, tagify, arcTween, getChildCounts, getChildCountsOld, getGlyphNode, placeHoverLabel;

  o = {
    w: 0,
    h: 0,
    margin: 0,
    dimPadding: 1,
    radiusInner: 40,
    taxonomies: null,
    highlightThickness: 2,
    arcInnerRadius: 12,
    circleRadius: 15,
    lossColor: "rgb(120, 35, 35)",
    neutralColor: "rgb(170,170,170)",
    gainColor: "rgb(150,241,150)",
    bgLineGlyphColor: "rgb(100,100,100)"
  }


  updateRadius = function() {
    o.radiusOuter = d3.min([o.w, o.h])/2 - o.margin;
  };
  updateRadius();

  getRadiusScale = function() {
    var maxDepth = d3.max(_.map(depths, (val, key) => val))
    // To have all the highest levels on the same radius
    var offsetTillHighestLevel = (o.radiusOuter-o.radiusInner)/maxDepth + o.radiusInner;
    // Distance between the highest node a the tax, like violent event, to the lowest leaf node, like bombing
    var highestNodeToLeafDist = o.radiusOuter - offsetTillHighestLevel;
    var rScale = function(node) {
      if (depths[node.dim] == 1) {
        // Special case for depth of 1: place nodes on the outmost ring
        return o.radiusOuter;
      }
      else {
        // Otherwise, place nodes euqidistantly between offsetTillHighestLevel and o.radiusOuter
        var levelIncrement = highestNodeToLeafDist / (depths[node.dim]-1);
        // Path of length 1 should be right on offsetTillHighestLevel, leaf path should be on o.radiusOuter
        return offsetTillHighestLevel + (node.path.length - 1) * levelIncrement;
      }
    };
    return rScale;
  };

  getAngleScale = function() {
    var nodes = [];
    // Calculate stepsize from one node center to the next
    var stepsize = 2 * Math.PI / (leafNodes.length + dimensions.length * o.dimPadding);
    // Object to store all angles in, referencable by stringified path
    var angles = {};
    var updateAngles = function() {
      // First, update the leaf node angles
      leafNodes.forEach((node, idx) => {
        angles[JSON.stringify(node.path)] = idx * stepsize + dimensions.indexOf(node.dim) * o.dimPadding * stepsize;
      })
      // Then, for each dimension, travel across the levels towards the center of the tree
      // to calculate the remaining node angles as the average of the corresponding child angles
      // in each case
      dimensions.forEach(dim => {
        for (var lvl = depths[dim]-1; lvl>0; lvl--) {
          // Get all nodes from that level
          var lvlNodes = nodes.filter(node => node.dim == dim && node.path.length == lvl);
          lvlNodes.forEach(node => {
            // Get all ancestor paths of the node
            var ancestorNodes = nodes.filter(d => (d.path.reduce((a,b) => a+"#"+b)+"#").startsWith(node.path.reduce((a,b) => a+"#"+b)+"#"));
            var ancestorPaths = ancestorNodes.map(d => d.path);
            // Get min and max angle of the children nodes/paths and set the mean as the angle for the node
            var minAngle = d3.min(ancestorPaths, p => angles[JSON.stringify(p)]);
            var maxAngle = d3.max(ancestorPaths, p => angles[JSON.stringify(p)]);
            angles[JSON.stringify(node.path)] = (maxAngle+minAngle) / 2;
          })
        }
      });
    };
    var aScale = function(node) {
      return angles[JSON.stringify(node.path)];
    };

    aScale.domain = function(value) {
        if (!arguments.length) return nodes;
        nodes = value;
        updateAngles();
        return aScale;
    };

    return aScale;
  };

  getColorScale = function() {
    var max = 0;
    var cScale = function(count) {
      return `rgb(${40 + count/max*(0-40)}, ${40 + count/max*(204-40)}, ${40 + count/max*(255-40)})`;
    }

    cScale.max = function(value) {
        if (!arguments.length) return max;
        max = value;
        return cScale;
    };

    return cScale;
  };

  tagify = function(array) {
    return array.reduce((a,b) => a + "#" + b) + "#";
  };

  getChildCounts = function(parentPath) {
    return svg.selectAll(".matchtree-main-glyph").filter(d => {
      if (typeof d.path !== "undefined") {
        var childPath = tagify(d.path);
        return childPath.startsWith(parentPath) && childPath !== parentPath;
      }
      else {
        return false;
      }
    }).data().map(d => d.count).reduce((a,b) => a+b)
  };

  getChildCountsOld = function(parentPath) {
    return svg.selectAll(".matchtree-main-glyph").filter(d => {
      if (typeof d.path !== "undefined") {
        var childPath = tagify(d.path);
        return childPath.startsWith(parentPath) && childPath !== parentPath;
      }
      else {
        return false;
      }
    }).data().map(d => d.countOld).reduce((a,b) => a+b)
  };

  getGlyphNode = function(path) {
    return svg.selectAll(".matchtree-main-glyph")
      .filter(d => tagify(d.path) == path);
  };

  placeHoverLabel = function(x, y, labeltext, labelname, yAnchor = "bottom") {
    // Create the label group if it does not exist yet
    var g = svg.selectAll(`.matchtree-label-g.${labelname}`)
    .data([0])
    .join(
      enter => enter
      .append("g").attr("class", `matchtree-label-g ${labelname}`)
    );
    // Create label background rect
    var backgroundRect = g.selectAll(`.matchtree-label-background.${labelname}`)
    .data([0])
    .join(
      enter => enter
      .append("rect").attr("class", `matchtree-label-background ${labelname}`)
      .attr("fill", d3.select(":root").style("--compenent-bg-color"))
      .attr("pointer-events", "none").attr("opacity", 0.7)
    );
    // Create or update the label text
    g.selectAll(`.matchtree-label-text.${labelname}`)
    .data([0])
    .join(
      enter => enter
      .append("text").attr("class", `matchtree-label-text ${labelname}`)
      .attr("fill", "rgb(170, 170, 170)").attr("pointer-events", "none")
      .attr("text-anchor", "middle")
    )
    .text(labeltext);
    // Set width of background according to text bounding box
    var bbox = svg.select(`.matchtree-label-text.${labelname}`).node().getBBox();
    // bbox is quite large at top but very tight at bottom
    var bboxShift = 4;
    backgroundRect.attr("x", -bbox.width/2).attr("width", bbox.width).attr("y", -bbox.height+bboxShift).attr("height", bbox.height);
    if (x-bbox.width/2 < 0) {
      x = bbox.width/2;
    }
    if (yAnchor == "top") {
      g.attr("transform", `translate(${x},${y+bbox.height-bboxShift})`);
    }
    else {
      // yAnchor bottom
      g.attr("transform", `translate(${x},${y-bboxShift})`);
    }
  }

  // Define the rendering
  function chart(selection) {
    selection.each(function(data, idx) {

      if (!data) {return;}

      // For collapsing the tree later on
      data.forEach(d => {
        d.childCounts = 0;
        d.countOld = d.count;
        d.childCountsOld = 0;
      });

      // LOCAL VARIABLES
      // Get all dimensions from the data
      dimensions = [...new Set(data.map(d => d.dim))];
      // Get the maximal depth for each dimension
      depths = {};
      dimensions.map(dim => {
        depths[dim] = o.taxonomies[dim]["depth"];
      })
      // Extract the leaf nodes
      leafNodes = data.filter(d => d.path.length == depths[d.dim]);
      // Count how many leaf nodes there are for each dimension
      nrDimLeafNodes = {};
      dimensions.map(dim => {
        nrDimLeafNodes[dim] = leafNodes.filter(d => d.dim == dim).length;
      });

      // Select the svg element
      svg = d3.select(this);

      // D3 HELPER FUNCTIONS

      // Create layout scales
      rScale = getRadiusScale();
      aScale = getAngleScale();
      aScale.domain(data);

      xPos = function(node) {
        return o.w/2-40 + rScale(node) * Math.cos(aScale(node));
      };
      yPos = function(node) {
        return o.h/2 + rScale(node) * Math.sin(aScale(node));
      };
      xPosRoot = function(dim) {
        var minAngle = d3.min(data.filter(d => d.dim == dim), d => aScale(d));
        var maxAngle = d3.max(data.filter(d => d.dim == dim), d => aScale(d));
        var a = (minAngle+maxAngle)/2;
        return o.w/2-40 + o.radiusInner * Math.cos(a);
      };
      yPosRoot = function(dim) {
        var minAngle = d3.min(data.filter(d => d.dim == dim), d => aScale(d));
        var maxAngle = d3.max(data.filter(d => d.dim == dim), d => aScale(d));
        var a = (minAngle+maxAngle)/2;
        return o.h/2 + o.radiusInner * Math.sin(a);
      };
      xPosRootLabel = function(dim) {
        var minAngle = d3.min(data.filter(d => d.dim == dim), d => aScale(d));
        var maxAngle = d3.max(data.filter(d => d.dim == dim), d => aScale(d));
        var a = (minAngle+maxAngle)/2;
        // Assume the radius to be 10px longer than the center of the root glyph
        return o.w/2-40 + (o.radiusInner+7+3) * Math.cos(a);
      };
      yPosRootLabel = function(dim) {
        var minAngle = d3.min(data.filter(d => d.dim == dim), d => aScale(d));
        var maxAngle = d3.max(data.filter(d => d.dim == dim), d => aScale(d));
        var a = (minAngle+maxAngle)/2;
        // Assume the radius to be 10px longer than the center of the root glyph
        return o.h/2 + (o.radiusInner+7+3) * Math.sin(a);
      };
      xLabelOffset = function(node, dim) {
        var offset = 0;
        if (xPosRoot(dim) <= o.w/2-40) {
          offset = -node.getBBox().width-3;
        }
        return offset;
      };
      yLabelOffset = function(node, dim) {
        var offset = 0;
        if (yPosRoot(dim) > o.h/2) {
          offset = node.getBBox().height-6;
        }
        return offset;
      };
      lineStart = function (node) {
        if (node.path.length > 1) {
          // A real parent node exists
          var parentNode = data.filter(d => JSON.stringify(d.path) == JSON.stringify(node.path.slice(0,node.path.length-1)))[0];
          return {x: xPos(parentNode), y:yPos(parentNode)};
        }
        else {
          // The dimension node itself is the parent node
          return {x: xPosRoot(node.dim), y: yPosRoot(node.dim)}
        }
      };
      lineEnd = function (node) {
        return {x: xPos(node), y:yPos(node)};
      };

      highlightAncestorLines = function (dim, path = null) {
        // Hightlight the ancestor lines corresponding to the given parent node data
        var lines;
        if (path != null) {
          // The selected node is a category
          var pathtag = tagify(path);
          lines = svg.selectAll(".matchtree-lines")
            .filter(d => (tagify(d.path).startsWith(pathtag) && tagify(d.path) != pathtag));
        }
        else {
          // The selected node is a root
          lines = svg.selectAll(".matchtree-lines")
            .filter(d => (d.dim == dim));
        }
        lines.style("stroke", "rgb(200,200,200)");
      };

      max = d3.max(data, d => d.count+d.childCounts);

      cScale = getColorScale();
      cScale.max(max)

      arc = d3.arc()
        .innerRadius(o.arcInnerRadius)
        .outerRadius(o.circleRadius);

      // draw lines from one level deeper to one level higher

      // DATA BINDING
      // Append group in the back to later append level indicator to
      svg.selectAll(".matchtree-level-indicator-g")
        .data([0])
        .join(
          enter => enter.append("g").attr("class", "matchtree-level-indicator-g")
        );
      chart.updateLines(data);
      chart.updateRoots(data);
      chart.updateLegend();
      chart.updateGlyphsCircle(data);
      chart.updateGlyphsArc(data);
      // Append label group in the front to later append labels to
      svg.selectAll(".matchtree-label-g")
        .data([0])
        .join(
          enter => enter.append("g").attr("class", "matchtree-label-g")
        );
    });
  }

  chart.update = function(selection) {
    selection.each(function(data, idx) {

      if (!data) {return;}
      var dataOld = [...svg.selectAll(".matchtree-main-glyph").data()];
      // On initial run, dataOld is still empty
      if (dataOld.length == 0) dataOld = data;
      // Calculate the child counts of set them to 0 for collapsing the tree later on
      data.forEach(d => {
        // If node corresponding to path is collapsed, calculate its childCounts correctly
        // Get nodes' path tagified
        var path = tagify(d.path);
        // Get node
        var node = getGlyphNode(path);
        // Check for collapse
        if (node.classed("collapsed")) {
          // getChildCounts here still refers to the old data, since it gets it from the glyphs which are not yet updated
          // Hence, use the adapted code from the function
          d.childCounts = data.filter(d => {
            if (typeof d.path !== "undefined") {
              var childPath = tagify(d.path);
              return childPath.startsWith(path) && childPath !== path;
            }
            else {
              return false;
            }
          }).map(d => d.count).reduce((a,b) => a+b);
        }
        else {
          d.childCounts = 0;
        }

        // Save the old counts to each datum
        var dOld = dataOld.filter(e => tagify(e.path) == path)[0];
        d.countOld = dOld.count;
        d.childCountsOld = dOld.childCounts;
      });

      chart.updateGlyphsCircle(data);
      chart.updateGlyphsArc(data);
    });
  };

  chart.updateLegend = function(data) {
    // Set up legend
    var legendG = svg.append("g")
      .attr("class", "matchtree-legend-g");

    var additionalOffset = 10;
    var lossColor = "rgb(0,0,0)";
    var neutralColor = "rgb(123,123,123)";
    var gainColor = "rgb(255,255,255)";
    legendG.append("text")
      .attr("x", -60)
      .attr("y", -6)
      .attr("fill", "rgb(220,220,220)")
      .attr("font-weight", "bold")
      .text("Count (compared to largest");
    legendG.append("text")
      .attr("x", -60)
      .attr("y", 10)
      .attr("fill", "rgb(220,220,220)")
      .attr("font-weight", "bold")
      .text("count per category):");
    // Circles
    legendG.append("circle")
      .attr("class", "matchtree-label-circle")
      .attr("cx", o.circleRadius)
      .attr("fill", cScale(0));
    legendG.append("circle")
      .attr("class", "matchtree-label-circle")
      .attr("cx", 4*o.circleRadius+additionalOffset)
      .attr("fill", cScale(max/2));
    legendG.append("circle")
      .attr("class", "matchtree-label-circle")
      .attr("cx", 7*o.circleRadius+2*additionalOffset)
      .attr("fill", cScale(max));
    legendG.selectAll(".matchtree-label-circle")
      .attr("cy", 2*o.circleRadius+additionalOffset)
      .attr("r", o.circleRadius);
    // Background arcs
    legendG.append("path")
      .attr("class", "matchtree-label-ring-background")
      .attr("transform", d => "translate(" + o.circleRadius + ", " + (2*o.circleRadius+additionalOffset) + ")");
    legendG.append("path")
      .attr("class", "matchtree-label-ring-background")
      .attr("transform", d => "translate(" + (4*o.circleRadius+additionalOffset) + ", " + (2*o.circleRadius+additionalOffset) + ")");
    legendG.append("path")
      .attr("class", "matchtree-label-ring-background")
      .attr("transform", d => "translate(" + (7*o.circleRadius+2*additionalOffset) + ", " + (2*o.circleRadius+additionalOffset) + ")");
    legendG.selectAll(".matchtree-label-ring-background")
      .style("fill", o.bgLineGlyphColor)
      .attr("d", d => arc({startAngle: 0, endAngle: 2 * Math.PI}));
    // Count arcs
    legendG.append("path")
      .attr("class", "matchtree-label-ring-count")
      .attr("transform", d => "translate(" + (4*o.circleRadius+additionalOffset) + ", " + (2*o.circleRadius+additionalOffset) + ")")
      .attr("d", d => arc({startAngle: 0, endAngle: Math.PI}));
    legendG.append("path")
      .attr("class", "matchtree-label-ring-count")
      .attr("transform", d => "translate(" + (7*o.circleRadius+2*additionalOffset) + ", " + (2*o.circleRadius+additionalOffset) + ")")
      .attr("d", d => arc({startAngle: 0, endAngle: 2 * Math.PI}));
    legendG.selectAll(".matchtree-label-ring-count")
      .attr("fill", o.neutralColor);
    // Text
    legendG.append("text")
      .attr("class", "matchtree-label-circle-text")
      .attr("x", o.circleRadius)
      .text("0%");
    legendG.append("text")
      .attr("class", "matchtree-label-circle-text")
      .attr("x", 4*o.circleRadius+additionalOffset)
      .text("50%");
    legendG.append("text")
      .attr("class", "matchtree-label-circle-text")
      .attr("x", 7*o.circleRadius+2*additionalOffset)
      .text("100%");
    legendG.selectAll(".matchtree-label-circle-text")
      .attr("fill", "rgb(220,220,220)")
      .attr("text-anchor", "middle")
      .attr("y", 3*o.circleRadius+18+additionalOffset)
    // Loss gain neutral arc labebls
    legendG.append("text")
      .attr("x", -60)
      .attr("y", 7*o.circleRadius)
      .attr("fill", "rgb(220,220,220)")
      .attr("font-weight", "bold")
      .text("Change from last filtering:");
    // Lines
    legendG.append("line")
      .attr("class", "matchtree-label-line")
      .attr("x1", 0*o.circleRadius)
      .attr("x2", 2*o.circleRadius)
      .attr("stroke", o.lossColor);
    legendG.append("line")
      .attr("class", "matchtree-label-line")
      .attr("x1", 3*o.circleRadius+additionalOffset)
      .attr("x2", 5*o.circleRadius+additionalOffset)
      .attr("stroke", o.neutralColor);
    legendG.append("line")
      .attr("class", "matchtree-label-line")
      .attr("x1", 6*o.circleRadius+2*additionalOffset)
      .attr("x2", 8*o.circleRadius+2*additionalOffset)
      .attr("stroke", o.gainColor);
    legendG.selectAll(".matchtree-label-line")
      .attr("y1", 7*o.circleRadius+2*additionalOffset)
      .attr("y2", 7*o.circleRadius+2*additionalOffset)
      .attr("stroke-width", 4);
    // Text
    legendG.append("text")
      .attr("class", "matchtree-label-line-text")
      .attr("x", o.circleRadius)
      .text("Loss");
    legendG.append("text")
      .attr("class", "matchtree-label-line-text")
      .attr("x", 4*o.circleRadius+additionalOffset)
      .text("Neutral");
    legendG.append("text")
      .attr("class", "matchtree-label-line-text")
      .attr("x", 7*o.circleRadius+2*additionalOffset)
      .text("Gain");
    legendG.selectAll(".matchtree-label-line-text")
      .attr("fill", "rgb(220,220,220)")
      .attr("text-anchor", "middle")
      .attr("y", 7*o.circleRadius+18+2*additionalOffset)

    var bbox = legendG.node().getBBox();
    var margin = 30;
    legendG.attr("transform", `translate(${o.w-bbox.width+50}, ${margin})`)
  };

  chart.updateLines = function(data) {
    // LAYOUT FUNCTIONS
    var g = svg.selectAll(".matchtree-lines-g")
      .data([0])
      .join(
        enter => enter.append("g").attr("class", "matchtree-lines-g")
      );
    g.selectAll(".matchtree-lines")
      .data(data)
      .join(
        enter => enter
          .append("line")
          .attr("class", "matchtree-lines")
          .attr("x1", d => lineStart(d).x)
          .attr("x2", d => lineEnd(d).x)
          .attr("y1", d => lineStart(d).y)
          .attr("y2", d => lineEnd(d).y)
          .style("stroke", o.bgLineGlyphColor)
      );
  };

  chart.updateRoots = function() {
    var g = svg.selectAll(".matchtree-root-glyph-g")
      .data([0])
      .join(
        enter => enter.append("g").attr("class", "matchtree-root-glyph-g")
      );
    g.selectAll(".matchtree-root-glyph")
      .data(dimensions)
      .join(
        enter => enter
          .append("circle")
          .attr("class", "matchtree-root-glyph")
          .attr("cx", d => xPosRoot(d))
          .attr("cy", d => yPosRoot(d))
          .attr("fill", "rgb(170,170,170)")
          .attr("stroke", "rgb(200,200,200)")
          .attr("stroke-width", 0)
          .attr("r", 7)
      );

    // Place labels here
    var labels = g.selectAll(".matchtree-root-glyph-label")
      .data(dimensions)
      .join(
        enter => enter
          .append("text")
          .attr("class", "matchtree-root-glyph-label")
          .attr("x", d => xPosRootLabel(d))
          .attr("y", d => yPosRootLabel(d))
          .attr("fill", "rgb(170,170,170)")
          .text(d => d === "score" ? "Match score" : d.charAt(0).toUpperCase() + d.slice(1))
      );
    labels.attr("transform", function(d) {return `translate(${xLabelOffset(this, d)}, ${yLabelOffset(this, d)})`})
  };

  chart.updateGlyphsCircle = function(data) {
    // LAYOUT FUNCTIONS
    // Update colorscale to new maxium
    // Just quick check with newmax to assure max > 0
    var newmax = d3.max(data, d => d.count+d.childCounts);
    cScale.max(newmax > 0 ? newmax : max);
    // Create group for glyphs
    var g = svg.selectAll(".matchtree-main-glyph-g")
      .data([0])
      .join(
        enter => enter.append("g").attr("class", "matchtree-main-glyph-g")
      );
    // Append glyphs
    g.selectAll(".matchtree-main-glyph")
      .data(data)
      .join(
        enter => enter
          .append("circle")
          .attr("class", "matchtree-main-glyph")
          .attr("cx", d => xPos(d))
          .attr("cy", d => yPos(d))
          .attr("fill", d => cScale(+d.count +d.childCounts))
          .attr("stroke", "rgb(200,200,200)")
          .attr("stroke-width", 0)
          .attr("r", o.circleRadius)
          .attr("stroke-alignment", "outer"),
        update => update
          .transition().duration(300)
          .attr("fill", d => cScale(+d.count+d.childCounts))
      );
  };

  chart.updateGlyphsArc = function(data, collapseMode = "none", collapsePath = null) {
    // changeCollapse is true in case of collapse or uncollapse
    // collapsePath is the path that was collapsed in case of collapse and null in case of uncollapse

    // LAYOUT FUNCTIONS
    var oldmax = parseFloat(max);
    var dataPreviousView = [...svg.selectAll(".matchtree-glyph-ring.neutral").data()];
    if (dataPreviousView.length == 0) dataPreviousView = data;
    // Just quick check with newmax to assure max > 0
    var newmax = d3.max(data, d => d.count+d.childCounts);
    max = newmax > 0 ? newmax : oldmax;

    var gBackground = svg.selectAll(".matchtree-glyph-ring-g.background")
      .data([0])
      .join(
        enter => enter.append("g").attr("class", "matchtree-glyph-ring-g background")
      );
    var gNeutral = svg.selectAll(".matchtree-glyph-ring-g.neutral")
      .data([0])
      .join(
        enter => enter.append("g").attr("class", "matchtree-glyph-ring-g neutral")
      );
    var gGain = svg.selectAll(".matchtree-glyph-ring-g.gain")
      .data([0])
      .join(
        enter => enter.append("g").attr("class", "matchtree-glyph-ring-g gain")
      );
    var gLoss = svg.selectAll(".matchtree-glyph-ring-g.loss")
      .data([0])
      .join(
        enter => enter.append("g").attr("class", "matchtree-glyph-ring-g loss")
      );

    gBackground.selectAll(".matchtree-glyph-ring.background")
      .data(data)
      .join(
        enter => enter
          .append("path")
          .attr("class", "matchtree-glyph-ring background")
          .attr("transform", d => "translate(" + xPos(d) + ", " + yPos(d) + ")")
          .style("fill", o.bgLineGlyphColor)
          .attr("d", d => arc({startAngle: 0, endAngle: 2 * Math.PI}))
      );

    gNeutral.selectAll(".matchtree-glyph-ring.neutral")
      .data(data)
      .join(
        enter => enter
          .append("path")
          .attr("class", "matchtree-glyph-ring neutral")
          .attr("transform", d => "translate(" + xPos(d) + ", " + yPos(d) + ")")
          .attr("fill", o.neutralColor)
          .attr("d", d => arc({startAngle: 0, endAngle: 2 * Math.PI * (d.count+d.childCounts)/max})),
        update => update
          .transition().duration(300)
          .attrTween("d", arcTweenNeutral)
      );

    gGain.selectAll(".matchtree-glyph-ring.gain")
      .data(data)
      .join(
        enter => enter
          .append("path")
          .attr("class", "matchtree-glyph-ring gain")
          .attr("transform", d => "translate(" + xPos(d) + ", " + yPos(d) + ")")
          .attr("fill", o.gainColor)
          .attr("d", d => arc({startAngle: 0, endAngle: 0})),
        update => update
          .transition().duration(300)
          .attrTween("d", arcTweenGain)
      )

    gLoss.selectAll(".matchtree-glyph-ring.loss")
      .data(data)
      .join(
        enter => enter
          .append("path")
          .attr("class", "matchtree-glyph-ring loss")
          .attr("transform", d => "translate(" + xPos(d) + ", " + yPos(d) + ")")
          .attr("fill", o.lossColor)//"rgb(241,197,197)")
          .attr("d", d => arc({startAngle: 0, endAngle: 0})),
        update => update
          .transition().duration(300)
          .attrTween("d", arcTweenLoss)
      )

    function arcTweenNeutral(d) {
      //http://bl.ocks.org/mbostock/5100636  --  but one nesting too much
      var tagPath = tagify(d.path);
      var dPreviousView = dataPreviousView.filter(e => tagify(e.path) == tagPath)[0];
      var newAngle = 2 * Math.PI * (d.count+d.childCounts)/max;
      var oldAngle = 2 * Math.PI * (dPreviousView.count+dPreviousView.childCounts)/oldmax;
      if (collapseMode == "collapse") {
        // We are collapsing. The bedinning old angle should not include the child counts yet.
        if (tagify(d.path) == collapsePath)
          oldAngle = 2 * Math.PI * (dPreviousView.countOld)/oldmax;
      }
      if (collapseMode == "uncollapse") {
        // We are uncollapsing. Since the old childCounts info is already reset
        // for the uncollapsed node, jump right to the end view instead of swriling around misleadingly
        if (tagify(d.path) == collapsePath)
          oldAngle = newAngle;
      }
      var interpolate = d3.interpolate(oldAngle, newAngle);
      return function(t) {
        var endAngle = interpolate(t);
        return arc({startAngle: 0, endAngle: endAngle});
      };
    }

    function arcTweenGain(d) {
      //http://bl.ocks.org/mbostock/5100636  --  but one nesting too much
      var oldCount = d.countOld+d.childCountsOld;
      var newCount = d.count+d.childCounts;
      var interpolateCount, interpolateMax;
      if (newCount > oldCount) {
        // Start off with new count here if only (un)collapsed to avoid too much movement in view
        // + Jump with counts of main node if (un)collapse happens because evething else would be mental, would need dPreviousView again
        if (collapseMode != "none")
          interpolateCount = d3.interpolate(newCount, newCount);
        else
          interpolateCount = d3.interpolate(oldCount, newCount);
        interpolateMax = d3.interpolate(oldmax, max);
      }
      else {
        interpolateCount = e => oldCount;
        interpolateMax = e => max;
      }
      return function(t) {
        var iCount = interpolateCount(t);
        var iMax = interpolateMax(t);
        return arc({startAngle: 2*Math.PI*oldCount/iMax, endAngle: 2*Math.PI*iCount/iMax});

      };
    }

    function arcTweenLoss(d) {
      //http://bl.ocks.org/mbostock/5100636  --  but one nesting too much
      // Important: Currently cutting off whatever exceeds the current scale implied by max and placing a dot.
      // This way, the loss arc might display less loss than was actually lost.
      // Questionable decision, but otherwise, we would have to drag the oldmax into the new view if it is
      // larger than max to put the glphys into relation, but that would mean potentially not using the
      // full range of colors for the circle glyph as the (old)max value would not be reached by and node
      if (d.countOld+d.childCountsOld > max) {
        // Get information whether corresponding glyph is currently visible or not (due to collapse)
        var dispVal = svg.selectAll(".matchtree-main-glyph").filter(e => tagify(e.path) == tagify(d.path)).attr("display");
        dispVal = dispVal == null ? "block" : dispVal;
        // Need to add it that complicatedly to prevent balls from showng up when collapsing
        var lossBall = gLoss.selectAll(".matchtree-loss-ball").filter(e => tagify(e.path) == tagify(d.path))
          .data([{path: d.path}])
          .enter()
          .append("circle")
          .attr("id", "matchtree-loss-ball-"+d.path.reduce((a,b) => a+b))
          .attr("class", "matchtree-loss-ball")
          .attr("cx", xPos(d))
          .attr("cy", yPos(d)-(o.arcInnerRadius+o.circleRadius)/2)
          .attr("r", o.highlightThickness*2)
          .attr("fill", o.lossColor)
          .attr("display", dispVal);
      }
      else {
        // Preemptively remove leftover loss balls that are not relevant anymore
        gLoss.selectAll(".matchtree-loss-ball").filter(e => tagify(e.path) == tagify(d.path)).remove();
      }
      var oldCount = d3.min([d.countOld+d.childCountsOld, max]);
      var newCount = d.count+d.childCounts;
      var interpolateCount, interpolateMax;
      if (newCount < oldCount) {
        // Start off with new count here if only (un)collapsed to avoid too much movement in view
        // + Jump with counts of main node if (un)collapse happens because evething else would be mental, would need dPreviousView again
        if (collapseMode != "none")
          interpolateCount = d3.interpolate(newCount, newCount);
        else
          interpolateCount = d3.interpolate(oldCount, newCount);
        interpolateMax = d3.interpolate(oldmax, max);
      }
      else {
        interpolateCount = e => oldCount;
        interpolateMax = e => max;
      }
      return function(t) {
        var iCount = interpolateCount(t);
        var iMax = interpolateMax(t);
        return arc({startAngle: 2*Math.PI*iCount/iMax, endAngle: 2*Math.PI*oldCount/iMax});
      };
    }

    svg.selectAll(".matchtree-glyph-ring").attr("pointer-events", "none");
  };

  chart.mouseInLevelIndicator = function(node, d) {
    if (node.classed("matchtree-main-glyph")) {
      svg.selectAll(".matchtree-level-indicator-g")
        .append("circle")
        .attr("fill", "none")
        .attr("stroke", "rgb(105,105,105)")
        .attr("cx", o.w/2-40)
        .attr("cy", o.h/2)
        .attr("r", rScale(d))
        .style("stroke-dasharray", 10);
      // If the current node is selected in Filter mode, respect its larger size
      var offsetSelected = 0;
      if (node.classed("selected")) offsetSelected = o.highlightThickness;

      // Determine category name (adjust if dimension is "dataset")
      var categoryName;
      switch(d.path[d.path.length-1]) {
        case "G": categoryName="GED"; break;
        case "A": categoryName="ACLED"; break;
        case "GT": categoryName="GTD"; break;
        case "S": categoryName="SCAD"; break;
        default: categoryName = d.path[d.path.length-1];
      }

      placeHoverLabel(xPos(d), yPos(d)-18, "Category: "+categoryName, "category");
      placeHoverLabel(xPos(d), yPos(d)+18, "Count: "+(d.count+d.childCounts)+" ("+Math.round((d.count+d.childCounts)/max*100)+"%)", "count", "top");
    }
  };

  chart.mouseInHightlight = function(node, d) {
    if (node.classed("matchtree-main-glyph")) {
      node
        .attr("r", o.circleRadius + o.highlightThickness/2)
        .attr("stroke-width", o.highlightThickness);
    }
    else {
      node
        .attr("r", 7 + o.highlightThickness/2)
        .attr("stroke-width", o.highlightThickness);
    }
  }

  chart.mouseInHightlightLines = function(node, d) {
    if (node.classed("matchtree-main-glyph")) {
      highlightAncestorLines(d.dim, d.path);
    }
    else {
      // d is dim
      highlightAncestorLines(d);
    }
  }

  chart.selectNodes = function(selection) {
    selection
      .classed("selected", true)
      .attr("r", o.circleRadius + o.highlightThickness)
      .attr("stroke-width", 2 * o.highlightThickness);
  };

  chart.deselectNodes = function(selection) {
    if (selection.classed("matchtree-main-glyph")) {
      selection
        .classed("selected", false)
        .attr("r", o.circleRadius)
        .attr("stroke-width", 0);
    }
    else {
      selection
        .attr("r", 7)
        .attr("stroke-width", 0);
    }
  };

  chart.mouseOutFilter = function(node) {
    if (node.classed("selected")) {
      chart.selectNodes(node);
    }
    else {
      // Is already deselected, this is just for the appearance
      chart.deselectNodes(node);
    }
    svg.selectAll(".matchtree-level-indicator-g")
      .selectAll("circle").remove();
    svg.selectAll(".matchtree-label-g").remove();
    svg.selectAll(".matchtree-lines")
      .style("stroke", o.bgLineGlyphColor);
  };

  chart.clickFilter = function(node) {
    if (!node.classed("selected")) {
      chart.selectNodes(node);
    }
    else {
      chart.deselectNodes(node);
    }
  };

  chart.clickFilterDim = function(dim) {
    var dimNodes = svg.selectAll(".matchtree-main-glyph")
      .filter(d => d.dim == dim);
    var dimNodesSelected = svg.selectAll(".matchtree-main-glyph.selected")
      .filter(d => d.dim == dim);
    if (dimNodesSelected.size() < dimNodes.size()) {
      chart.selectNodes(dimNodes);
    }
    else {
      chart.deselectNodes(dimNodes);
    }
  };

  chart.enterFilterMode = function() {
    svg.selectAll(".matchtree-main-glyph").call(chart.selectNodes);
  };

  chart.exitFilterMode = function() {
    svg.selectAll(".matchtree-main-glyph").call(chart.deselectNodes);
  };

  chart.clickStructure = function(node, datum) {
    var parentPath = tagify(datum.path);
    // Get all child elements
    var childElements = svg.selectAll("*").filter(d => {
      // Some props do not have path data at all
      if (typeof d.path !== "undefined") {
        var childPath = tagify(d.path);
        // Ensure that their path contains the parent path but is not the same
        return childPath.startsWith(parentPath) && childPath !== parentPath;
      }
      else {
        return false;
      }
    });

    // Only collapse if there are child elements
    if (childElements.size() > 0) {
      // If a child node was collapsed already, reset it to uncollapsed
      childElements.each(function(d) {
        d.childCounts = 0;
        d3.select(this).classed("collapsed", false);
      });

      if (node.classed("collapsed")) {
        // Uncollapse
        node.classed("collapsed", false);
        childElements.attr("display", "block")
        node.datum().childCounts = 0;
        node.datum().childCountsOld = 0;
        // Update arcs. Additionally pass path that was just collapsed to transition corresponding arc appropriately
        chart.updateGlyphsArc(svg.selectAll(".matchtree-main-glyph").data(), "uncollapse", parentPath);
      }
      else {
        // Collapse
        node.classed("collapsed", true);
        childElements.attr("display", "none")
        // Get sum of the child elements' counts and update datum
        node.datum().childCounts = getChildCounts(parentPath);
        node.datum().childCountsOld = getChildCountsOld(parentPath);
        // Update arcs
        chart.updateGlyphsArc(svg.selectAll(".matchtree-main-glyph").data(), "collapse", parentPath);
      }
      // Update circles
      chart.updateGlyphsCircle(svg.selectAll(".matchtree-main-glyph").data());
      // Update hover label text
      d3.select(".matchtree-label-text.count")
        .text("Count: "+(+node.datum().count+(+node.datum().childCounts))+" ("+Math.round((+node.datum().count+(+node.datum().childCounts))/max*100)+"%)");
    }
  };

  chart.getFilterExclusionConstraints = function() {
    var emptyDims = [];
    // For each dimension, check whether it has an empty selection
    dimensions.forEach(dim => {
      var nrDimSelections = svg.selectAll(".matchtree-main-glyph.selected").filter(d => d.dim == dim).size();
      if (nrDimSelections == 0) {
        emptyDims.push(dim);
      }
    });
    if (emptyDims.length > 0) {
      alert('Please select at least one category to filter for for the attribute(s) "'+emptyDims.reduce((a,b) => a+", "+b)+'".');
      return null;
    }
    else {
      // This data contains all categories that should NOT be included in the filter results
      // Nodes that are not collapsed
      var filterExclusionData = svg.selectAll(".matchtree-main-glyph:not(.selected):not(.collapsed)").data();
      // Nodes that are collapsed
      var filterExclusionDataCollapsed = svg.selectAll(".matchtree-main-glyph.collapsed:not(.selected)").data();
      filterConstraints = {};
      // For each dimension, create array
      dimensions.forEach(dim => {
        filterConstraints[dim] = [];
      });
      // For each datum, enter its path to the filter constraints
      filterExclusionData.forEach(d => {
        filterConstraints[d.dim].push(d.path);
      });
      // If the corresponding node is collapsed, remove all of its ancestor nodes as well
      var allPaths = svg.selectAll(".matchtree-main-glyph").data().map(d => d.path);
      filterExclusionDataCollapsed.forEach(d => {
        // Get pathtag of collapsed node
        var pathtag = tagify(d.path);
        // Get all ancestor paths
        var ancestorPaths = allPaths.filter(p => (tagify(p).startsWith(pathtag)));
        ancestorPaths.forEach(p => {
          // Add nodes' path to removal list
          filterConstraints[d.dim].push(p);
        });
        // Since all of the child nodes are filtered out as well, update the child count to 0

      });
      return filterConstraints;
    }
  }

  chart.w = function(value) {
      if (!arguments.length) return o.w;
      o.w = value;
      updateRadius();
      return chart;
  };
  chart.h = function(value) {
      if (!arguments.length) return o.h;
      o.h = value;
      updateRadius();
      return chart;
  };
  chart.margin = function(value) {
      if (!arguments.length) return o.margin;
      o.margin = value;
      updateRadius();
      return chart;
  };
  chart.taxonomies = function(value) {
      if (!arguments.length) return o.taxonomies;
      o.taxonomies = value;
      return chart;
  };
  chart.dimPadding = function(value) {
      if (!arguments.length) return o.dimPadding;
      o.dimPadding = value;
      return chart;
  };
  chart.radiusInner = function(value) {
      if (!arguments.length) return o.radiusInner;
      o.radiusInner = value;
      return chart;
  };
  chart.radiusOuter = function(value) {
      if (!arguments.length) return o.radiusOuter;
      o.radiusOuter = value;
      return chart;
  };
  chart.arcInnerRadius = function(value) {
      if (!arguments.length) return o.arcInnerRadius;
      o.arcInnerRadius = value;
      return chart;
  };
  chart.circleRadius = function(value) {
      if (!arguments.length) return o.circleRadius;
      o.circleRadius = value;
      return chart;
  };

  return chart;
}
