function paraMultiples() {
  /*
  This function returns the object for drawing the ParaMultiples component
  */
  var o, fn, svg, projection, path, dim, simWidthScale, getParas, getColorScale, getDissimilarities,
  translateSub, xSim, ySim, wSim, hSim, colTicks, rowTicks; // options, (helper)functions

  o = {
    w: 0,
    h: 0,
    margin: {x: 20, y: 20, outer: 20}
  }

  // Helper functions
  getParas = function(paraCombString) {
    // @desc: Extract parameters as object
    var sIdx = paraCombString.indexOf("s");
    var tIdx = paraCombString.indexOf("t");
    return {s: +paraCombString.slice(sIdx+1, tIdx), t: +paraCombString.slice(tIdx+1, paraCombString.length)}
  }

  getDissimilarities = function(node) {
    // @desc: Extract dissimilarity scores for node
    // Determine at which position in the grid the current node resides
    var s = node[0].s;
    var t = node[0].t
    // Filter for the row where the first entry is the current paracomb and the second one has a larger Delta s
    // than the current paracomb. Undefined if it does not exist (border cases)
    var dissimilarities = [];
    var rightNeighbor = dissimData.find(d => (d.first=="s"+s+"t"+t && s < getParas(d.second).s));
    if (typeof rightNeighbor !== "undefined") {
      var dissim = rightNeighbor.dissimilarity;
      dissimilarities.push({orientation: "right", value: dissim});
    }
    // Filter for the row where the first entry is the current paracomb and the second one has a larger Delta t
    // than the current paracomb. Undefined if it does not exist (border cases)
    var bottomNeighbor = dissimData.find(d => (d.first=="s"+s+"t"+t && t < getParas(d.second).t));
    if (typeof bottomNeighbor !== "undefined") {
      var dissim = bottomNeighbor.dissimilarity;
      dissimilarities.push({orientation: "bottom", value: dissim});
    }
    return dissimilarities;
  };

  getColorScale = function() {
    // @desc: Set up color scale function
    var max = 0;
    var cScale = function(count) {
      return `rgb(${0 + count/max*(0-0)}, ${0 + count/max*(204-0)}, ${0 + count/max*(255-0)})`;
    }
    // Allows to adjust maximum afterwards
    cScale.max = function(value) {
        if (!arguments.length) return max;
        max = value;
        return cScale;
    };
    return cScale;
  };

  translateSub = function(key, direction) {
    // @desc: Calculate how far to shift a subview in the given 'direction' depending on its 'key'
    var sStart = key.indexOf("s");
    var tStart = key.indexOf("t");
    // val is the value of the considered tick
    // lookup is the list of ticks in which the val should have a specific position
    var val, lookup;
    if (direction == "x") {
      val = +key.slice(sStart+1, tStart);
      lookup = colTicks;
    }
    else if (direction == "y") {
      val = +key.slice(tStart+1, key.length);
      lookup = rowTicks;
    }
    else {
      console.log("Invalid direction: " + direction);
    }
    // The how manyth subview is the current one?
    var idxInLookup = lookup.indexOf(val);
    return o.margin.outer + o.margin[direction] + idxInLookup * (o.margin[direction] + dim);
  };

  xSim = function(sim) {
    // @desc: Determine x position of similarity bar 'sim'
    if (sim.orientation == "bottom") return dim/2 - simWidthScale(sim.value)/2;
    else return dim/2
  };

  ySim = function(sim) {
    // @desc: Determine y position of similarity bar 'sim'
    if (sim.orientation == "right") return dim/2 - simWidthScale(sim.value)/2;
    else return dim/2
  };

  wSim = function(sim) {
    // @desc: Determine width of similarity bar 'sim'
    if (sim.orientation == "bottom") return simWidthScale(sim.value);
    else return dim + o.margin.x;
  };

  hSim = function(sim) {
    // @desc: Determine height of similarity bar 'sim'
    if (sim.orientation == "right") return simWidthScale(sim.value);
    else return dim + o.margin.y;
  };


  // Define the rendering
  function chart(selection) {
    // @desc: Draw the initial plot
    selection.each(function(data, idx) {

      if (!data) {return;}

      // Select the svg element
      svg = d3.select(this);
      o.w = svg.attr("width");
      o.h = svg.attr("height");

      paraData = data.paraData;
      dissimData = data.dissimData;

      console.log(paraData);
      console.log(dissimData);
      // Get the ticks for the grid, colTicks refers to s, rowTicks to t
      colTicks = [...new Set(paraData.map(x => x.s))];
      rowTicks = [...new Set(paraData.map(x => x.t))];

      // Nest paraData by parameter combination.
      var combinations = d3.nest()
        .key(function(d) { return "s" + d.s + "t" + d.t; })
        .entries(paraData);

      combinations.forEach(d => d["dissimilarities"] = getDissimilarities(d.values))
      // Determine the width and height of each small multiple
      var wSub = Math.floor((o.w - ((colTicks.length+1)*o.margin.x + 2 * o.margin.outer)) / colTicks.length);
      var hSub = Math.floor((o.h - ((rowTicks.length+1)*o.margin.y + 2 * o.margin.outer)) / rowTicks.length);
      // The final height/width 'dim' is the minimum as the subview should be quadratic
      dim = Math.min(wSub, hSub)

      var xScale = d3.scaleLinear()
          .domain([0, 1])
          .range([0, dim]);

      var yScale = d3.scaleLinear()
          .domain([0, d3.max(paraData, d => d.y)])
          .range([dim, 0]);

      var cScale = getColorScale();
      // Update colorscale to new maxium
      cScale.max(d3.max(combinations, d => d3.max(d.values, e => e.count)));
      // Determine the limits for how small/big the dissimilarity bars can get
      simWidthScale = d3.scaleLinear()
          .domain([0, d3.max(combinations, d => d3.max(d.dissimilarities, e => e.value))]) // TBD can go over combinations -> dissimilarities
          .range([2, dim/2]);

      // Create the bars depicting the dissimilarites
      var svgSims = svg.append("svg").selectAll("g")
          .data(combinations)
          .enter().append("g")
            .attr("class", "paramultiple-sim-g")
            .attr("transform", d => "translate(" + translateSub(d.key, "x") + "," + translateSub(d.key, "y") + ")")
            .selectAll("rect")
            .data(d => d.dissimilarities)
            .enter().append("rect")
              .attr("x", d => xSim(d))
              .attr("y", d => ySim(d))
              .attr("width", d => wSim(d))
              .attr("height", d => hSim(d))
              .attr("fill", "rgba(158, 102, 134, 0.4)")
              .attr("stroke", "rgb(158, 102, 134)");

      // Add an SVG element for each symbol, with the desired dimensions and margin.
      var svgsSub = svg.selectAll("g").filter(function() { return !this.classList.contains('paramultiple-sim-g'); })
        .data(combinations)
        .enter().append("g")
          .attr("transform", d => "translate(" + translateSub(d.key, "x") + "," + translateSub(d.key, "y") + ")")
          .append("svg")
          .attr("width", dim)
          .attr("height", dim);

      // Create background for each small multiple
      svgsSub.append("rect")
          .attr("class", "paramultiple-frame-background")
          .attr("x", 0)
          .attr("y", 0)
          .attr("width", dim)
          .attr("height", dim)
          .attr("fill", d3.select(":root").style("--compenent-bg-color"));

      // Create the bar for each bin of each histogram
      svgsSub.selectAll(".hist-rect")
        .data(d => d.values)
        .join(
          enter => enter.append("rect")
            .attr("class", ".hist-rect")
            .attr("x", d => xScale(d.x))
            .attr("y", d => yScale(d.y))
            .attr("width", xScale(0.1)-xScale(0)) // bandwidth of 0.1
            .attr("height", d => yScale(0)-yScale(d.y))
            .attr("fill", d => cScale(d.count))
        );

      // Create frame for each small multiple
      svgsSub.append("rect")
          .attr("class", "paramultiple-frame")
          .attr("x", 0)
          .attr("y", 0)
          .attr("width", dim)
          .attr("height", dim)
          .attr("fill", "rgba(0, 0, 0, 0)")
          .attr("stroke", "rgb(220, 220, 220)");

      // Add axes
      var labelPadding = 5;
      var tickLength = 5;
      var fontSize = 6;

      // Group to append labels to
      var labelG = svg.append("g").attr("class", "paramultiples-label-g")
      // Set up col-axis ticks
      labelG.selectAll(".paramultiples-col-axis-ticks")
        .data(colTicks)
        .enter().append("line")
        .attr("class", "paramultiples-col-axis-ticks")
        .attr("x1", (d,i) => i*(dim+o.margin.x) + dim/2 + o.margin.x + o.margin.outer)
        .attr("y1", o.margin.outer - tickLength/2)
        .attr("x2", (d,i) => i*(dim+o.margin.x) + dim/2 + o.margin.x + o.margin.outer)
        .attr("y2", o.margin.outer + tickLength/2)
        .attr("stroke", "rgb(220, 220, 220)");

      // Set up row-axis ticks
      labelG.selectAll(".paramultiples-row-axis-ticks")
        .data(rowTicks)
        .enter().append("line")
        .attr("class", "paramultiples-row-axis-ticks")
        .attr("x1", o.margin.outer - tickLength/2)
        .attr("y1", (d,i) => i*(dim+o.margin.y) + dim/2 + o.margin.y + o.margin.outer)
        .attr("x2", o.margin.outer + tickLength/2)
        .attr("y2", (d,i) => i*(dim+o.margin.y) + dim/2 + o.margin.y + o.margin.outer)
        .attr("stroke", "rgb(220, 220, 220)")

      // Define what the arrow head of the axis lines should look like
      labelG.append("svg:defs").append("svg:marker")
        .attr("id", "arrow")
        .attr("viewBox","0 -5 10 10")
        .attr("refX", 5)
        .attr("refY", 0)
        .attr("markerWidth", 8)
        .attr("markerHeight", 8)
        .attr("orient", "auto")
        .append("path")
        .attr("d", "M0,-5L10,0L0,5")
        .style("stroke", "rgb(220, 220, 220)")
        .style("fill", "rgb(220, 220, 220)");

      // Create x-axis line
      labelG.append("line")
        .attr("id", "paramultiples-x-axis-line")
        .attr("x1", o.margin.outer + tickLength/2)
        .attr("y1", o.margin.outer + tickLength/2)
        .attr("x2", (colTicks.length)*(dim + o.margin.x) + o.margin.outer)
        .attr("y2", o.margin.outer + tickLength/2)
        .attr("stroke", "rgb(220, 220, 220)")
        .attr("marker-end", "url(#arrow)");

      // Create y-axis line
      labelG.append("line")
        .attr("id", "paramultiples-y-axis-line")
        .attr("x1", o.margin.outer + tickLength/2)
        .attr("y1", o.margin.outer + tickLength/2)
        .attr("x2", o.margin.outer + tickLength/2)
        .attr("y2", (rowTicks.length)*(dim + o.margin.y) + o.margin.outer)
        .attr("stroke", "rgb(220, 220, 220)")
        .attr("marker-end", "url(#arrow)");

      // Create col-axis tick labels
      labelG.selectAll(".paramultiples-col-ticks")
        .data(colTicks)
        .enter().append("text")
        .attr("class", "paramultiples-col-ticks")
        .text(d => d)
        .attr("transform", (d,i) => `translate(${i*(dim+o.margin.x) + dim/2 + o.margin.x + o.margin.outer},${-labelPadding + o.margin.outer})`)
        .attr("fill", "rgb(220, 220, 220)")
        .attr("text-anchor", "middle")

      // Create row-axis tick labels
      labelG.selectAll(".paramultiples-row-ticks")
        .data(rowTicks)
        .enter().append("text")
        .attr("class", "paramultiples-row-ticks")
        .text(d => d)
        .attr("transform", (d,i) => `translate(${-labelPadding + o.margin.outer},${i*(dim+o.margin.y) + dim/2 + o.margin.y + fontSize + o.margin.outer})`)
        .attr("fill", "rgb(220, 220, 220)")
        .attr("text-anchor", "end");

      // Identifier for the col-axis, a.k.a. "s"
      labelG.append("text")
        .text("\u0394" + "s" + " [km]")
        .attr("transform", `translate(${(colTicks.length)*(dim + o.margin.x) + o.margin.outer + 2 * fontSize}, ${o.margin.outer - tickLength/2 + fontSize})`)
        .attr("fill", "rgb(220, 220, 220)")
        .attr("font-weight", "bold")
        .attr("text-anchor", "start");

      // Identifier for the row-axis, a.k.a. "t"
      labelG.append("text")
        .text("\u0394" + "t" + " [d]")
        .attr("transform", `translate(${o.margin.outer + tickLength/2}, ${Math.min((rowTicks.length)*(dim + o.margin.y) + o.margin.outer + 5 * fontSize, o.h-2)})`)
        .attr("fill", "rgb(220, 220, 220)")
        .attr("font-weight", "bold")
        .attr("text-anchor", "middle");

      // Create x-axis min tick labels
      labelG.selectAll(".paramultiple-x-min-ticks")
        .data(colTicks)
        .enter().append("text")
        .attr("class", "paramultiple-x-min-ticks")
        .text("0")
        .attr("transform", (d,i) => `translate(${i*(dim+o.margin.x) + o.margin.x + o.margin.outer},${rowTicks.length*(dim+o.margin.y) + o.margin.outer + fontSize + 2* labelPadding})`)
        .attr("fill", "rgb(120, 120, 120)")
        .attr("text-anchor", "middle")
      // Create x-axis max tick labels
      labelG.selectAll(".paramultiple-x-max-ticks")
        .data(colTicks)
        .enter().append("text")
        .attr("class", "paramultiple-x-max-ticks")
        .text("1")
        .attr("transform", (d,i) => `translate(${i*(dim+o.margin.x) + dim + o.margin.x + o.margin.outer},${rowTicks.length*(dim+o.margin.y) + o.margin.outer + fontSize + 2* labelPadding})`)
        .attr("fill", "rgb(120, 120, 120)")
        .attr("text-anchor", "middle")

      // Create y-axis min tick labels
      labelG.selectAll(".paramultiple-y-min-ticks")
        .data(rowTicks)
        .enter().append("text")
        .attr("class", "paramultiple-y-min-ticks")
        .text("0")
        .attr("transform", (d,i) => `translate(${colTicks.length*(dim+o.margin.x) + 0.5 * o.margin.outer + 2.5 * labelPadding},${i*(dim+o.margin.y) + dim + o.margin.y + fontSize + o.margin.outer})`)
        .attr("fill", "rgb(120, 120, 120)")
        .attr("text-anchor", "start");

      // Create y-axis max tick labels
      labelG.selectAll(".paramultiple-y-max-ticks")
        .data(rowTicks)
        .enter().append("text")
        .attr("class", "paramultiple-y-max-ticks")
        .text(Math.round((yScale.domain()[1] + Number.EPSILON) * 100) / 100) // Round for 2 decimal positions
        .attr("transform", (d,i) => `translate(${colTicks.length*(dim+o.margin.x) + 0.5 * o.margin.outer + 2.5 * labelPadding},${i*(dim+o.margin.y) + o.margin.y + fontSize + o.margin.outer})`)
        .attr("fill", "rgb(120, 120, 120)")
        .attr("text-anchor", "start");

      // Identifier for the x-axis, a.k.a. "Match Quality"
      labelG.append("text")
        .text("Match Score")
        .attr("transform", `translate(${colTicks.length*(dim+o.margin.x)/2 + o.margin.x/2 + o.margin.outer}, ${rowTicks.length*(dim+o.margin.y) + 2 * o.margin.outer + fontSize + 2* labelPadding})`)
        .attr("fill", "rgb(120, 120, 120)")
        .attr("text-anchor", "middle");

      // Identifier for the row-axis, a.k.a. "Relative Amount of Matches"
      labelG.append("text")
        .text("Relative Amount of Matches")
        .attr("transform", `translate(${colTicks.length*(dim+o.margin.x) + 3 * o.margin.outer + 3 * labelPadding}, ${rowTicks.length*(dim+o.margin.y)/2 + o.margin.y/2 + o.margin.outer}) rotate(270)`)
        .attr("fill", "rgb(120, 120, 120)")
        .attr("text-anchor", "middle");

      // Set up the  legends
      setUpCountLegend(combinations);
      setUpDissimilarityLegend(combinations);
    });
  }

  function setUpCountLegend(combinations) {
    // @desc: Set up the count legend
    var legendG = svg.append("g")
      .attr("class", "paramultiples-legend-count-g")
      .attr("transform", `translate(700, ${2*o.margin.outer})`);
    // Create gradient from min color to max color
    var gradient = legendG.append('defs').append('linearGradient')
        .attr('id', 'paramultiples-legend-gradient')
        .attr('x1', '0%') // bottom
        .attr('y1', '100%')
        .attr('x2', '0%') // to top
        .attr('y2', '0%')
        .attr('spreadMethod', 'pad');
    gradient.append("stop").attr('offset', '0').attr("stop-color", "rgb(0,0,0)");
    gradient.append("stop").attr('offset', '1').attr("stop-color", "rgb(0,204,255)");
    // Create the visual components of the legend
    var barWidth = 20;
    var barHeight = 80;
    var yUpper = 20;
    var yLower = barHeight+yUpper;
    // Legend header
    legendG.append("text")
      .attr("x", -barWidth)
      .attr("y", 6)
      .attr("fill", "rgb(220,220,220)")
      .attr("font-weight", "bold")
      .text("Count:");
    // Color rect
    legendG.append("rect")
      .attr("x", 0)
      .attr("y", yUpper)
      .attr("height", barHeight)
      .attr("width", barWidth)
      .attr("fill", "url(#paramultiples-legend-gradient)")
      .attr("stroke", "rgb(220, 220, 220)");
    // Max line
    legendG.append("line")
      .attr("x1", barWidth)
      .attr("y1", yUpper)
      .attr("x2", barWidth+7)
      .attr("y2", yUpper)
      .attr("stroke", "rgb(220, 220, 220)");
    // Min line
    legendG.append("line")
      .attr("x1", barWidth)
      .attr("y1", yLower)
      .attr("x2", barWidth+7)
      .attr("y2", yLower)
      .attr("stroke", "rgb(220, 220, 220)");
    // Max Count text
    legendG.append("text")
      .attr("x", barWidth+10)
      .attr("y", yUpper+6)
      .attr("fill", "rgb(220,220,220)")
      .text(d3.max(combinations, d => d3.max(d.values, e => e.count)));
    // Min Count text
    legendG.append("text")
      .attr("x", barWidth+10)
      .attr("y", yLower+6)
      .attr("fill", "rgb(220,220,220)")
      .text("0");
  }

  function setUpDissimilarityLegend(combinations) {
    // @desc: Set up dissimilarity legend
    var legendG = svg.append("g")
      .attr("class", "paramultiples-legend-dissimilarity-g")
      .attr("transform", `translate(700, ${2*o.margin.outer+o.h/2})`);
    // Create the visual components of the legend
    var barWidth = 20;
    var yUpper = 20;
    var yLower = 40;
    // Legend header
    legendG.append("text")
      .attr("x", -barWidth)
      .attr("y", 6)
      .attr("fill", "rgb(220,220,220)")
      .attr("font-weight", "bold")
      .text("Dissimilarity:");
    // Min dissimilarity rect
    legendG.append("rect")
      .attr("x", 0)
      .attr("y", yUpper)
      .attr("height", 2)
      .attr("width", barWidth)
      .attr("fill", "rgba(158, 102, 134, 0.4)")
      .attr("stroke", "rgb(158, 102, 134)");
    // Max dissimilarity rect
    legendG.append("rect")
      .attr("x", 0)
      .attr("y", yLower)
      .attr("height", simWidthScale.range()[1])
      .attr("width", barWidth)
      .attr("fill", "rgba(158, 102, 134, 0.4)")
      .attr("stroke", "rgb(158, 102, 134)");
    // Min label text
    legendG.append("text")
      .attr("x", barWidth+10)
      .attr("y", yUpper+6)
      .attr("fill", "rgb(220,220,220)")
      .text("none");
    // Max label text
    legendG.append("text")
      .attr("x", barWidth+10)
      .attr("y", simWidthScale.range()[1]/2+yLower+6)
      .attr("fill", "rgb(220,220,220)")
      .text("max.");
  }

  chart.highlight = function(d) {
    // @desc: Draw frame around a single subview
    var thisFrame = d3.select(d);
    if (!thisFrame.classed("selected"))
      d3.select(d).attr("stroke-width", 3);
  }

  chart.click = function(d) {
    // @desc: Create lasting thicker frame around single subview
    var allFrames = d3.selectAll(".paramultiple-frame");
    var thisFrame = d3.select(d);
    if (!thisFrame.classed("selected")) {
      // Highlight and select this frame and lowlight all others
      allFrames.classed("selected", false);
      thisFrame.classed("selected", true);
      allFrames.attr("stroke-width", 1);
      thisFrame.attr("stroke-width", 6);
    }
    else {
      // Deselect this frame
      thisFrame.classed("selected", false);
      thisFrame.attr("stroke-width", 3);
    }
  }

  chart.lowlight = function(d) {
    // @desc: Remove thick frame from a single subview
    var thisFrame = d3.select(d);
    if (!thisFrame.classed("selected"))
      thisFrame.attr("stroke-width", 1);
  }

  // Allow external setting and getting of the options 'o'
  chart.w = function(value) {
      if (!arguments.length) return o.w;
      o.w = value;
      return chart;
  };
  chart.h = function(value) {
      if (!arguments.length) return o.h;
      o.h = value;
      return chart;
  };
  chart.margin = function(value) {
      if (!arguments.length) return o.margin;
      o.margin = value;
      return chart;
  };

  return chart;
}
