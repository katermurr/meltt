function filterFunctions() {
  var o = {
    testval: "",
    minDate: null,
    maxDate: null,
    scale: 0,
    centerLon: 0,
    centerLat: 0,
    nrBins: 0,
    taxonomies: null
  };

  var tagify = function(array) {
    return array.reduce((a,b) => a + "#" + b) + "#";
  };

  var inBinRange = function(val, intervalString) {
    // Split the interval string to check if val lies inbetween its bounds
    var sepIdx = intervalString.indexOf(",");
    var lowerBound = +intervalString.slice(1, sepIdx);
    var upperBound = +intervalString.slice(sepIdx+1, intervalString.length-1);
    if (intervalString[intervalString.length-1] == "]") {
      // Last bin should include the upper bound
      return (lowerBound <= val && val <= upperBound)
    }
    else {
      // Upper bound should be excluded
      return (lowerBound <= val && val < upperBound)
    }
  };

  var getFilterFnArray = function(filterConstraints) {
    // Each filterFn in the filterFnArray returns true if filterconstraint i does not exclude d from the final filter result
    // since the filterConstraints are exclusionary, meaning that if they are met, the element should NOT be included in the filter result
    var filterFnArray = [];
    _.each(filterConstraints, (pathArray, dim) => {
      if (pathArray.length > 0) {
        pathArray.forEach(path => {
          var filterFn;
          if (dim == "dataset") {
            // Filter for matches that do not have the corresponding dataset either in their primary or duplicate event
            filterFn = d => (d[dim] != path && d["duplicate_"+dim] != path);
          }
          else if (dim == "size") {
            // Filter for matches that do not have the corresponding length/size
            filterFn = d => d[dim] != path;
          }
          else if (dim == "score") {
            // Filter for matches whose score does not fall into the corresponding bin given by path string
            filterFn = d => (!(inBinRange(d[dim], path[0])));
          }
          else {
            // Just filter for matches not having the current path
            filterFn = d => d[dim+"_path"] != tagify(path);
          }
          filterFnArray.push(filterFn);
        });
      }
    });

    return filterFnArray;
  }



  // To determine into which bin each event falls

  var operations = {};

  operations.testfunc = function(arg) {
    return arg==o.testval;
  }

  operations.binScale = d3.scaleTime()
    .domain([o.minDate, o.maxDate])
    .range([0, o.nrBins]);


  // Returns the number of the bin for each event, however, with an extra micro bin
  // in the end for maxDate
  operations.binNr = function(date) {
    return Math.floor(operations.binScale(date));
  };

  // Check if point d is inside the bounding box
  operations.inBbox = function(bbox, d){
    return (d.longitude >= bbox.minLon &&
            d.latitude >= bbox.minLat &&
            d.longitude <= bbox.maxLon &&
            d.latitude <= bbox.maxLat)
  };

  operations.inPolygon = function(polygon, point) {
    // TBD need some extra handling for polygons inside of polygons. Prolly add
    // another polygonType and adjust the json data accordingly
    // Check if point inside of the main polygon
    var pointContained = d3.polygonContains(polygon[0],[point[0],point[1]]);
    if (polygon.length > 1) {
      // Case: There are holes in the polygon
      for (var i = 1; i < polygon.length; i++) {
        // If point lies within a hole, it is not contained in main polygon
        if (d3.polygonContains(polygon[i][0],[point[0],point[1]]))
        pointContained = false;
      }
    }
    return pointContained;
  };

  // Extract Bounding box of a given (Multi)Polygon
  operations.getPolygonBbox = function(polygons, polygonType) {
    var bbox = {minLon: 90, minLat: 180, maxLon: -90, maxLat: -180};
    if (polygonType==="Polygon") {
      // If new value is smaller/greater than the current min/max, replace the current value
      var newVal = d3.min(polygons[0], d => d[0]); // d[0] corresponds to longitude
      if (newVal < bbox.minLon) bbox.minLon = newVal;
      newVal = d3.min(polygons[0], d => d[1]); // d[1] corresponds to latitude
      if (newVal < bbox.minLat) bbox.minLat = newVal;
      newVal = d3.max(polygons[0], d => d[0]); // d[0] corresponds to longitude
      if (newVal > bbox.maxLon) bbox.maxLon = newVal;
      newVal = d3.max(polygons[0], d => d[1]); // d[1] corresponds to latitude
      if (newVal > bbox.maxLat) bbox.maxLat = newVal;
    }
    // Iterate through the multiple polygons
    else if (polygonType==="MultiPolygon") {
      for (var i=0; i<polygons.length; i++) {
        var newVal = d3.min(polygons[i][0], d => d[0]); // d[0] corresponds to longitude
        if (newVal < bbox.minLon) bbox.minLon = newVal;
        newVal = d3.min(polygons[i][0], d => d[1]); // d[1] corresponds to latitude
        if (newVal < bbox.minLat) bbox.minLat = newVal;
        newVal = d3.max(polygons[i][0], d => d[0]); // d[0] corresponds to longitude
        if (newVal > bbox.maxLon) bbox.maxLon = newVal;
        newVal = d3.max(polygons[i][0], d => d[1]); // d[1] corresponds to latitude
        if (newVal > bbox.maxLat) bbox.maxLat = newVal;
      }
    }
    else {
      console.log("Unknown polygon type: "+polygonType);
    }
    return bbox;
  };

  operations.inPolygons = function(polygons, polygonType, polygonBbox, d) {
    // Can cull all objects outside the bounding box to save the expensive
    // inPolygon check for the events inside the bounding box
    if (!operations.inBbox(polygonBbox, d)) return false;

    if(polygonType == "Polygon") {
      return operations.inPolygon(polygons,[d.longitude,d.latitude]);
    }
    else if (polygonType == "MultiPolygon") {
      // Go through all polygons and check whether the line begins
      // in them
      var pointContained = false;
      for(var i=0; i<polygons.length; i++) {
        if(operations.inPolygon(polygons[i],[d.longitude,d.latitude]))
          pointContained = true;
      }
      return pointContained;
    }
    else {
      console.log("Unknown polygon type: "+polygonType);
      return false;
    }
  };

  operations.hideBarchartsFilterButton = function() {
    d3.selectAll("#barcharts-filter-button")
      .classed("active", false)
      .style("display", "none");
  };

  operations.showBarchartsFilterButton = function() {
      d3.selectAll("#barcharts-filter-button")
        .classed("active", true)
        .style("display", "initial");
    };

  operations.getPathString = function(d, dimension) {
    if (["dataset", "match"].includes(dimension)) {
      // "#" to ensure unambiguity, e.g. between "Violent Attack" and "Violent Attack (No State)"
      return d[dimension] + "#";
    }
    else {
      var pathString = "";
      var depth = o.taxonomies[dimension]["depth"];
      for (var i=depth; i>0; i--) {
        // "#" to ensure unambiguity, e.g. between "Violent Attack" and "Violent Attack (No State)"
        pathString = pathString + d[dimension+"_level_"+i] + "#";
      }
      return pathString;
    }
  }

  operations.barchartFilter = function(d, filterConstraints) {
    var containedAll = true;
    var dimensions = Object.keys(filterConstraints);
    var dimIdx = 0;
    while(containedAll && dimIdx<dimensions.length) {
      var dimension = dimensions[dimIdx];
      var filterPaths = filterConstraints[dimension];
      // Get the stringified path of the current data element
      dPathString = operations.getPathString(d, dimension);
      var containedDim = false;
      pIdx = 0;
      while(!containedDim && pIdx<filterPaths.length) {
        containedDim = dPathString.startsWith(filterPaths[pIdx]);
        pIdx++;
      }
      containedAll = containedDim;
      dimIdx++;
    }
    return containedAll;
  };

  operations.getMatchtreeFilter = function(filterConstraints) {
    // Build a function that returns true if a datum shall not be excluded from the filtering results due to the (exclusion) filterConstraints
    // Extract the correct way to format the filter constraints depending on their dimensions
    var filterFnArray = getFilterFnArray(filterConstraints);
    // Final function
    var filterFn = function(d) {
      var contained = true;
      var i = 0;
      // Go over all filterfunction, each corresponding to a filterconstraint, to filter d
      // "while" since as soon as one constraint tells to exclude d, it is ultimately excluded
      while (i<filterFnArray.length && contained) {
        // filterFnArray[i](d) returns true if filterconstraint i does not exclude d from the final filter result
        contained = filterFnArray[i](d);
        i++;
      }
      return contained;
    };
    return filterFn;
  };

  operations.temporalHighlight = function(d, highlightBin) {
    // Filter for all lines in the given temporal bin
    if (highlightBin == o.nrBins) {
      return operations.binNr(d.date) >= highlightBin;
    }
    else {
      return operations.binNr(d.date) == highlightBin;
    }
  };

  operations.spatialHighlight = function(polygonBbox, d) {
    // Filter for all lines in the given polygon data
    return operations.inPolygons(polygons, polygonType, polygonBbox, d);
  }

  operations.spatialCountry = function(poly, d) {
    return operations.inPolygons(poly.polygons, poly.polygonType, operations.getPolygonBbox(poly.polygons, poly.polygonType), d);
  }

  operations.temporal = function(d) {
    return ((d.date >= o.minDate) && (d.date <= o.maxDate))
  };

  operations.getAllDataFilter = function(filst) {
    // Get spatial, temporal and categorical filter functions and combine them to one
    if (filst.temporal != null) {
      o.minDate = filst.temporal.minDate;
      o.maxDate = filst.temporal.maxDate;
    }
    if (filst.spatial != null) {
      o.centerLon = filst.spatial.centerLon;
      o.centerLat = filst.spatial.centerLat;
      o.scale = filst.spatial.scale;
    }

    var temporalF = filst.temporal != null ? d => operations.temporal(d) : d => true;
    var spatialF = filst.spatial != null ? d => operations.inBbox(filst.spatial.bbox, d) : d => true;
    var spatialCountryF = filst["spatial (country)"] != null ? d => operations.spatialCountry(filst["spatial (country)"].poly, d) : d => true;
    var categoricalBarchartsF = filst["categorical (barcharts)"] != null ? d => operations.barchartFilter(d, filst["categorical (barcharts)"]) : d => true;
    var categoricalMatchtreeF = filst["categorical (match-tree)"] != null ? d => {var fn = operations.getMatchtreeFilter(filst["categorical (match-tree)"]); return fn(d)} : d => true;

    return d => (temporalF(d) && spatialF(d) && spatialCountryF(d) && categoricalBarchartsF(d) && categoricalMatchtreeF(d));
  };

  operations.getMergedFilteredData = function(data, filst) {
    // Return whether the event was contained in the originally
    var allDataFilter = operations.getAllDataFilter(filst);
    var dataFiltered = data.filter(d => allDataFilter(d));
    // Modify to not contain duplicates anymore, but keep those cases in mind where
    // the primary duplicate event was filtered out vs where it's still in the dataset
    // -> Use reference (dataset + event(-id) ?) to primary duplicate for that purpose in extra column

    // Modify data to have the original columns

    // Return data
    return dataFiltered;
  };

  operations.updateMatchDataParaComb = function(matchData, paraCombString) {
    return matchData.filter(d => d.parameters == paraCombString);
  };

  operations.updateEventDataParaComb = function(data, matchInfo, paraCombString) {
    // Update match info or every event
    data.forEach((d,i) => {
      d["match info_level_1"] = matchInfo[i][paraCombString] == "1" ? "match" : "unique";
    });
  };

  operations.testval = function(value) {
      if (!arguments.length) return o.testval;
      o.testval = value;
      return operations;
  };
  operations.minDate = function(value) {
      if (!arguments.length) return o.minDate;
      o.minDate = value;
      operations.binScale.domain([o.minDate, o.maxDate]);
      return operations;
  };
  operations.maxDate = function(value) {
      if (!arguments.length) return o.maxDate;
      o.maxDate = value;
      operations.binScale.domain([o.minDate, o.maxDate]);
      return operations;
  };
  operations.scale = function(value) {
      if (!arguments.length) return o.scale;
      o.scale = value;
      return operations;
  };
  operations.centerLon = function(value) {
      if (!arguments.length) return o.centerLon;
      o.centerLon = value;
      return operations;
  };
  operations.centerLat = function(value) {
      if (!arguments.length) return o.centerLat;
      o.centerLat = value;
      return operations;
  };
  operations.nrBins = function(value) {
      if (!arguments.length) return o.nrBins;
      o.nrBins = value;
      operations.binScale.range([0, o.nrBins]);
      return operations;
  };
  operations.taxonomies = function(value) {
      if (!arguments.length) return o.taxonomies;
      o.taxonomies = value;
      return operations;
  };

  return operations;
}
