function calculateSimilarities(buildTaxonomies, taxDataObj, matchData, taxonomyoperations) {
  // {"actor": actorTax, "event": eventTax, "precision": precisionTax}
  var sAll = [0, 5, 10, 25, 50, 100, 250];
  var tAll = [0, 1, 2];

  var dimensions = ["actor", "event", "precision"];
  var dissimilarities = [];
  var allTaxonomies = {};

  var normalizationFactor = function(n) {
    // For each level of the depth, half the weight
    var fac = 0;
    for (var i=0; i<n; i++) {
      fac += (1/2)**i;
    }
    return 1/fac;
  };

  // Set up all taxonomies for all para combinations
  sAll.forEach(s => {
    tAll.forEach(t => {
      allTaxonomies["s"+s+"t"+t] = buildTaxonomies(taxDataObj);
      var updateFn = allTaxonomies["s"+s+"t"+t].calculateCountsReal;
      var filteredData = matchData.filter(d => d.parameters == "s"+s+"t"+t)
      allTaxonomies["s"+s+"t"+t].updateTaxonomyCountsReal(updateFn, filteredData);

      var updateFn = allTaxonomies["s"+s+"t"+t].cumulateCountsReal;
      allTaxonomies["s"+s+"t"+t].updateTaxonomyCountsReal(updateFn);
      console.log("s"+s+"t"+t);
    });
  });

  sAll.forEach((s, i) => {
    tAll.forEach((t, j) => {
      // Get all node paths
      var nodePaths = allTaxonomies["s"+s+"t"+t].getAllNodePaths();
      // Get both neighbors, one to the right and one to the bottom, if they exist
      var neighbors = [];
      if (i+1 < sAll.length) neighbors.push("s"+sAll[i+1]+"t"+t);
      if (j+1 < tAll.length) neighbors.push("s"+s+"t"+tAll[j+1]);
      neighbors.forEach(neighbor => {
        // Iterate over all node paths to calculate the difference between both trees for each node
        var sum = 0;
        // Perform comparisons for all taxonomies
        dimensions.forEach(dim => {
          var dimSum = 0;
          // Get all node paths for the current dimension
          dimNodePaths = nodePaths.filter(d => d.dim == dim);
          dimNodePaths.forEach(pathObj => {
            var path = pathObj.path;
            var count1 = allTaxonomies["s"+s+"t"+t].getCount(dim, path);
            var count2 = allTaxonomies[neighbor].getCount(dim, path);
            dimSum += Math.abs(count1 - count2);
          })
          // Normalize to keep maximum possible dissimilarity per dimension at 1
          sum += dimSum * normalizationFactor(allTaxonomies["s"+s+"t"+t][dim]["depth"]);
        });
        // Max dissimilarity per dimension is 1 => divide by number of dimensions
        sum = sum/dimensions.length;
        dissimilarities.push({first: "s"+s+"t"+t, second: neighbor, dissimilarity: sum})
      });
      console.log("s"+s+"t"+t);
    });
  });

  exportToCsv("dissimilarities.csv", dissimilarities);



  // From https://stackoverflow.com/questions/14964035/how-to-export-javascript-array-info-to-csv-on-client-side
  function exportToCsv(filename, rows) {
    var processRow = function (rowObj) {
      row = Object.values(rowObj);
      var finalVal = '';
      for (var j = 0; j < row.length; j++) {
        var innerValue = row[j] === null ? '' : row[j].toString();
        if (row[j] instanceof Date) {
          innerValue = row[j].toLocaleString();
        };
        var result = innerValue.replace(/"/g, '""');
        if (result.search(/("|,|\n)/g) >= 0)
        result = '"' + result + '"';
        if (j > 0)
        finalVal += ',';
        finalVal += result;
      }
      return finalVal + '\n';
    };

    var csvFile = processRow(Object.keys(rows[0]));
    for (var i = 0; i < rows.length; i++) {
      csvFile += processRow(rows[i]);
    }

    var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, filename);
    } else {
      var link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
        // Browsers that support HTML5 download attribute
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", filename);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }

}
