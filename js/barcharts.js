// TBD maybe legend as in https://fivethirtyeight.com/features/jeb-bushs-problem-is-jeb-bush/

function barCharts() {
  /*
  This function returns the object for drawing the EventCharts component
   */
  var o, chartWidth, chartHeight, stack, groupKeys;

  o = {
    svg: null,
    w: 0,
    h: 0,
    dimensions: null,
    primaryVals: ["G", "A", "GT", "S"],
    colors: {"A": "rgb(75,174,121)", "G": "rgb(0,162,219)", "GT": "rgb(255,152,35)", "S": "rgb(183,41,177)"},
    margin: {inner: 20, outer: 20},
    tickLabelOffset: 20,
    fontSize: 13,
    baselineKey: null,
    xScale: [],
    yScale: [],
    hoverSelection: [],
    filterfunctions: null,
    checkboxWidth: 22,
    labelVisibility: true,
    hierarchyVisibility: false,
    taxonomies: null
  }

  // Create helper functions to invoke only internally
  var fn = {};

  fn.tagify = function(array) {
    return array.reduce((a,b) => a + "#" + b) + "#";
  };

  fn.getLeafPaths = function(node, currPath) {
    // @desc: Recursively extend the 'currPath' by the nodes children as long as they are "opened"
    var returnPath = [currPath];

    if (typeof node["children"] !== "undefined" // First part makes sure that the children array exists
     && typeof node["children"]["undefined"] === "undefined" // The second part makes sure that it does not only contain the "undefined" child
                                                                // which is used to fill up taxonomies that don't reach the full depth of levels which is 4
     && node["opened"] === true // The third part checks whether the node was opened
       )
    {
      returnPath = [];
      Object.keys(node["children"]).forEach(child => {
        // Append child name to path
        var childPath = [...currPath];
        childPath.push(child);
        // Pass on new path to extend it further
        childPathArray = fn.getLeafPaths(node["children"][child], childPath);
        childPathArray.forEach(path => returnPath.push(path));
      });
    }
    return returnPath;
  };

  fn.getAllLeafPaths = function(i) {
    // @desc: Return an array of paths to all the leaf nodes  from taxonomy 'i' that are actually "opened"
    var leafPaths = [];
    Object.keys(o.taxonomies[o.dimensions[i]]["children"]).forEach(child => {
      // For each first child of the root, get all corresponding leaf paths and add them to the overall 'leafPaths' array
      childLeafPathArray = fn.getLeafPaths(o.taxonomies[o.dimensions[i]]["children"][child], [child]);
      childLeafPathArray.forEach(path => leafPaths.push(path));
    });
    return leafPaths;
  };

  fn.getCountedData = function(data, i) {
    // @desc: Retrieve the count information for all the currently displayed categories, i.e., the last consecutively opened ones
    // Get all currently "opened" leaf-paths from taxonomy i
    var leafPaths = fn.getAllLeafPaths(i);
    var labels = [];
    var counts = [];
    var levels = [];
    var paths = [];
    // Determine the count for each leaf path
    leafPaths.forEach(path => {
      // Make copy of data that is filtered later on
      var dataFiltered = [...data];
      var depth = o.taxonomies[o.dimensions[i]]["depth"];
      // Iteratively filter the copied data down to the leaf category of the current leaf path
      for (var j=0; j<path.length; j++) {
        dataFiltered = dataFiltered.filter(d => d[fn.colHeader(i, depth-j)] === path[j])
      }
      valCounts = {};
      // Count how the bar crsp to the path is split up into stacks according to the 'primaryVals'
      o.primaryVals.forEach(val => valCounts[val] = dataFiltered.filter(d => d[fn.colHeader(0,o.taxonomies[o.dimensions[0]]["depth"])] === val).length);
      if (_.reduce(valCounts, (a,b) => a+b) > 0) {
        // Add name of current leaf category
        labels.push(path[path.length-1]);
        counts.push(valCounts);
        // This can be another value than 1 as the "leafPaths" are not the overall leaf paths but the paths to the last consecutively "opened" nodes
        levels.push(depth + 1 - path.length);
        paths.push(path);
      }
    });

    return {"labels": labels, "counts": counts, "levels": levels, "paths": paths};
  };

  fn.pathCheck = function(d, pathString, pathLength, dimIdx) {
    // @desc: Return whether 'pathString' equals the path of event 'd' up to position 'pathLength'
    compareString = "";
    var depth = o.taxonomies[o.dimensions[dimIdx]]["depth"];
    // Concatenate the categories along the path of element d up to position 'pathLength'
    for (var i=depth; i>depth-pathLength; i--) {
      compareString += d[fn.colHeader(dimIdx, i)];
    }
    return compareString === pathString;
  }

  fn.getYScale = function(range, paths, levels, padFactor, dimIdx) {
    // @desc: For dimension 'dimIdx', calculate the y-scale object yielding the y-position and the bandwidth of each of the displayed categories
    // Example for how the levels are used:
    // Level 1: Nonviolent Event -> Nonviolent Action -> Nonviolent Possession -> Territorial Dispute
    // Level 1: A

    var yScale;

    if (paths.length > 0) {
      // Get depth of dimension
      var depth = o.taxonomies[o.dimensions[dimIdx]]["depth"];
      // padFactor * minimal bandwidth is the minimal absolute padding
      var padArray = Array(paths.length-1).fill(padFactor);
      for (let i=0; i<paths.length-1; i++) {
        // For each path get the depth
        var maxPathLength = paths[0].length;
        for (var j=depth; j>=levels[i]; j--) {
          // For each level on the path
          // Check if the entry of the same level from the next path is different to the current one
          if (paths[i][depth-j] !== paths[i+1][depth-j]) {
            // Adjust value only if it has not been adjusted before since we go from the most
            // distant hierarchy levels to the less distance ones. The more distant ones yield
            // a larger padding factor
            if (padArray[i] === padFactor) {
              // Depending on the current level, increase the padFactor to up to depth times the minimal one
              padArray[i] = 2**j*padFactor;
            }
          }
        }
      }
      var levelsPow = levels.map(d => 2**(d-1))
      // Catch case where there is only one bar
      padArray = padArray.length > 0 ? padArray : [0];
      // Calculate the minimal bandwidth
      var bandwidthBase = (range[1]-range[0]) / (levelsPow.reduce((a,b) => a+b) + padArray.reduce((a,b) => a+b));
      // Calculate bandwidth array
      var bandwidths = levelsPow.map(d => bandwidthBase * d)
      // Where each of the barstacks begins
      var yStarts = Array(paths.length);
      // Capture the y-axis end point of the previous barstacks throughout the loop
      var ySum = range[0];
      for (let i=0; i<yStarts.length; i++) {
        if (i===0) {
          yStarts[i] = ySum;
        }
        else {
          yStarts[i] = ySum + padArray[i-1]*bandwidthBase;
          ySum += padArray[i-1]*bandwidthBase;
        }
        ySum += bandwidths[i];
      }

      // Create returnable function similar to the style of d3 scale
      yScale = function(i) {
        return yStarts[i];
      };
      yScale.bandwidth = function(i) {
        return bandwidths[i];
      };
    }
    else {
      // Create returnable function similar to the style of d3 scale
      // As no paths, i.e., no categories are passed, simply return 0
      yScale = function(i) {
        return 0;
      };
      yScale.bandwidth = function(i) {
        return 0;
      };
    }

    return yScale;
  };

  fn.colHeader = function(dimIdx, lvl) {
    // @desc: Depending on the dimension, the column headers in the original data vary, as some attributes have a
    // flat taxonomy. This is the case for the derived attributes "dataset" and "match". Do not attach the level
    // information for them. For all others, attach level information to dimension name to retrieve column header
    var dimName = o.dimensions[dimIdx];
    if (["dataset", "match"].includes(dimName)) {
      return dimName;
    }
    else {
      return dimName+"_level_"+lvl;
    }
  };

  fn.deactivateBaselineButton = function(selection) {
    // @desc: Hide and deactivate the baseline buttons
  	selection
  		.attr("fill", d3.select(":root").style("--compenent-bg-color"))
  		.classed("inactive", true)
  		.classed("active", false);
  }

  fn.activateBaselineButton = function(selection) {
    // @desc: Show baseline buttons again and activate them
  	selection
      .attr("fill", d => o.colors[d])
  		.classed("active", true)
  		.classed("inactive", false);
  };

  fn.getSplitScore = function(dimIdx) {
    // @desc: Get the separability of dimension 'dimIdx'
    // Get the stack data
    var series = d3.selectAll(".barchart-"+dimIdx).data();
    var nrPaths = series[0].length;
    pathRatios = [];
    // For each path, i.e., category, i.e., bar stack get the count of the largest bar
    for (let path=0; path<nrPaths; path++) {
      var totalStackWidth = 0;
      var largestStackWidth = d3.max(o.primaryVals, val => {
        var stacksForCurrentVal = series.filter(d => d.key === val)[0];
        // The values in 'series' are accumulated, so subtract each start value from the end value to retrieve the actual count
        var currentStackWidth = stacksForCurrentVal[path].end - stacksForCurrentVal[path].start;
        // While extracting the maximum, also extract the total count
        totalStackWidth += currentStackWidth;
        return currentStackWidth;
      });
      // Relative count is used
      pathRatios.push(largestStackWidth/totalStackWidth);
    }
    return pathRatios.reduce((a,b) => a+b)/pathRatios.length;
  };

  // Define the rendering
  function chart(selection) {
    selection.each(function(data, idx) {

      if (!data || !o.dimensions) {return;}

      // Select the svg element
      o.svg = d3.select(this);
      o.w = o.svg.attr("width");
      o.h = o.svg.attr("height");
      // Get the size each barchart can have
      chartWidth = (o.w - 3*o.margin.outer - (o.dimensions.length-1)*o.margin.inner) / o.dimensions.length; // 3* due to hierarchy offset on the left
      chartHeight = o.h - 2*o.margin.outer;
      // Function to stack the bars inside the loop
      stack = d3.stack().keys(o.primaryVals);
      // Can be deleted?
      groupKeys = {};
      // Append an svg for each axis
      for (let i=0; i<o.dimensions.length; i++) {
        o.svg.append("svg").attr("id", "barchart-svg-"+i)
      }
      // Iterate over axes and draw barcharts
      for (let i=0; i<o.dimensions.length; i++) {
        update(data, i);
        appendDimensionFilterboxes(i);
      }
      createBaselineButtons();
    })
  }

  var update = function(data, i) {
    // @desc: Extract the count information for each displayed category, i.e., each consecutively-opened leaf path
    // of attribute/taxonomy 'i'
    var cData = fn.getCountedData(data, i);
    // Create the stacked information
    var series = stack(cData.counts);
    // Calculate left border of plot
    var xStart = 2*o.margin.outer+i*(chartWidth+o.margin.inner)
    // Create scale for x-positions of the bars
    o.xScale[i] = d3.scaleLinear()
      .range([xStart, xStart+chartWidth]);
    // The xScale domain for the widths of the bar stacks needs to be adjusted depending on where the baseline is.
    // 'lmax' describes the maximum count displayed to the left of the baseline for a single category,
    // 'rmax' the maximum count displayed to the right
    var lmax, rmax;
    // Calculate the values for 'lmax' and 'rmax'
    if(i > 0) {
      // In all but the first dimension, reshape series so it can be aligned along the baseline
      // lmax is everything left to the beginning of the baseline rect
      lmax = d3.max(series[series.map(d => d.key).indexOf(o.baselineKey)], d => d[0]);
      // rmax is everything right to the beginning of the baseline rect
      rmax = 0;
      for(var j=0; j<cData.labels.length; j++) {
        // End value of leftmost rect minus begin value of baseline rect
        var newMax = series[series.length-1][j][1] - series[series.map(d => d.key).indexOf(o.baselineKey)][j][0];
        rmax = rmax < newMax ? newMax : rmax;
      }
      // Adjust series according to the offset on the left and the right
      for(var l=0; l<cData.labels.length; l++) {
        var offsetLeft = lmax - series[series.map(d => d.key).indexOf(o.baselineKey)][l][0];
        for(var k=0; k<series.length; k++) {
          // Increase beginning and end mark of bar stack by left offset
          series[k][l][0] += offsetLeft;
          series[k][l][1] += offsetLeft;
        }
      }
      // Set overall domain of the xScale
      o.xScale[i].domain([0, lmax+rmax]);
    }
    else {
      // In the primary dimension we do not adjust the baseline since no stacks exist
      // For each bar, sum over all stack components and take max
      o.xScale[i].domain([0, d3.max(cData.counts, d => _.reduce(d, (a,b) => a+b))])
      // Set lmax to 0 for setting the baseline line
      lmax = 0;
    }
    // Create scale for y-positions of the bars
    o.yScale[i] = fn.getYScale([o.margin.outer, o.margin.outer + chartHeight], cData.paths, cData.levels, 0.2, i);
    // Bring 'series' in more human-readable form and append the path information to each bar
    series = series.map(d => {var out = d.map((e,j) => {return {"start": e[0], "end": e[1], "path":cData.paths[j]}}); out.key = d.key; return out;})
    // Populate the DOM
    var svgSub = d3.select("#barchart-svg-"+i);
    // Overarching g to change the dimension order later on
    var reorderG = svgSub.selectAll("#barchart-reorder-g-"+i)
      .data([0])
      .join(
        enter => enter.append("g")
          .attr("id", "barchart-reorder-g-"+i)
      );
    // To append the filterboxes later in the background
    reorderG.selectAll(".barchart-filterbox-g.dim-"+i)
      .data([0])
      .join(
        enter => enter.append("g")
          .attr("class", "barchart-filterbox-g dim-"+i)
      );

    // Append baseline line (only draw if any filtered data exists)
    var t = svgSub.transition().duration(500);
    if (cData.counts.length > 0) {
      reorderG.selectAll("#barchart-baseline-"+i)
      .data([0])
      .join(
        enter => enter.append("line")
        .attr("id", "barchart-baseline-"+i)
        .attr("x1", d => o.xScale[i](lmax))
        .attr("x2", d => o.xScale[i](lmax))
        .attr("y1", o.margin.outer - o.tickLabelOffset/2)
        .attr("y2", o.margin.outer + chartHeight)
        .attr("stroke", "rgb(220, 220, 220)")
        .attr('stroke-dasharray', '8,8'),
        update => update
        .call(update => update.transition(t)
        .attr("x1", d => o.xScale[i](lmax))
        .attr("x2", d => o.xScale[i](lmax)))
      );
    }
    else {
      // Without data, no baseline should be displayed
      reorderG.selectAll("#barchart-baseline-"+i).remove()
    }

    // Create groups for each row of the series. If groups do not exist yet, they are created, otherwise updated
    // Group creation based on (not really anymore though): https://groups.google.com/forum/#!topic/d3-js/Rlv0O8xsFhs
    // Afterwards, append the bar rects
    reorderG.selectAll(".barchart-"+i)
      .data(series, d => d.key)
      .join("g")
      .attr("class", "barchart-"+i)
      .attr("fill", d => o.colors[d.key])
      .attr("stroke", d => o.colors[d.key])
      .attr("stroke-width", 1)
      .selectAll(".barchart-rect")
      .data(d => d, d => d.path)
      .join(
        enter => enter.append("rect")
          .attr("class", "barchart-rect dim-"+i)
          .attr("x", d => o.xScale[i](d.start))
          .attr("width", d => o.xScale[i](d.end)-o.xScale[i](d.start))
          .attr("y", (d,j) => o.yScale[i](j))
          .attr("height", (d,j) => o.yScale[i].bandwidth(j))
          .attr("opacity", 0),
        update => update
          .call(update => update.transition(t)
            .attr("x", d => o.xScale[i](d.start))
            .attr("width", d => o.xScale[i](d.end)-o.xScale[i](d.start))
            .attr("y", (d,j) => o.yScale[i](j))
            .attr("height", (d,j) => o.yScale[i].bandwidth(j)))
      )
      .transition().duration(500).delay(500)
      // Prevent strange things form happening when clicking again while still in transition
      .on("start", function() {
        d3.select(this).attr("pointer-events", "none");
      })
      .on("end", function() {
        d3.select(this).attr("pointer-events", "all");
      })
      .attr("opacity", 1)

    // Create label data using labels for text and paths for key indexing
    var labelData = [];
    d3.range(cData.labels.length).forEach(j => {
      labelData.push([cData.labels[j], cData.paths[j]]);
    })

    // Append background rects for label data
    var backgroundRects = reorderG.selectAll(".barchart-tick-label-background-"+i)
      .data([0]) // Creates a single group of the referenced class if it does not exist yet
      .join(
        enter => enter.append("g")
          .attr("class", "barchart-tick-label-background-"+i)
      )
        .selectAll(".barchart-tick-label-box")
        .data(labelData, d => d[1])
        .join(
          enter => enter.append("rect")
            .attr("fill", d3.select(":root").style("--compenent-bg-color"))
            .attr("x", o.xScale[i].range()[0]-1)
            .attr("y", (d,j) => o.yScale[i](j) + o.yScale[i].bandwidth(j) - o.fontSize)
            .attr("height", o.fontSize + 1)
            .attr("opacity", 0)
            .attr("class", "barchart-tick-label-box"),
          update => update
            .call(update => update.transition(t)
              .attr("x", o.xScale[i].range()[0]-1)
              .attr("y", (d,j) => o.yScale[i](j) + o.yScale[i].bandwidth(j) - o.fontSize)
              .attr("height", o.fontSize + 1))
        )
        .attr("pointer-events", "none");

    // Append tick labels
    reorderG.selectAll("#barchart-tick-labels-"+i)
      .data([0])
      .join(
        enter => enter.append("g")
          .attr("id", "barchart-tick-labels-"+i)
      )
        .selectAll("text")
        .data(labelData, d => d[1])
        .join(
          enter => enter.append("text")
            .text(d => {
              var text;
              switch(d[0]) {
                case "G": text="GED"; break;
                case "A": text="ACLED"; break;
                case "GT": text="GTD"; break;
                case "S": text="SCAD"; break;
                default: text = d[0];
              }
              return text;
            })
            .attr("class", "barchart-tick-label")
            .attr("x", o.xScale[i](0))
            .attr("y", (d,j) => o.yScale[i](j) + o.yScale[i].bandwidth(j))
            .attr("anchor", "middle")
            .attr("fill", "rgb(220, 220, 220)")
            .attr("opacity", 0),
          update => update
            .call(update => update.transition(t)
              .text(d => {
                var text;
                switch(d[0]) {
                  case "G": text="GED"; break;
                  case "A": text="ACLED"; break;
                  case "GT": text="GTD"; break;
                  case "S": text="SCAD"; break;
                  default: text = d[0];
                }
                return text;
              })
              .attr("x", o.xScale[i](0))
              .attr("y", (d,j) => o.yScale[i](j) + o.yScale[i].bandwidth(j)))
        )
        .attr("pointer-events", "none")
        .transition().duration(500).delay(500)
        .attr("opacity", o.labelVisibility ? 1 : 0);
    // Adjust the width of the 'backgroundRects' to the width of the overlying label text
    backgroundRects
      //.attr("width", d => d3.select("#barchart-tick-labels-"+i).selectAll("text").filter(e => e[0]==d[0]).node().getBBox().width)
      .attr("width", chartWidth+2)
      .transition().duration(500).delay(500)
      .attr("opacity", o.labelVisibility ? 0.5 : 0);

    // Append chart title
    reorderG.selectAll("#barchart-title-"+i)
      .data([o.dimensions[i]])
      .join("text")
      .attr("id", "barchart-title-"+i)
      .text(d => d.charAt(0).toUpperCase() + d.slice(1))
      .attr("text-anchor", "middle")
      .attr("x", (o.xScale[i].range()[1]+o.xScale[i].range()[0]) / 2)
      .attr("y", o.margin.outer - o.tickLabelOffset)
      .attr("fill", "rgb(220, 220, 220)");

    // Append box around title
    reorderG.selectAll("#barchart-title-box-"+i)
      .data([o.dimensions[i]])
      .join("rect")
      .attr("id", "barchart-title-box-"+i)
      .attr("x", o.xScale[i].range()[0])
      .attr("y", 2)
      .attr("height", o.margin.outer - 2 - o.tickLabelOffset/2)
      .attr("width", chartWidth)
      .attr("fill", "none")
      .attr("stroke", "rgb(220, 220, 220)");

    //////////////////////////////////////////
    // Create hierarchy representation here //
    //////////////////////////////////////////
    // For each of the displayed labels, get the path

    const hierarchyColor = 'rgb(180,180,180)';

    const labelPaths = labelData.map(d => d[1]);
    const maxDepth = d3.max(labelPaths, path => path.length);

    // Extract all additional upper node paths that need to be displayed

    // Concat to long array and create set
    const allNodePaths = {};
    labelPaths.forEach(path => {
      for (let j = 1; j < path.length+1; j++) {
        // Initialize node positions with y: 0, and x according to path-lenth in ratio to depth of the dimension and o.margin.inner
        const tagPath = fn.tagify(path.slice(0,j));
        allNodePaths[tagPath] = {path : tagPath, depth: j, x: o.xScale[i].range()[0] - (o.margin.inner/2 - (j)*o.margin.inner/(2*maxDepth)) - 2, y: null};
      }
    })
    
    // For each element of set, determine the y-value. Begin with childmost nodes
    // First, cover the "childmost nodes": use the displayed label information for that
    labelPaths.forEach((path, j) => {
      allNodePaths[fn.tagify(path)].y = o.yScale[i](j) + o.yScale[i].bandwidth(j) - o.fontSize/2;
    });

    // For each (upper?) node, get highest and lowest position of child node
    // To get those, iterate from max possible depth of the current tree upwards until 0 and filter for paths of the corresponding length that still have a y-value of null
    d3.range(maxDepth+1, 0, -1).forEach(l => {
      //const filteredNodePaths = _.filter(allNodePaths, (info, path) => info.depth == l);
      _.forEach(allNodePaths, (info, path) => {
        if (info.y == null && info.depth == l) {
          // Get all displayed child nodes, extract the min and max y-value, and select middle as own y-value
          const childNodes = _.filter(allNodePaths, (childinfo, childpath) => childpath.startsWith(path) && !(childpath == path));
          const maxChildPos = d3.max(_.map(childNodes, (childinfo, childpath) => childinfo), d => d.y);
          const minChildPos = d3.min(_.map(childNodes, (childinfo, childpath) => childinfo), d => d.y);
          info.y = (maxChildPos + minChildPos) / 2;
        }
      });
    })


    // CONTINUE: Try out by appending small circles for each category, joined by path as key. Need to convert to data array first, though
    const nodeData = _.map(allNodePaths, (val, key) => val);
    
    const hierarchyG = reorderG.selectAll(".barchart-hierarchy-"+i)
      .data([0]) // Creates a single group of the referenced class if it does not exist yet
      .join(
        enter => enter.append("g")
          .attr("class", "barchart-hierarchy-"+i)
      )

    hierarchyG.selectAll(".barchart-hierarchy-circle")
        .data(nodeData, d => d.path)
        .join(
          enter => enter
            .append("circle")
            .attr('class', 'barchart-hierarchy-content barchart-hierarchy-circle')
            .attr('cx', d => d.x)
            .attr('cy', d => d.y)
            .attr('r', 2)
            .attr('fill', hierarchyColor)
            .attr('opacity', 0)
        )
        .transition()
        .duration(500)
        .attr('opacity', o.hierarchyVisibility ? 1 : 0)
        .attr('cx', d => d.x)
        .attr('cy', d => d.y)
    // (Append root node information?)

    hierarchyG.selectAll(".barchart-hierarchy-horline")
      .data(nodeData, d => d.path)
      .join(
        enter => enter
          .append("line")
          .attr('class', 'barchart-hierarchy-content barchart-hierarchy-horline')
          .attr('x1', d => d.x - o.margin.inner/(2*maxDepth)) //o.margin.inner/(2*maxDepth))
          .attr('x2', d => d.x)
          .attr('y1', d => d.y)
          .attr('y2', d => d.y)
          .attr('stroke', hierarchyColor)
          .attr('opacity', 0)
      )
      .transition()
      .duration(500)
      .attr('opacity', o.hierarchyVisibility ? 1 : 0)
      .attr('x1', d => d.x - o.margin.inner/(2*maxDepth))//o.margin.inner/(2*maxDepth))
      .attr('x2', d => d.x )
      .attr('y1', d => d.y)
      .attr('y2', d => d.y)
    // Alternative: Draw each line straight back horizontally (no parent information necessary) and iterate over all non-childmost nodes afterwards to draw vertical lines between corresponding highest and lowest child - actually cooler, eh?

    // Remove childnodes from nodeData
    const labelPathsTagged = labelPaths.map(d => fn.tagify(d))
    hierarchyG.selectAll(".barchart-hierarchy-vertline")
      .data(nodeData.filter(d => !(labelPathsTagged.includes(d.path))), d => d.path)
      .join(
        enter => enter
          .append("line")
          .attr('class', 'barchart-hierarchy-content barchart-hierarchy-vertline')
          .attr('x1', d => d.x)
          .attr('x2', d => d.x)
          .attr('y1', d => { // Only get direct children
            const childNodes = _.filter(allNodePaths, (childinfo, childpath) => {
              // Filter for those paths including the current path
              return childpath.startsWith(d.path)
              // Drop information until first # and return true if remaining path contains of exactly one #, i.e. the split path consists of two substrings
              && childpath.slice(d.path.length, childpath.length).split("#").length == 2
            })
            return d3.min(_.map(childNodes, (childinfo, childpath) => childinfo), e => e.y);
          })
          .attr('y2', d => {
            const childNodes = _.filter(allNodePaths, (childinfo, childpath) => {
              // Filter for those paths including the current path
              return childpath.startsWith(d.path)
              // Drop information until first # and return true if remaining path contains of exactly one #, i.e. the split path consists of two substrings
              && childpath.slice(d.path.length, childpath.length).split("#").length == 2
            })
            return d3.max(_.map(childNodes, (childinfo, childpath) => childinfo), e => e.y);
          })
          .attr('stroke', hierarchyColor)
          .attr('opacity', 0)
      )   
      .transition()
      .duration(500)
      .attr('opacity', o.hierarchyVisibility ? 1 : 0)
      .attr('x1', d => d.x)
      .attr('x2', d => d.x)
      .attr('y1', d => { // Only get direct children
        const childNodes = _.filter(allNodePaths, (childinfo, childpath) => {
          // Filter for those paths including the current path
          return childpath.startsWith(d.path)
          // Drop information until first # and return true if remaining path contains of exactly one #, i.e. the split path consists of two substrings
          && childpath.slice(d.path.length, childpath.length).split("#").length == 2
        })
        return d3.min(_.map(childNodes, (childinfo, childpath) => childinfo), e => e.y);
      })
      .attr('y2', d => {
        const childNodes = _.filter(allNodePaths, (childinfo, childpath) => {
          // Filter for those paths including the current path
          return childpath.startsWith(d.path)
          // Drop information until first # and return true if remaining path contains of exactly one #, i.e. the split path consists of two substrings
          && childpath.slice(d.path.length, childpath.length).split("#").length == 2
        })
        return d3.max(_.map(childNodes, (childinfo, childpath) => childinfo), e => e.y);
      })

      hierarchyG.selectAll(".barchart-hierarchy-vertline-root")
      .data([{path: "root", x: o.xScale[i].range()[0] - o.margin.inner/2 - 2}], d => d.path)
      .join(
        enter => enter
          .append("line")
          .attr('class', 'barchart-hierarchy-content barchart-hierarchy-vertline-root')
          .attr('x1', d => d.x)
          .attr('x2', d => d.x)
          .attr('y1', d => { // Only get direct children
            const childNodes = _.filter(allNodePaths, (childinfo, childpath) => {
              // Return true if path is first real node after root, i.e. contains exactly one #, i.e. the split path consists of two substrings
              return childpath.split("#").length == 2
            })
            return d3.min(_.map(childNodes, (childinfo, childpath) => childinfo), e => e.y);
          })
          .attr('y2', d => { // Only get direct children
            const childNodes = _.filter(allNodePaths, (childinfo, childpath) => {
              // Return true if path is first real node after root, i.e. contains exactly one #, i.e. the split path consists of two substrings
              return childpath.split("#").length == 2
            })
            return d3.max(_.map(childNodes, (childinfo, childpath) => childinfo), e => e.y);
          })
          .attr('stroke', hierarchyColor)
          .attr('opacity', 0)
      )
      .transition()
      .duration(500)
      .attr('opacity', o.hierarchyVisibility ? 1 : 0)
      .attr('x1', d => d.x)
      .attr('x2', d => d.x)
      .attr('y1', d => { // Only get direct children
        const childNodes = _.filter(allNodePaths, (childinfo, childpath) => {
          // Return true if path is first real node after root, i.e. contains exactly one #, i.e. the split path consists of two substrings
          return childpath.split("#").length == 2
        })
        return d3.min(_.map(childNodes, (childinfo, childpath) => childinfo), e => e.y);
      })
      .attr('y2', d => { // Only get direct children
        const childNodes = _.filter(allNodePaths, (childinfo, childpath) => {
          // Return true if path is first real node after root, i.e. contains exactly one #, i.e. the split path consists of two substrings
          return childpath.split("#").length == 2
        })
        return d3.max(_.map(childNodes, (childinfo, childpath) => childinfo), e => e.y);
      })

  };

  var appendDimensionFilterboxes = function(dimIdx) {
    // @desc: For attribute nr. 'dimIdx', append "(de-)select all" boxes next to the axis title
    // Set up basic shape-determining parameters for the box
    var labelBoxHeight = o.margin.outer - 2 - o.tickLabelOffset/2;
    var height = labelBoxHeight/2;
    var xStart = o.xScale[dimIdx].range()[0]-height;
    var yStart = 2;
    var reorderG = d3.select("#barchart-reorder-g-"+dimIdx);
    var filterG = reorderG.selectAll(".barchart-checkbox-all-g")
      .data([0])
      .join(
        enter => enter.append("g").attr("class", "barchart-checkbox-all-g")
      );
    // Append filterbox to (de)select entire dimension
    filterG.selectAll(".barchart-checkbox-all.box.dim-"+dimIdx)
      .data(["select", "deselect"])
      .enter()
      .append("rect")
      .attr("class", "barchart-checkbox-all box dim-"+dimIdx)
      .attr("x", xStart)
      .attr("y", d => d == "select" ? yStart : yStart+height)
      .attr("height", height)
      .attr("width", height)
      .attr("fill", d => d == "select" ? "white" : d3.select(":root").style("--compenent-bg-color"))
      .attr("stroke", "white");
    // Append vertical Line of the plus
    filterG.selectAll(".barcharts-checkbox-all.vertLine.dim-"+dimIdx)
      .data(["select", "deselect"])
      .enter()
      .append("line")
      .attr("class", "barchart-checkbox-all vertLine dim-"+dimIdx)
      .attr("x1", xStart+height/2)
      .attr("x2", xStart+height/2)
      .attr("y1", d => d == "select" ? yStart+2 : yStart+height+2)
      .attr("y2", d => d == "select" ? yStart+height-2 : yStart+2*height-2)
      .attr("stroke", d => d == "select" ? d3.select(":root").style("--compenent-bg-color") : "white");
    // Append horizontal line of the plus
    filterG.selectAll(".barcharts-checkbox-all.horiLine.dim-"+dimIdx)
      .data(["select", "deselect"])
      .enter()
      .append("line")
      .attr("class", "barchart-checkbox-all horiLine dim-"+dimIdx)
      .attr("x1", xStart+2)
      .attr("x2", xStart+height-2)
      .attr("y1", d => d == "select" ? yStart+height/2 : yStart+height*1.5)
      .attr("y2", d => d == "select" ? yStart+height/2 : yStart+height*1.5)
      .attr("stroke", d => d == "select" ? d3.select(":root").style("--compenent-bg-color") : "white");
    // Make the mouse cursor change when hovering over the boxes to indicate possible interaction
    filterG.selectAll(".barchart-checkbox-all.dim-"+dimIdx)
      .attr("custom:dimIdx", dimIdx)
      .style("display", "none") // Do not display initially
      .on("mouseover", function(d) {d3.select(this).style("cursor", "pointer")});
    filterG.selectAll(".barchart-checkbox-all.dim-"+dimIdx)
      .on("mouseout", function(d) {d3.select(this).style("cursor", "default")});
  };

  var createBaselineButtons = function() {
    // @desc: For each primary value, create a circular button in the vertical center of the corresponding subcategories (if dug down)
    // Create an adjusted yScale that reaches over all leafPaths corresponding to one primaryVal
    var yPos = [];
    // Get rects from first dimension
    var rects = d3.selectAll(".barchart-rect.dim-0");
    // Create copy of primaryVals to remove entries from if they do not have counts>0 anymore due to filtering
    var primaryValsCopy = [...o.primaryVals];
    // For each primaryVal
    o.primaryVals.forEach((primaryVal, i) => {
      // Filter rects to those with the primaryVal corresponding to the first entry of their path
      var rectsFiltered = rects.filter(d => d.path[0] == primaryVal);
      // If empty, remove the corresponding entry from the primaryVals copy
      if (rectsFiltered.empty()) {
        primaryValsCopy.splice(primaryValsCopy.indexOf(primaryVal), 1)
      }
    })
    // Determine the y-positions of the baseline buttons
    var pathIdx = 0;
    if (primaryValsCopy.length == 1) {
      // If only one primary val is left, use half the chart height for the y-position of the button
      yPos.push(o.margin.outer + chartHeight/2);
    }
    else {
      primaryValsCopy.forEach((primaryVal, i) => {
        // Filter for all categories of the current 'primaryVal' that are currently displayed due to digging down
        var nrPaths = rects.filter(d => d.path[0] == primaryVal).size()/o.primaryVals.length;
        // Get distance between start of first bar end end of last bar and divide by two to get y-position of button
        yPos.push((o.yScale[0](pathIdx) + o.yScale[0](pathIdx+nrPaths-1)+o.yScale[0].bandwidth(pathIdx+nrPaths-1))/2);
        pathIdx += nrPaths;
      });
    }
    // Append the buttons for the current selection
    var svgSub = d3.select("#barchart-svg-0")
    svgSub.selectAll(".barchart-baseline-button-g")
      .data([0])
      .join("g")
      .attr("class", "barchart-baseline-button-g")
      .selectAll("circle")
      .data(primaryValsCopy, d=>d)
      .join(
        enter => enter.append("circle")
          .attr("class", "button barchart-baseline inactive")
          .attr("id", (d,j) => "barchart-baseline-button-"+d.replace(" ", "-"))
          .attr("cx", o.margin.outer/4)
          .attr("cy", (d,j) => yPos[j])
          .attr("r", o.margin.outer/5)
          .attr("fill", d3.select(":root").style("--compenent-bg-color"))
          .attr("stroke", d => o.colors[d])
          .attr("stroke-width", 2),
        update => update
          .attr("fill", d3.select(":root").style("--compenent-bg-color"))
          .attr("stroke", d => o.colors[d])
          .call(update => update
            .transition()
            .duration(500)
            .attr("cy", (d,j) => {console.log(d); console.log(yPos[j]); return yPos[j];})
          ),
        exit => exit
          .remove()
      )
    // Set the correct button as activated
    svgSub.selectAll("#barchart-baseline-button-"+o.baselineKey.replace(" ", "-"))
      .call(fn.activateBaselineButton);
  }

  chart.updateAll = function(selection) {
    // @desc: Invoke the update of the barcharts for all axes
    selection.each(function(data, idx) {
      if (!data && !o.dimensions) {return;}
      // Iterate over dimensions and draw barcharts
      console.time("barcharts baseline update")
      for (var i=0; i<o.dimensions.length; i++) {
        update(data, i);
      }
      if (data.length == 0) {
        // No data to display
        d3.selectAll(".svg-barcharts")
        .append("text")
        .attr("x", o.w/2)
        .attr("y", o.h/2)
        .attr("text-anchor", "middle")
        .text("All data filtered out.")
        .attr("transform", "skewX(-20)")
        .attr("fill", "rgb(220, 220, 220)");
      }
      console.timeEnd("barcharts baseline update")
      // Sometimes the baseline buttons are still hidden when jumping out of filtering mode
      createBaselineButtons();
      chart.showBaselineButtons();
    });
  };

  chart.hoverDigdown = function(selection, rect) {
    // @desc: When hovering over a bar stack, display the underlying categories on top of it
    // Selection to append the newly created bars to so they can be removed later if we do not actually digdown
    o.hoverSelection = [];
    selection.each(function(data, idx) {
      if (!data || !o.dimensions) {return;}
      // Get the dimension number via the class of the parent group
      var parentG = d3.select(rect.parentElement);
      var dimIdx = parentG.attr("class").slice(-1);
      rect = d3.select(rect);
      // Where the current bar starts
      var xStart = o.margin.outer + dimIdx*(o.margin.inner+chartWidth)
      // Extract path of selected rect
      var path = rect.data()[0].path;
      // Fade the hovered category to less opacity as it will be displayed in the background
      d3.select("#barchart-reorder-g-"+dimIdx)
        .selectAll(".barchart-rect")
          .filter(d => d.path === path)
          .classed("barchart-hover-removal-rect", true)
          .transition()
          .duration(300)
          .attr("opacity", 0.1);
      // Get list of children
      var childrenObject = o.taxonomies.getChildren(o.dimensions[dimIdx], path);
      // Only perform if there are any children to digdown into
      if (childrenObject != null) {
        var children = Object.keys(childrenObject);
        // In the following, extract the counted data for the child categories
        // Filter data for path of the given parent category
        var depth = o.taxonomies[o.dimensions[dimIdx]]["depth"];
        for (var i=0; i<path.length; i++) {
          data = data.filter(d => d[fn.colHeader(dimIdx, depth-i)] == path[i])
        }
        // i is now at the level that is to be added
        // Do the stuff originally done to count data on the filtered subset
        var groupedDataUnordered = _.chain(data)
          // Slice data...
          .map(d => [
              d[fn.colHeader(0, o.taxonomies[o.dimensions[0]]["depth"])], //...according to primary values
              d[fn.colHeader(dimIdx, depth-i)] // ...and according to the child categories for the current axis
          ])
          .groupBy(d => d[1]) // Group data, d[1] corresponds to o.dimensions[i]
          .value() // Get results from chain
        // Bring groupedData into the order used in the initial visualization so the barstacks don't jump around
        var groupedData = {};
        // Reorder the stacks to match the taxonomy order
        children.forEach(child => {
          // 'groupedData' is newly filled, hence, the order of the keys becomes the same as in 'children'
          groupedData[child] = groupedDataUnordered[child];
        })
        // Count data, d[0] corresponds to the datasets
        var countedData = _.map(groupedData, d => _.countBy(d, d => d[0]))
        // Set count to 0 for those datasets not occurring in certain barstacks (otherwise they get NaN when calling stack())
        countedData = _.map(countedData, d => {
          counts = {};
          o.primaryVals.forEach(g => counts[g] = typeof d[g] === 'undefined' ? 0:d[g]);
          return counts
        });
        // Get height of the preview chart as the height of the parent category stack
        var rectHeight = rect.attr("height");
        // To plot the child categories along the baseline of the full, non-hovered axis, its corresponding lmax value
        // needs to be known. Extract it in the following
        var svgSub = d3.select("#barchart-svg-"+dimIdx);
        // Series data of the axis without hovering
        var seriesOld = svgSub.selectAll(".barchart-"+dimIdx).data();
        // Series data of the child categories
        var series = stack(countedData);
        // Get lmax of the axis withou hovering
        var lmax = d3.max(seriesOld[seriesOld.map(d => d.key).indexOf(o.baselineKey)], d => d.start);
        // Reshape series so it can be aligned along the baseline
        for(var l=0; l<children.length; l++) {
          // Set offset to 0 for the primary attribute dimension since it is always left-aligned
          var offsetLeft = dimIdx == 0 ? 0 : lmax - series[series.map(d => d.key).indexOf(o.baselineKey)][l][0];
          for(var k=0; k<series.length; k++) {
            // Increase beginning and end mark of bar stack by left offset
            series[k][l][0] += offsetLeft;
            series[k][l][1] += offsetLeft;
          }
        }
        // Make series human-readable and append paths to match its structure to the 'seriesOld'
        series = series.map(d => {
          var out = d.map((e,j) => {return {"start": e[0], "end": e[1], "path":[...path, children[j]]}});
          out.key = d.key;
          return out;
        });
        // Concatenate the information of 'seriesOld' and 'series' be able to append the full data to the DOM later on
        // (Remember: The old series needs to still be available if the hovering is stopped without clicking, i.e.,
        // without acutally diggin down)
        series = seriesOld.map((d, i) => {
          var dNew = d.concat(...series.filter(e => e.key === d.key)); // 'key' refers to the primary value
          dNew.key = d.key;
          return dNew;
        });
        // Create equidistant y-scale for the child categories
        var yScale = d3.scaleBand()
          .domain(children)
          .range([rect.attr("y"), parseFloat(rect.attr("y"))+parseFloat(rectHeight)])
          .paddingInner(0.1)
          .paddingOuter(0.1);
        // Draw the child category rects
        series.forEach(groupArray => {
          // Each groupArray corresponds to one primaryVal with its key
          var group = svgSub.selectAll(".barchart-"+dimIdx).filter(d => d.key == groupArray.key);
          // Go over the different rect to be drawn for the group
          var appendedRects = group.selectAll(".barchart-rect")
            .data(groupArray)
            .enter() // As we only consider the enter selection, only the child rects get drawn here
            .append("rect")
            .attr("class", "barchart-rect dim-"+dimIdx)
            .attr("pointer-events", "none")
            .attr("x", d => o.xScale[dimIdx](d.start))
            .attr("width", d => o.xScale[dimIdx](d.end)-o.xScale[dimIdx](d.start))
            .attr("y", (d,j) => {console.log(yScale(d.path[d.path.length-1]));return yScale(d.path[d.path.length-1])})
            .attr("height", (d,j) => yScale.bandwidth())
            .attr("opacity", 0);
          appendedRects
            .transition()
            .duration(300)
            .attr("opacity", 1);
          o.hoverSelection.push(appendedRects);
        });
      }
    });
  };

  chart.hoverDigup = function(rect, data) {
    // @desc: Draw a rectangle around all sibling categories of the current category (unless already on the highest level)
    // Selection to append the overarching rect to
    o.hoverSelection = [];
    // Get the parent group
    var parentG = d3.select(rect.parentElement);
    var dimIdx = parentG.attr("class").slice(-1);
    rect = d3.select(rect);
    // Determine whether the current category is already the hightest up in the taxonomy
    var alreadyOnHighestLevel = data.path.length === 1;
    // Draw the encompassing rect in the following
    var siblingRects;
    var parentPath = null;
    var reorderG = d3.selectAll("#barchart-reorder-g-"+dimIdx);
    if (alreadyOnHighestLevel) {
      // If we are already on the highest level, only draw rect around current bar stack
      siblingRects = reorderG.selectAll(".barchart-rect")
        .filter(d => JSON.stringify(d.path) === JSON.stringify(data.path));
    }
    else {
      // Cut off the deepest level to get the parent path and filter for all rects having the same parent path
      var extractIdx = data.path.length - 1;
      parentPath = data.path.slice(0,extractIdx);
      siblingRects = reorderG.selectAll(".barchart-rect")
        .filter(d => JSON.stringify(d.path.slice(0,extractIdx)) === JSON.stringify(parentPath));
    }
    // Retrieve minimal y position and maximal y position + its bandwidth
    var ymin = o.h;
    // Beginning of last sibling rect
    var ymax = 0;
    // Bandwidth of last sibling rect
    var lastbandwidth = 0;
    // Get actual values for 'ymax' and 'lastbandwidth'
    siblingRects.each(function() {
      var r = d3.select(this);
      var y = +r.attr("y");
      ymin = y < ymin ? y : ymin;
      if (y >= ymax) {
        ymax = y;
        lastbandwidth = +r.attr("height");
      }
    });
    // Offset with which the overarching rect should be drawn around the corresponding bars
    var offset = 2;
    // Append overarching rect
    var overarchingRect = reorderG.append("rect")
      .data([parentPath])
      .attr("x", o.xScale[dimIdx].range()[0]-offset)
      .attr("y", ymin-offset)
      .attr("custom:bandwidthSmall", lastbandwidth)
      .attr("custom:path", parentPath)
      .attr("width", chartWidth+offset)
      .attr("height", ymax+lastbandwidth-ymin+offset)
      .attr("fill", "none")
      .attr("stroke", alreadyOnHighestLevel ? "rgb(120,120,120)" : "rgb(220,220,220)")
      .attr("stroke-width", 2);
    // Append to hoverSelection to delete it when done
    o.hoverSelection.push(overarchingRect);
  };

  chart.stopHoverMouseOut = function() {
    // @desc: When moving the mouse off of a hovered element, remove the 'hoverSelection' and go back to the original state
    o.hoverSelection.forEach(selection => {
      selection
        .remove();
    })
    // Display original rects that were to possibly be removed in full shine again
    d3.selectAll(".barchart-hover-removal-rect")
      .classed("barchart-hover-removal-rect", false)
      .transition()
      .duration(300)
      .attr("opacity", 1);
  }

  chart.digdown = function(selection, dimIdx) {
    // @desc: When clicking while hovering over a category, remove the parent category and flush the 'hoverSelection'
    // Remove parent category
    d3.selectAll(".barchart-hover-removal-rect").remove();
    selection.each(function(data, idx) {
      if (!data || !o.dimensions) {return;}
      // Update barcharts of selected dimension
      update(data, dimIdx);
      // Update baseline button position when digging down
      if (dimIdx==0) createBaselineButtons();
    });
    // Hovering is over now
    o.hoverSelection = [];
  };

  chart.digup = function(selection) {
    // @desc: When clicking while hovering over a category, remove the siblings and display the parent category
    selection.each(function(data, idx) {
      var overarchingRect = o.hoverSelection[0];
      // Dimension index is last character of the parent groups class
      var dimIdx = d3.select(overarchingRect.node().parentElement).attr("id").slice(-1);
      var path = overarchingRect.datum();
      if (path != null) {
        // If there is a level to dig up to, do so
        // Close the corresponding paths in the taxonomy
        o.taxonomies.closeNode(o.dimensions[dimIdx], path);
        // Get new updated yScale
        var reorderG = d3.selectAll("#barchart-reorder-g-"+dimIdx);
        var ytemp = +overarchingRect.attr("y") + (+overarchingRect.attr("height"))/2 - +(+overarchingRect.attr("custom:bandwidthSmall"));
        var heighttemp = +overarchingRect.attr("custom:bandwidthSmall") * 2;
        reorderG.selectAll(".barchart-rect").attr("pointer-events", "none");
        overarchingRect.remove();
        // Mark the rects to be deleted
        var deleteRects = reorderG.selectAll(".barchart-rect")
          .filter(d => JSON.stringify(d.path.slice(0,path.length)) === JSON.stringify(path))
          .attr("class", "barchart-delete-rect");
        // Update barcharts of selected dimension
        update(data, dimIdx);
        // Update baseline button position when digging down
        if (dimIdx === 0) createBaselineButtons();
        // Remove only now after calling 'update' to make it more smooth
        deleteRects
          .transition()
          .duration(500)
          .attr("y", ytemp)
          .attr("height", heighttemp)
          .attr("opacity", 0)
          .on("end", function() {
            d3.select(this).remove();
            reorderG.selectAll(".barchart-rect").attr("pointer-events", "all");
          });
      }
      else {
        // If no higher level to dig up to was available, still remove the overarching rect
        // Make it flash quickly for better visual feedback
        o.hoverSelection.forEach(selection => {
          selection
            .transition().duration(200)
            .attr("opacity", 0)
            .on("end", function() {
              d3.select(this)
                .transition().duration(200)
                .attr("opacity", 1)
                .on("end", function() {
                  d3.select(this)
                    .transition().duration(200)
                    .attr("opacity", 0)
                    .on("end", function() {
                      d3.select(this).remove();
                    })
                })
            })
        })
      }
    });
    // Hovering is over now
    o.hoverSelection = [];
    // Return the updated taxonomies
    return o.taxonomies;
  };

  chart.hoverInfo = function(rect, data) {
    // @desc: Display the count of the current 'rect' as a hover label on top of a background for better readability
    // Get the fill from the parent group
    var fill = d3.select(rect.parentElement).attr("fill");
    rect = d3.select(rect);
    var dimIdx = rect.attr("class").slice(-1);
    var translate = d3.selectAll("#barchart-reorder-g-"+dimIdx).attr("transform");
    // Create text background
    var background = o.svg
      .append("rect")
      .attr("y", rect.attr("y"))
      .attr("fill", d3.select(":root").style("--compenent-bg-color"))
      .attr("opacity", 0.7)
      .attr("transform", translate);
    // Create info text
    var label = o.svg
      .append("text")
      .attr("class", "barchart-hover-info")
      .text(data.end-data.start + " (" + Math.round((data.end-data.start)/o.svg.datum().length*100) +"%)")
      .attr("y", +rect.attr("y")+o.fontSize)
      .attr("fill", fill)
      .attr("transform", translate);
    // Set width of background according to text bounding box
    var bgHeight = d3.select(".barchart-hover-info").node().getBBox().height;
    var bgWidth = d3.select(".barchart-hover-info").node().getBBox().width;
    // Calculate xPos only now that we know the width of the label. If it exceeds the svg width, move it to the left
    var xPos = d3.min([+rect.attr("x")+(+rect.attr("width")), o.w-bgWidth-14]);
    // Adjust the label and background rect depending on the label width
    label
      .attr("x", xPos);
    background
      .attr("x", xPos)
      .attr("height", bgHeight)
      .attr("width", bgWidth)
      .attr("class", "barchart-hover-info");
  };

  chart.stopHoverInfo = function() {
    // @desc: Remove hovering label with the count info
    d3.selectAll(".barchart-hover-info").remove();
  };

  chart.uncheckBox = function(path) {
    // @desc: Classify the filter box corresponding to 'path' as unchecked and adjust its appearance accordingly.
    // Afterwards, remove it by calling 'removeFilterBoxes'
    d3.selectAll(".barcharts-checkbox.box").filter(d => d === path)
      .attr("fill", d3.select(":root").style("--compenent-bg-color"))
      .classed("checked", false);
    d3.selectAll(".barcharts-checkbox.vertLine").filter(d => d === path)
      .attr("stroke", "white")
      .classed("checked", false);
    d3.selectAll(".barcharts-checkbox.horiLine").filter(d => d === path)
      .attr("stroke", "white")
      .classed("checked", false);
    d3.selectAll(".barcharts-highlightbox").filter(d => d === path)
      .classed("checked", false);
    chart.removeFilterBoxes();
  };

  chart.checkBox = function(path) {
    // @desc: Classify the filter box corresponding to 'path' as checked and adjust its appearance accordingly
    d3.selectAll(".barcharts-checkbox.box").filter(d => d === path)
      .attr("fill", "white")
      .classed("checked", true);
    d3.selectAll(".barcharts-checkbox.vertLine").filter(d => d === path)
      .attr("stroke", d3.select(":root").style("--compenent-bg-color"))
      .classed("checked", true);
    d3.selectAll(".barcharts-checkbox.horiLine").filter(d => d === path)
      .attr("stroke", d3.select(":root").style("--compenent-bg-color"))
      .classed("checked", true);
    d3.selectAll(".barcharts-highlightbox").filter(d => d === path)
      .classed("checked", true);
  };

  chart.addFilterBoxes = function(rect, data) {
    //@desc: Display a filter box for the stack given which 'rect' is part of
    // Get the parent group
    var parentG = d3.select(rect.parentElement);
    var dimIdx = parentG.attr("class").slice(-1);
    rect = d3.select(rect);

    // Stroke-width of the rects to be appended
    var strokeWidth = 2
    // Width of the entire barstack corresponding to the current path
    var series = d3.select("#barchart-svg-"+dimIdx)
                  .selectAll(".barchart-rect")
                  .filter(d => d.path === data.path)
                  .data();
    // Where the bar stack starts
    var xStart = o.xScale[dimIdx](d3.min(series, d => d.start));
    var stackWidth = o.xScale[dimIdx](d3.max(series, d => d.end)) - xStart;

    // Append overarching highlight rect
    var filterbox = d3.select(".barchart-filterbox-g.dim-"+dimIdx)
      .selectAll(".barcharts-highlightbox")
      .data([data.path], d => d)
      .enter()
      .append("rect")
      .attr("class", "barcharts-highlightbox dim-"+dimIdx)
      .attr("x", xStart)
      .attr("width", stackWidth)
      .attr("y", +rect.attr("y"))
      .attr("height", +rect.attr("height"))
      .attr("stroke", "white")
      .attr("stroke-width", 2*strokeWidth)
      .attr("fill", "none")
      .attr("opacity", 0)
    filterbox.transition()
      .duration(300)
      .attr("opacity", 1);

    // Append filter rect
    var checkbox = d3.select(".barchart-filterbox-g.dim-"+dimIdx)
      .selectAll(".barcharts-checkbox.box")
      .data([data.path], d => d)
      .enter()
      .append("rect")
      .attr("class", "barcharts-checkbox box dim-"+dimIdx)
      .attr("x", xStart)
      .attr("y", +rect.attr("y")+(+rect.attr("height"))/2-o.checkboxWidth/2)
      .attr("width", o.checkboxWidth)
      .attr("height", o.checkboxWidth)
      .attr("fill", d3.select(":root").style("--compenent-bg-color"))
      .attr("stroke", "white")
      .attr("stroke-width", strokeWidth);
    var vertLine = d3.select(".barchart-filterbox-g.dim-"+dimIdx)
      .selectAll(".barcharts-checkbox.vertLine")
      .data([data.path], d => d)
      .enter()
      .append("line")
      .attr("class", "barcharts-checkbox vertLine dim-"+dimIdx)
      .attr("x1", xStart+o.checkboxWidth/2)
      .attr("x2", xStart+o.checkboxWidth/2)
      .attr("y1", +rect.attr("y")+(+rect.attr("height"))/2-o.checkboxWidth/2+2*strokeWidth)
      .attr("y2", +rect.attr("y")+(+rect.attr("height"))/2+o.checkboxWidth/2-2*strokeWidth)
      .attr("stroke", "white")
      .attr("stroke-width", strokeWidth);
    var horiLine = d3.select(".barchart-filterbox-g.dim-"+dimIdx)
      .selectAll(".barcharts-checkbox.horiLine")
      .data([data.path], d => d)
      .enter()
      .append("line")
      .attr("class", "barcharts-checkbox horiLine dim-"+dimIdx)
      .attr("x1", xStart+2*strokeWidth)
      .attr("x2", xStart+o.checkboxWidth-2*strokeWidth)
      .attr("y1", +rect.attr("y")+(+rect.attr("height"))/2)
      .attr("y2", +rect.attr("y")+(+rect.attr("height"))/2)
      .attr("stroke", "white")
      .attr("stroke-width", strokeWidth);

    // Add events to toggle between selected and unselected state
    d3.select(".barchart-filterbox-g.dim-"+dimIdx)
      .selectAll(".barcharts-checkbox").on("click", function(d) {
                                if (d3.select(this).classed("checked")) {
                                  chart.uncheckBox(d);
                                }
                                else {
                                  chart.checkBox(d);
                                }
                              })
    // Make the mouse cursor change when hovering over the filter box
    d3.select(".barchart-filterbox-g.dim-"+dimIdx)
      .selectAll(".barcharts-checkbox").on("mouseover", function(d) {d3.select(this).style("cursor", "pointer")});
    d3.select(".barchart-filterbox-g.dim-"+dimIdx)
      .selectAll(".barcharts-checkbox").on("mouseout", function(d) {d3.select(this).style("cursor", "default")});
    // Animate the appearance of the checkbox components from right to left
    d3.select(".barchart-filterbox-g.dim-"+dimIdx)
      .selectAll(".barcharts-checkbox")
      .filter(d => d === data.path)
      .transition()
      .duration(300)
      .attr("transform", `translate(${-o.checkboxWidth}, 0)`);
  };

  chart.removeFilterBoxes = function(removeSelected = false) {
    // @desc: Remove the filter boxes. If "removeSelected == false", only the unselected ones get removed.
    // This is usually just a single one, i.e., one which was previously selected but was then clicked, removing
    // its "checked" state (and thereby marking it as "to be deleted") before this function was called

    // Remove the checkbox
    d3.selectAll(".barcharts-checkbox")
      .filter(function() {return !d3.select(this).classed("checked") || removeSelected})
      .transition()
      .duration(300)
      .attr("transform", "translate(0, 0)")
      .on("end", function() {d3.select(this).remove()});
    // Remove the highlight box
    d3.selectAll(".barcharts-highlightbox")
      .filter(function() {return !d3.select(this).classed("checked") || removeSelected})
      .transition()
      .duration(300)
      .attr("opacity", 0)
      .on("end", function() {d3.select(this).remove()});
  };

  chart.addAllToFilter = function() {
    // @desc: Add all categories to the current filter selection and draw filterboxes around them accordingly

    // Store the paths of the current selection and add a filterbox to each
    coveredPaths = [];
    o.svg.selectAll(".barchart-rect").nodes().forEach(rect => {
      var data = d3.select(rect).datum();
      if (!coveredPaths.includes(data.path)) {
        // Add path
        coveredPaths.push(data.path);
        // Draw filterbox
        chart.addFilterBoxes(rect, data);
      }
    });
    // Mark all filterboxes as "checked"
    d3.selectAll(".barcharts-checkbox.box")
      .attr("fill", "white")
      .classed("checked", true);
    d3.selectAll(".barcharts-checkbox.vertLine")
      .attr("stroke", d3.select(":root").style("--compenent-bg-color"))
      .classed("checked", true);
    d3.selectAll(".barcharts-checkbox.horiLine")
      .attr("stroke", d3.select(":root").style("--compenent-bg-color"))
      .classed("checked", true);
    d3.selectAll(".barcharts-highlightbox")
      .classed("checked", true);
  };

  chart.toggleBaselineButtons = function(button) {
    // @desc: Make 'button' the new active baseline button and all others inactive
    d3.selectAll(".button.barchart-baseline")
      .call(fn.deactivateBaselineButton);
    d3.select(button)
      .call(fn.activateBaselineButton);
  };

  chart.updateBaselineButtons = function() {
    // @desc: Update the baseline buttons
    createBaselineButtons();
  };

  chart.hideBaselineButtons = function() {
    // @desc: Make all baseline buttons temporarily unusable
    d3.selectAll(".barchart-baseline.button").transition().duration(300)
        .attr("opacity", 0.2).attr("pointer-events", "none");
  };

  chart.showBaselineButtons = function() {
    // @desc: Make all baseline buttons usable
    d3.selectAll(".barchart-baseline.button").attr("opacity", 1).attr("pointer-events", "all");
  };

  chart.getFilterConstraints = function() {
    // @desc: Retrieve the tagified paths to all categories that are to be kept

    // Eventually store values to filter for and corresponding column headers here
    var filterConstraints = {};

    // To check whether filter result would be empty. In this case, only return null and do not filter
    var emptyFiltering = [];
    for (var i=0; i<o.dimensions.length; i++) {
      // Selected filter paths of the current dimension
      var filterPaths = d3.selectAll(".barcharts-checkbox.box.dim-"+i).data();
      // Collection of the (slightly modified) filter paths
      filterConstraints[o.dimensions[i]] = [];
      // Insert hashtags for unambiguity
      filterPaths.forEach(path =>{
          // Store value to filter for (stringified path array) in output
          // "#" to ensure unambiguity, e.g. between "Violent Attack" and "Violent Attack (No State)"
          filterConstraints[o.dimensions[i]].push(path.reduce((a,b) => a+"#"+b)+"#")
      });
      // Keep track of empty filtering dimensions
      if (filterConstraints[o.dimensions[i]].length === 0) emptyFiltering.push(o.dimensions[i]);
    }
    // Check whether to actually filter or not. (not if filter results would be empty)
    if (emptyFiltering.length > 0) {
      alert('Please select at least one category to filter for for the attribute(s) "'+emptyFiltering.reduce((a,b) => a+', '+b)+'".');
      return null;
    }
    else {
      // To check if the baseline key was removed
      var baselineCheck = [...new Set(d3.selectAll(".barcharts-checkbox.box.dim-0").data().map(d => d[0]))];
      // If current baseline primaryVal was filtered out, select the first remaining one
      if (!baselineCheck.includes(o.baselineKey)) o.baselineKey = baselineCheck[0];

      return filterConstraints;
    }
  };

  chart.toggleDimensionFilterboxes = function(turn) {
    // @desc: Show or hide the filterboxes next to the axis titles based on 'turn'
    var boxes = d3.selectAll(".barchart-checkbox-all");
    if (turn === "on") boxes.transition("show").duration(300).style("display", "block");
    else boxes.transition("hide").duration(300).style("display", "none");
  };

  chart.reorderDimensions = function() {
    // @desc: Calculate the separability/split score for all axes and reorder them accordingly via transation
    var splitScores = [];
    // Get the scores
    for (var dimIdx=1; dimIdx<o.dimensions.length; dimIdx++) {
      splitScores.push([dimIdx, fn.getSplitScore(dimIdx)]);
    }
    // Order scores
    splitScores.sort((a, b) => b[1] - a[1]);
    // Reorder axes
    for (var dimIdx=1; dimIdx<o.dimensions.length; dimIdx++) {
      var newDimIdx = splitScores.map(d => d[0]).indexOf(dimIdx) + 1;
      var shiftDistance = newDimIdx - dimIdx;
      d3.selectAll("#barchart-reorder-g-"+dimIdx)
        .transition()
        .duration(500)
        .attr("transform", `translate(${shiftDistance * (chartWidth + o.margin.inner)}, 0)`);
    }
  };

  // Allow external setting and getting of the options 'o'
  chart.w = function(value) {
      if (!arguments.length) return o.w;
      o.w = value;
      return chart;
  };
  chart.h = function(value) {
      if (!arguments.length) return o.h;
      o.h = value;
      return chart;
  };
  chart.dimensions = function(value) {
      if (!arguments.length) return o.dimensions;
      o.dimensions = value;
      return chart;
  };
  chart.primaryVals = function(value) {
      if (!arguments.length) return o.primaryVals;
      o.primaryVals = value;
      o.baselineKey = o.primaryVals[0];
      return chart;
  };
  chart.colors = function(value) {
      if (!arguments.length) return o.colors;
      o.colors = value;
      return chart;
  };
  chart.margin = function(value) {
      if (!arguments.length) return o.margin;
      o.margin = value;
      return chart;
  };
  chart.filterfunctions = function(value) {
      if (!arguments.length) return o.filterfunctions;
      o.filterfunctions = value;
      return chart;
  };
  chart.baselineKey = function(value) {
      if (!arguments.length) return o.baselineKey;
      o.baselineKey = value;
      return chart;
  };
  chart.taxonomies = function(value) {
      if (!arguments.length) return o.taxonomies;
      o.taxonomies = value;
      return chart;
  };
  chart.labelVisibility = function(value) {
      if (!arguments.length) return o.labelVisibility;
      o.labelVisibility = value;
      return chart;
  };
  chart.hierarchyVisibility = function(value) {
      if (!arguments.length) return o.hierarchyVisibility;
      o.hierarchyVisibility = value;
      return chart;
  };
  
  return chart;
}
