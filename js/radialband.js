function radialBand() {
  // Options
  var sumArr, sumObj, svg, minDate, maxDate, formatDate, binnedData, bandData, stack, series,
  xScale, yScale, arc, arcMask, stackband, maskband, _oldmax, dateScale, brushStartAngle, brushEndAngle, colors;
  _oldmax = 0;

  var o = {
    w: 0,
    h: 0,
    primaryAttr: "dataset",
    primaryVals: ["A", "G", "GT", "S"],
    colors: {"A": "rgb(75,174,121)", "G": "rgb(0,162,219)", "GT": "rgb(255,152,35)", "S": "rgb(183,41,177)"},
    nrBins: 50,
    rInner: 10,
    bandHeight: 60,
    paddingOuter: 0.1,
    labelSpace: 0,
    normalize: false,
    firstMax: 0,
    labelBGColor: "rgb(70, 70, 70)",
    buttonBGColor: "rgb(50, 50, 50)",
    filterfunctions: null,
    taxonomies: null
  }

  // Helper functions
  // Sum over all values of an array
  sumArr = arr => arr.reduce((a,b)=> a+b);
  // Sum over all values of an object
  sumObj = obj => sumArr(Object.values(obj));

  var colHeader = function(dimName, lvl) {
    if (["dataset", "parameters"].includes(dimName)) {
      return dimName;
    }
    else {
      return dimName+"_level_"+lvl;
    }
  };

  function getBandData(binnedData) {
    var out = [];
    // Go over each bin's data and count the frequency of each dataset
    for (var bin = 0; bin < o.nrBins+1; bin++) {
      // Get depth of primary attribute if it is defined (not the case for match
      // view because primary attr then is "parameters", not included in tax, only used for uniform color)
      var depthPrimaryAttr = o.taxonomies[o.primaryAttr] ? o.taxonomies[o.primaryAttr]["depth"] : null;
      out[bin] = _.countBy(binnedData[bin], d => d[colHeader(o.primaryAttr, depthPrimaryAttr)]);
    }
    // Add the counts from the last micro bin with only max date to the last valid bin
    Object.keys(out[o.nrBins-1]).forEach(key => { out[o.nrBins-1][key] += (out[o.nrBins][key] || 0) });
    // Remove last micro bin
    out.pop();
    // For each bin, check if there are any datasets not contained at all
    // (in which case they are not listed) and list them with a count of 0
    for (var bin=0; bin<o.nrBins; bin++) {
      _.each(out, d => {for(var i=0; i<o.primaryVals.length; i++) d[o.primaryVals[i]] = ((!d[o.primaryVals[i]]) ? 0 : d[o.primaryVals[i]])})
    }
    return out;
  }

  // END for import from gradientlines

  // For custom interpolation when transitioning between the different bars
  var arc2Tween = function(d, indx) {
    var c = Array.from(d);
    c["data"] = d["data"];
    c[1] = c[0];
    //  function that returns values between 'this._current' and 'd' given an input between 0 and 1
    var interp = d3.interpolate(this._current, d);    // this will return an interpolater
    var interpScale = d3.interpolate(_oldmax, d3.max(bandData, e => sumObj(e)));

    this._current = d;                    // update this._current to match the new value
    // returns a function that attrTween calls with a time input between 0-1;
    //  0 as the start time, and 1 being the end of the animation
    return function(t) {
       // use the time to get an interpolated value (between this._current and d)
      var tmp = interp(t);
      // Rescale for smooth scale change
      tmp[0] = interpScale(1)/interpScale(t) * tmp[0];
      tmp[1] = interpScale(1)/interpScale(t) * tmp[1];
      // pass this new information to the accessor function to calculate the path points.
      return arc(tmp, indx);
    }
  };

  var vectorDistance = function(p, q) {
    ;
  };

  var getLabelMarker = function(x, y, distOuterLineEnd) {
    var c = [o.w/2, o.h/2];
    var distActual = Math.sqrt(Math.pow((x-c[0]),2) + Math.pow((y-c[1]),2));
    return [[c[0] + (x-c[0]) * o.rInner/distActual, c[1] + (y-c[1]) * o.rInner/distActual], // Inner point of marker
            [c[0] + (x-c[0]) * distOuterLineEnd/distActual, c[1] + (y-c[1]) * distOuterLineEnd/distActual]] // Outer point of marker
  };

  // From https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
  // To calculate distance between point and line segment
  var sqr = function(x) { return x * x }
  var dist2 = function(v, w) { return sqr(v[0] - w[0]) + sqr(v[1] - w[1]) }
  var distToSegmentSquared = function(p, v, w) {
    var l2 = dist2(v, w);
    if (l2 == 0) return dist2(p, v);
    var t = ((p[0] - v[0]) * (w[0] - v[0]) + (p[1] - v[1]) * (w[1] - v[1])) / l2;
    t = Math.max(0, Math.min(1, t));
    return dist2(p, [v[0] + t * (w[0] - v[0]),
                     v[1] + t * (w[1] - v[1])]);
  };
  var distToSegment = function(p, v, w) { return Math.sqrt(distToSegmentSquared(p, v, w)); };

  var createStaticLabels = function() {
    // Just place them 0, 1/3, 2/3, 1
    minAngle = dateScale(minDate);
    maxAngle = dateScale(maxDate);
    chart.createLabel(o.w/2 + o.rInner*Math.cos(-minAngle+Math.PI/2), o.h/2 - o.rInner*Math.sin(-minAngle+Math.PI/2), "static-min", "static");
    chart.createLabel(o.w/2 + o.rInner*Math.cos(-0+Math.PI/2), o.h/2 + o.rInner*Math.sin(-0+Math.PI/2), "static-half", "static");
    chart.createLabel(o.w/2 + o.rInner*Math.cos(-maxAngle+Math.PI/2), o.h/2 - o.rInner*Math.sin(-maxAngle+Math.PI/2), "static-max", "static");

    createFilterButtons({
      x1: o.w/2 + (o.rInner+o.labelSpace)*Math.cos(-minAngle+Math.PI/2),
      y1: o.h/2 - (o.rInner+o.labelSpace)*Math.sin(-minAngle+Math.PI/2),
      x2: o.w/2 + (o.rInner+o.labelSpace)*Math.cos(-maxAngle+Math.PI/2),
      y2: o.h/2 - (o.rInner+o.labelSpace)*Math.sin(-maxAngle+Math.PI/2)
    });
  };

  const createFilterButtons = function(pos) {
    const adjustWidth = 70;
    const adjustHeight = 40;
    const cornerRadius = 10;
    const eps = 3;
    const textOffset = 3;
    const okWidth = 30;
    const labelWidth = 90;

    // Adjust
    svg.append('rect')
      .datum({side: 'right'})
      .attr('class', 'tempmap-button-rect adjust right')
      .attr('id', 'adjust-right')
      .attr('x', pos.x1 - adjustWidth/2 - eps)
      .attr('y', pos.y1-adjustHeight - eps)
      .attr('width', adjustWidth)
      .attr('height', adjustHeight)
      .attr('rx', cornerRadius)
      .attr('ry', cornerRadius);

    svg.append('rect')
      .datum({side: 'left'})
      .attr('class', 'tempmap-button-rect adjust left')
      .attr('id', 'adjust-left')
      .attr('x', pos.x2 - adjustWidth/2 + eps)
      .attr('y', pos.y2-adjustHeight - eps)
      .attr('width', adjustWidth)
      .attr('height', adjustHeight)
      .attr('rx', cornerRadius)
      .attr('ry', cornerRadius);

    // ok
    svg.append('rect')
      .datum({side: 'right', mode: 'ok'})
      .attr('class', 'tempmap-button-rect adjust-mode right')
      .attr('id', 'ok-right')
      .attr('x', pos.x1 - labelWidth/2 - eps)
      .attr('y', pos.y1 + adjustHeight/2 - eps)
      .attr('width', okWidth)
      .attr('height', adjustHeight/2)
      .attr('rx', cornerRadius/2)
      .attr('ry', cornerRadius/2);

    svg.append('rect')
      .datum({side: 'left', mode: 'ok'})
      .attr('class', 'tempmap-button-rect adjust-mode left')
      .attr('id', 'ok-left')
      .attr('x', pos.x2 - labelWidth/2 + eps)
      .attr('y', pos.y2 + adjustHeight/2 - eps)
      .attr('width', okWidth)
      .attr('height', adjustHeight/2)
      .attr('rx', cornerRadius/2)
      .attr('ry', cornerRadius/2);

    // cancel
    svg.append('rect')
      .datum({side: 'right', mode: 'cancel'})
      .attr('class', 'tempmap-button-rect adjust-mode right')
      .attr('id', 'cancel-right')
      .attr('x', pos.x1 - labelWidth/2 + okWidth - eps)
      .attr('y', pos.y1 + adjustHeight/2 - eps)
      .attr('width', labelWidth - okWidth)
      .attr('height', adjustHeight/2)
      .attr('rx', cornerRadius/2)
      .attr('ry', cornerRadius/2);

    svg.append('rect')
      .datum({side: 'left', mode: 'cancel'})
      .attr('class', 'tempmap-button-rect adjust-mode left')
      .attr('id', 'cancel-left')
      .attr('x', pos.x2 - labelWidth/2 + okWidth + eps)
      .attr('y', pos.y2 + adjustHeight/2 - eps)
      .attr('width', labelWidth - okWidth)
      .attr('height', adjustHeight/2)
      .attr('rx', cornerRadius/2)
      .attr('ry', cornerRadius/2);

    // Button general
    svg.selectAll('.tempmap-button-rect')
      .attr('fill', o.buttonBGColor)
      .attr('stroke-width', 1)
      .attr('stroke', o.labelBGColor)
      .on('mouseover', function() {d3.select(this).attr('fill', o.labelBGColor)})
      .on('mouseout', function() {d3.select(this).attr('fill', o.buttonBGColor)})

    // Adjust
    svg.append('text')
      .attr('class', 'button-text right')
      .attr('id', 'adjust-right-text')
      .attr('x', pos.x1 - eps)
      .attr('y', pos.y1 - adjustHeight/2 - eps - textOffset)
      .text('Adjust');
        
    svg.append('text')
      .attr('class', 'button-text left')
      .attr('id', 'adjust-left-text')
      .attr('x', pos.x2 + eps)
      .attr('y', pos.y2 - adjustHeight/2 - eps - textOffset)
      .text('Adjust');
      
    // manually
    svg.append('text')
      .attr('class', 'button-text right')
      .attr('id', 'manually-right-text')
      .attr('x', pos.x1 - eps)
      .attr('y', pos.y1 - eps - 2*textOffset)
      .text('manually');
        
    svg.append('text')
      .attr('class', 'button-text left')
      .attr('id', 'manually-left-text')
      .attr('x', pos.x2 + eps)
      .attr('y', pos.y2 - eps - 2*textOffset)
      .text('manually');

    // ok
    svg.append('text')
      .attr('class', 'button-text adjust-mode right')
      .attr('id', 'ok-right-text')
      .attr('x', pos.x1 - labelWidth/2 + okWidth/2 - eps)
      .attr('y', pos.y1 + adjustHeight - eps - 2*textOffset)
      .text('ok');

    svg.append('text')
      .attr('class', 'button-text adjust-mode left')
      .attr('id', 'ok-left-text')
      .attr('x', pos.x2 - labelWidth/2 + okWidth/2 + eps)
      .attr('y', pos.y2 + adjustHeight - eps - 2*textOffset)
      .text('ok');

    // cancel
    svg.append('text')
      .attr('class', 'button-text adjust-mode right')
      .attr('id', 'cancel-right-text')
      .attr('x', pos.x1 - labelWidth/2 + okWidth + (labelWidth - okWidth)/2 - eps)
      .attr('y', pos.y1 + adjustHeight - eps - 2*textOffset)
      .text('cancel');

    svg.append('text')
      .attr('class', 'button-text adjust-mode left')
      .attr('id', 'cancel-left-text')
      .attr('x', pos.x2 - labelWidth/2 + okWidth + (labelWidth - okWidth)/2 + eps)
      .attr('y', pos.y2 + adjustHeight - eps - 2*textOffset)
      .text('cancel');

    // Text general
    svg.selectAll('.button-text')
      .attr('text-anchor', 'middle')
      .attr('fill', 'rgb(120, 120, 120)')
      .attr('pointer-events', 'none');
    
    // Input
    svg.append('foreignObject')
      .attr('class', 'adjust-mode right')
      .attr('id', 'foreign-right')
      .attr('x', pos.x1 - 100/2 - eps)
      .attr('y', pos.y1 - eps)
      .attr('width', 100)
      .attr('height', 20)
      .append("xhtml:body")
      .style('margin', 0)
      .style('padding', 0)
      .append('div')
      .append('input')
      .attr('id', 'tempmap-input-static-min')
      .attr('size', 10)
      .attr('type', 'text')
      .attr('placeholder', d3.selectAll('#radialband-static-min-text').text())

    svg.append('foreignObject')
      .attr('class', 'adjust-mode left')
      .attr('id', 'foreign-left')
      .attr('x', pos.x2 - 100/2 + eps)
      .attr('y', pos.y2 - eps)
      .attr('width', 100)
      .attr('height', 20)
      .append("xhtml:body")
      .style('margin', 0)
      .style('padding', 0)
      .append('div')
      .append('input')
      .attr('id', 'tempmap-input-static-max')
      .attr('size', 10)
      .attr('type', 'text')
      .attr('placeholder', d3.selectAll('#radialband-static-max-text').text())

    d3.selectAll('.adjust-mode').style('display', 'none');
    
    /*
      <foreignObject x="10" y="10" width="100" height="150">
      <div xmlns="http://www.w3.org/1999/xhtml">
      <input></input>
          </div>
  </foreignObject>
  */
  };

  // Define the rendering
  function chart(selection) {
    selection.each(function(data, idx) {

      if (!data) {return;}

      // Select the svg element
      svg = d3.select(this);
      o.w = svg.attr("width");
      o.h = svg.attr("height");

      // First date in table
      minDate = d3.min(data, function(d) { return d.date; });
      // Last date in table
      maxDate = d3.max(data, function(d) { return d.date; });
      // Put data into temporal bins, however, one extra bin for max date
      binnedData = _.groupBy(data, d => o.filterfunctions.binNr(d.date));
      // Place empty arrays for all bins that are empty and, hence, undefined in binnedData
      for (var i=0; i<o.nrBins; i++) binnedData[i] = binnedData[i] ? binnedData[i] : [];
      bandData = getBandData(binnedData);

      console.log(bandData);
      stack = d3.stack().keys(o.primaryVals);
      series = stack(bandData);

      // To be replaced by arc (maybe?)
      xScale = d3.scaleBand()
        .domain(d3.range(bandData.length))
        .range([0, 2*Math.PI])
        .paddingInner(0)
        .paddingOuter(o.paddingOuter);

      yScale = d3.scaleLinear()
        .domain([0, d3.max(bandData, d => sumObj(d))]) // Function simply sums over all values in each object
        .range([o.rInner, o.rInner+o.bandHeight]);

      // Only necessary later for temporal brushing and hover labels
      // Set up scale to extract the label text from the marker position
      // Calculate offset in the beginning and the end of the band
      var offset = 2*Math.PI/(o.nrBins + 2*o.paddingOuter) * 2*o.paddingOuter;
      // Set up scale
      dateScale = d3.scaleTime()
        .domain([minDate, maxDate])
        .range([offset/2, 2*Math.PI-offset/2]);

      // To extract shortened versions of the date
      formatDate = d3.timeFormat("%Y-%m-%d")

      arc = d3.arc()
        .innerRadius(d => yScale(d[0]))
        .outerRadius(d => yScale(d[1]))
        .startAngle((d,i) => xScale(i))
        .endAngle((d,i) => xScale(i) + xScale.bandwidth())
        .padAngle(0);

      // Mask for hover highlighting of the bins
      arcMask = d3.arc()
        .innerRadius(o.rInner)
        .outerRadius(o.rInner + o.bandHeight)
        .startAngle((d,i) => xScale(i))
        .endAngle((d,i) => xScale(i) + xScale.bandwidth())
        .padAngle(0);

      // Arc for the surrounding frame of the radialband
      arcFrameLine = d3.arc()
        .innerRadius(o.rInner + 1)
        .outerRadius(o.rInner + o.bandHeight + 1)
        .padAngle(0);

      // Arc for the band registering the hovering to create labels
      arcFrameBand = d3.arc()
        .innerRadius(o.rInner + o.bandHeight)
        .outerRadius(o.rInner + o.bandHeight + o.labelSpace)
        .padAngle(0);

      // Arc for drawing the brush area over the radialband
      arcBrush = d3.arc()
        .innerRadius(o.rInner)
        .outerRadius(o.rInner + o.bandHeight)
        .padAngle(0);

      frameLine = svg.append("g")
        .attr("id", "arc-frameline");

      frameLine.append("path")
          .attr("class", "radialband-arc-frameline")
          .attr("transform", "translate(" + (o.w/2) + ", " + (o.h/2) + ")")
          .attr("fill", "none")
          .attr("stroke", o.labelBGColor)
          .attr("stroke-width", "1")
          .attr("d", arcFrameLine({startAngle:xScale(0)-0.01, endAngle:xScale(bandData.length-1)+xScale.bandwidth()+0.01}));

      frameBand = svg.append("g")
        .attr("id", "radialband-arc-frameband");
      frameBand.append("path")
          .attr("class", "radialband-arc-frameband")
          .attr("transform", "translate(" + (o.w/2) + ", " + (o.h/2) + ")")
          .attr("opacity", 0)
          .attr("d", arcFrameBand({startAngle:xScale(0)-0.01, endAngle:xScale(bandData.length-1)+xScale.bandwidth()+0.01}))
          .style("cursor", "crosshair");

      // Add a group for each row of data
      stackband = svg.append("g") // Append overarching group for structure
                      .attr("id", "radialband-stackband-group")
                      .selectAll("#radialband-stackband-group") // To append groups for each dataset's bars
                      .data(series) // Add data of each dataset's bar information
                      .join("g") // Bind data of each dataset's bars to the corresponding four groups
                      .attr("class", "radialband-original-gs")
                      .attr("transform", "translate(" + (o.w/2) + ", " + (o.h/2) + ")")
                      .attr("fill", d => o.colors[d.key]); // Set dataset specific color to each group
      stackband.selectAll("path") // Inside each group
                        .data(d => d)
                        .enter().append("path")
                          .attr("class", "radialband-original-paths")
                          .attr("d", arc);

      // Append text label giving the max amount of events per bin
      minAngle = dateScale(minDate);
      maxAngle = dateScale(maxDate);
      // Rotate position of label a bit further by distAngle
      var distAngle = 2;
      var x = o.w/2 + (o.rInner+o.bandHeight/2) * Math.cos(-maxAngle + Math.PI/2 - distAngle/180*Math.PI);
      var y = o.h/2 - (o.rInner+o.bandHeight/2) * Math.sin(-maxAngle + Math.PI/2 - distAngle/180*Math.PI);
      // Outer g to combine rotation and translation
      svg.append("g").attr("class", "radialband-axis-label-g").attr("transform", `translate(${x}, ${y})`)
        .append("text")
        .attr("class", "radialband-axis-labels")
        .attr("x", 0)
        .attr("y", 0)
        .attr("text-anchor", "middle")
        .attr("transform", `rotate(${180 - (-maxAngle+Math.PI/2) * 180/Math.PI})`)
        .text("Max. "+d3.max(bandData, e => sumObj(e)))
        .attr("fill", "rgb(160,160,160)")

      createStaticLabels();
      chart.createLabel(null, null, "hover-moving", "hover");

      // As the last element, append the mask band used to highlight individual bins
      maskband = svg.append("g")
        .attr("id", "radialband-arc-mask-group")
        .selectAll("#radialband-arc-mask-group")
          .data(Array.from(Array(o.nrBins).keys()))
          .enter()
          .append("path")
            .attr("class", "radialband-arc-mask")
            .attr("d", arcMask)
            .attr("transform", "translate(" + (o.w/2) + ", " + (o.h/2) + ")")
            .attr("opacity", 0)
            .attr("fill", d3.select(":root").style("--compenent-bg-color"));

        o.firstMax = d3.max(bandData, e => sumObj(e));
    })
  };

  chart.highlight = function(filterFn) {
    d3.selectAll(".radialband-arc-mask").filter(filterFn)
      .attr("opacity", 0.5);
  }

  chart.lowlight = function() {
    d3.selectAll(".radialband-arc-mask")
      .attr("opacity", 0);
  }

  chart.brush = function(filterFn, oldmax, adjustData = false, dateRange = null) {
    _oldmax = oldmax;

    if (dateRange !== null) {
      // This does not update the dateScale and binScale yet
      minDate = dateRange[0];
      maxDate = dateRange[1];
      svg.selectAll(`#tempmap-input-static-min`).property('placeholder', formatDate(minDate));
      svg.selectAll(`#tempmap-input-static-max`).property('placeholder', formatDate(maxDate));
      var minBin = o.filterfunctions.binNr(minDate);
      var maxBin = o.filterfunctions.binNr(maxDate);
      var data = [];
      // First, get the most recent data
      _.each(binnedData, bin => bin.forEach(d => data.push(d)))
      // Then, update the temporal scale
      dateScale.domain(dateRange);
      o.filterfunctions.binScale.domain(dateRange);
      // Now reassign the recent data to the 20 bins
      binnedData = _.groupBy(data, d => o.filterfunctions.binNr(d.date));
      // Place empty arrays for all bins that are empty and, hence, undefined in binnedData
      for (var i=0; i<o.nrBins; i++) binnedData[i] = binnedData[i] ? binnedData[i] : [];

      d3.select("#radialband-static-min-text")
        .text(formatDate(minDate))
      d3.select("#radialband-static-half-text")
        .text(formatDate(dateScale.invert((dateScale.range()[0]+dateScale.range()[1])/2))) // lower middle of circle
      d3.select("#radialband-static-max-text")
        .text(formatDate(maxDate))
    }

    var binnedDataModified = [];
    for (var bin=0; bin<o.nrBins; bin++) {
      if (typeof binnedData[bin] !== "undefined") {
        binnedDataModified[bin] = _.filter(binnedData[bin],
          d => filterFn(d));
      }
    }

    if (adjustData) {
      // In case of brushing, reduce the overall data to the brushed one
      binnedData = binnedDataModified;
    }

    // Filter the existing binnedData to contain only the selected elements
    bandData = getBandData(binnedDataModified);

    var series = stack(bandData);

    yScale.domain([0, d3.max(bandData, d => sumObj(d))]) // Function simply sums over all values in each object

    arc.innerRadius(d => yScale(d[0]))
       .outerRadius(d => yScale(d[1]));

    stackband
       .data(series)
       .join(
            enter => enter.append("g"),
            update => update,
            exit => exit.remove()
          )
       .attr("class", "radialband-updated-gs") // working till here
       .attr("transform", "translate(" + (o.w/2) + ", " + (o.h/2) + ")")
       .attr("fill", d => o.colors[d.key])
       .selectAll("path")
         .data(d => d)
         .join(
              enter => enter,
              update => update,
              exit => exit.remove()
            )
            .transition().duration(500)
            .attrTween("d", arc2Tween);

    // Update label about max height of the stackband
    svg.selectAll(".radialband-axis-labels")
      .transition().duration(500)
      .text("Max. "+d3.max(bandData, e => sumObj(e)));

    return d3.max(bandData, e => sumObj(e));
  };

  chart.lowlightSpatial = function() {chart() //actually not that easy since it takes selection as input
  };

  chart.createLabel = function(x, y, id, clas) {
    // Create everything for the hover labels, but don't show it yet
    // label circle
    svg.append("line")
      .attr("class", `radialband-${clas}-label`)
      .attr("id", `radialband-${id}-marker`)
      .attr("opacity", 0)
      .attr("stroke", o.labelBGColor)
      .style('pointer-events', 'none');
    // label rect
    svg.append("rect")
      .attr("class", `radialband-${clas}-label`)
      .attr("id", `radialband-${id}-rect`)
      .attr("width", 100)
      .attr("height", 20)
      .attr("opacity", 0)
      .attr("fill", o.labelBGColor)
      .style('pointer-events', 'none');
    // label text
    svg.append("text")
      .attr("class", `radialband-${clas}-label`)
      .attr("id", `radialband-${id}-text`)
      .attr("text-anchor", "middle")
      .attr("font-family", "sans-serif")
      .attr("fill", "white")
      .attr("opacity", 0)
      .style('pointer-events', 'none');

    if (x!=null && y!=null) chart.moveLabel(x, y, id);
  };

  var getAngle = function(p0, p1) {
    var angle = Math.atan2(-(p1[1] - p0[1]), // Factor -1 due to flipped y-axis
                             p1[0] - p0[0]);
    // Perform rotation adjustments since mathematical counting works differently
    // than in our case
    angle = angle<0 ? -angle + Math.PI/2 : Math.PI/2 - angle;
    angle = angle<0 ? 2*Math.PI + angle : angle; // modulo 2*PI

    return angle;
  };

  chart.moveLabel = function(x, y, id) {
    // Set up an initial outer line length that gets replaced later however
    var distOuterLineEnd = o.rInner + o.bandHeight + o.labelSpace/10;
    // Calculate the beginning and end point of the label line
    var [p0,p1] = getLabelMarker(x, y, distOuterLineEnd);
    // Get the properties of the box surrounding the label text
    var rect = d3.select(`#radialband-${id}-rect`);
    wR = rect.attr("width");
    hR = rect.attr("height");
    // Calculate the initial center of the box surrounding the label text (later replaced, too)
    var c = [p1[0], p1[1]];
    // Calculate how much distance is missing for the box to be fully outside of the radialband
    var distMissing = distOuterLineEnd - Math.min(distToSegment([o.w/2,o.h/2], [c[0]-wR/2, c[1]-hR/2], [c[0]+wR/2, c[1]-hR/2]),
                           distToSegment([o.w/2,o.h/2], [c[0]-wR/2, c[1]-hR/2], [c[0]-wR/2, c[1]+hR/2]),
                           distToSegment([o.w/2,o.h/2], [c[0]-wR/2, c[1]+hR/2], [c[0]+wR/2, c[1]+hR/2]),
                           distToSegment([o.w/2,o.h/2], [c[0]+wR/2, c[1]-hR/2], [c[0]+wR/2, c[1]+hR/2]));

    // Calculate how far the box needs to be moved in x and y direction to not
    // overlap with the radialband anymore
    dx = (p1[0] - o.w/2) * distMissing/distOuterLineEnd;
    dy = (p1[1] - o.h/2) * distMissing/distOuterLineEnd;

    // Position marker line
    d3.select(`#radialband-${id}-marker`)
      .raise()
      .attr("x1", p0[0])
      .attr("y1", p0[1])
      .attr("x2", p1[0] + dx)
      .attr("y2", p1[1] + dy)
      .attr("opacity", 1);

    // Position marker background
    d3.select(`#radialband-${id}-rect`)
      .raise()
      .attr("x", p1[0] + dx - wR/2)
      .attr("y", p1[1] + dy - hR/2)
      .attr("opacity", 1);

    // Calculate current angle of the label marker line to extract the date
    var angle = getAngle(p0, p1);

    var minPlus1Date = dateScale.domain()[0];
    minPlus1Date.setDate(minPlus1Date.getDate() + 1);
    var angleOffset = (dateScale(minPlus1Date)-dateScale(minDate)) / 2;

    // Position label text
    d3.select(`#radialband-${id}-text`)
      .raise()
      .attr("x", p1[0] + dx)
      .attr("y", p1[1] + dy + 6)
      .text(formatDate(dateScale.invert(angle+angleOffset)))
      .attr("opacity", 1);
  };

  chart.createBrush = function() {
    chart.createLabel(d3.event.layerX, d3.event.layerY, "brush-fixed", "hover");
    // Set up an initial outer line length that gets replaced later however
    var distOuterLineEnd = o.rInner + o.bandHeight + o.labelSpace/10;
    // Calculate the beginning and end point of the label line
    var [p0,p1] = getLabelMarker(d3.event.layerX, d3.event.layerY, distOuterLineEnd);

    var beginningLine = d3.select("#radialband-brush-fixed-marker");
    // Mask for hover highlighting of the bins

    brushStartAngle = getAngle(p0, p1);

    arcBrush
      .startAngle(brushStartAngle)
      .endAngle(brushStartAngle);

    var brushBand = svg.append("g")
      .attr("id", "radialband-arc-brushband");
    brushBand.append("path")
        .attr("class", "radialband-arc-brushband")
        .attr("transform", "translate(" + (o.w/2) + ", " + (o.h/2) + ")")
        .attr("opacity", 0.3)
        .attr("stroke", "white")
        .attr("stroke-width", 1)
        .attr("d", arcBrush);
  }

  chart.updateBrush = function() {
    // Set up an initial outer line length that gets replaced later however
    var distOuterLineEnd = o.rInner + o.bandHeight + o.labelSpace/10;
    // Calculate the beginning and end point of the label line
    var [p0,p1] = getLabelMarker(d3.event.layerX, d3.event.layerY, distOuterLineEnd);

    brushEndAngle = getAngle(p0, p1);

    arcBrush.endAngle(brushEndAngle)

    d3.select(".radialband-arc-brushband")
        .attr("d", arcBrush);
  };

  chart.removeBrush = function() {
    d3.select("#radialband-arc-brushband").remove();

    var minPlus1Date = dateScale.domain()[0];
    minPlus1Date.setDate(minPlus1Date.getDate() + 1);
    var angleOffset = (dateScale(minPlus1Date)-dateScale(minDate)) / 2;

    var firstDate = dateScale.invert(Math.min(brushStartAngle+angleOffset, brushEndAngle+angleOffset));
    var lastDate = dateScale.invert(Math.max(brushStartAngle+angleOffset, brushEndAngle+angleOffset));
    // Deal with the fact that all events are listed at 00:00:00 o clock
    firstDate.setHours(0,0,0,0);
    lastDate.setHours(23,59,59,999);

    return [firstDate, lastDate];
  };

  chart.brushData = function(dateRange) {
    // Redundant?
    // Update temporal scale
    minDate = dateRange[0];
    maxDate = dateRange[1];
    dateScale.domain(dateRange);
    svg.selectAll(`#tempmap-input-static-min`).property('placeholder', formatDate(minDate));
    svg.selectAll(`#tempmap-input-static-max`).property('placeholder', formatDate(maxDate));
  }

  chart.removeLabel = function(clas) {
    d3.selectAll(`.radialband-${clas}-label`)
      .transition()
      .duration(100)
      .attr("opacity", 0);
  };

  chart.enterManualAdjustment = function(side) {
    svg.selectAll(`.adjust-mode.${side}`)
      .style('display', 'block');
    svg.selectAll(`.adjust.${side}`)
      .style('display', 'none');
  };
  
  chart.exitManualAdjustment = function(mode, side) {
    const inputNode = svg.selectAll(`#tempmap-input-static-${side === 'left' ? 'max' : 'min'}`);

    if (mode === 'cancel') {
      svg.selectAll(`.adjust-mode.${side}`)
        .style('display', 'none');
      svg.selectAll(`.adjust.${side}`)
        .style('display', 'block');

      inputNode.property('value', '');
      return null;
    }
    else {
      const newDateString = inputNode.property('value').slice(); // slice to create a copy

      // Check if newDate is valid
      const parseTime = d3.timeParse('%Y-%m-%d');
      const newDate = parseTime(newDateString);

      if (newDate == null) {
        alert('Please enter a valid date format of the shape %Y-%m-%d, like 1999-07-23.');
        return null;
      }
      else if (newDate < minDate || newDate > maxDate) {
        alert(`You have entered the date ${formatDate(newDate)}. Please select a date that lies between ${formatDate(minDate)} and ${formatDate(maxDate)}.`);
        return null;
      }
      else {
        inputNode.property('value', '');
        inputNode.property('placeholder', newDateString);

        svg.selectAll(`.adjust-mode.${side}`)
          .style('display', 'none');
        svg.selectAll(`.adjust.${side}`)
          .style('display', 'block');

        [minDate, maxDate] = side === 'left' ? [minDate, newDate] : [newDate, maxDate];
          
        return [minDate, maxDate]; 
      }
    }
  };

  chart.w = function(value) {
      if (!arguments.length) return o.w;
      o.w = value;
      return chart;
  };
  chart.h = function(value) {
      if (!arguments.length) return o.h;
      o.h = value;
      return chart;
  };
  chart.primaryVals = function(value) {
      if (!arguments.length) return o.primaryVals;
      o.primaryVals = value;
      return chart;
  };
  chart.primaryAttr = function(value) {
      if (!arguments.length) return o.primaryAttr;
      o.primaryAttr = value;
      return chart;
  };
  chart.colors = function(value) {
      if (!arguments.length) return o.colors;
      o.colors = value;
      return chart;
  };
  chart.nrBins = function(value) {
      if (!arguments.length) return o.nrBins;
      o.nrBins = value;
      return chart;
  };
  chart.rInner = function(value) {
      if (!arguments.length) return o.rInner;
      o.rInner = value;
      return chart;
  };
  chart.bandHeight = function(value) {
      if (!arguments.length) return o.bandHeight;
      o.bandHeight = value;
      return chart;
  };
  chart.paddingOuter = function(value) {
      if (!arguments.length) return o.paddingOuter;
      o.paddingOuter = value;
      return chart;
  };
  chart.labelSpace = function(value) {
      if (!arguments.length) return o.labelSpace;
      o.labelSpace = value;
      return chart;
  };
  chart.firstMax = function(value) {
      if (!arguments.length) return o.firstMax;
      o.firstMax = value;
      return chart;
  };
  chart.filterfunctions = function(value) {
      if (!arguments.length) return o.filterfunctions;
      o.filterfunctions = value;
      return chart;
  };
  chart.taxonomies = function(value) {
      if (!arguments.length) return o.taxonomies;
      o.taxonomies = value;
      return chart;
  };

  return chart;
}
