function miniMap() {
  var o, svg, projection, tScale, path, arcBase, arcBrush; // options, (helper)functions

  o = {
    sidelength: 0,
    fill: "rgb(50, 50, 50)",
    highfill: "rgb(100, 100, 100)",
    stroke: "black",
    strokewidth: 1,
    scale: 0,
    centerLon: 0,
    centerLat: 0,
    center: [0,0],
    paddingOuter: 0,
    minDate: null,
    maxDate: null,
    filterfunctions: null
  }

  // Define the rendering
  function chart(selection) {
    selection.each(function(data, idx) {

      if (!data) {return;}

      // LOCAL VARIABLES
      // Select the svg element
      svg = d3.select(this);

      // D3 HELPER FUNCTIONS
      // Define projection path generator, using an appropriate projection
      // To center the projection correctly, calculate the centers here
      projection = d3.geoMercator()
            .translate(o.center)
            .center([o.centerLon,o.centerLat])
            .scale(o.scale);


      path = d3.geoPath()
             .projection(projection);

      tScale = d3.scaleTime()
        .domain([o.minDate, o.maxDate])
        .range([o.paddingOuter/2, 2*Math.PI-o.paddingOuter/2]);

      // DATA BINDING
      // Bind data and create one path per GeoJSON feature
      svg.selectAll("#minimap-boundary")
        .data([0])
        .join(
          enter => enter.append("g")
          .attr("id", "minimap-boundary")
          .selectAll("path")
          .data(data)
          .join("path")
            .attr("class", "minimap-path")
            .attr("d", path)
            .attr("fill", o.fill)
            .attr("stroke", "none")
        );
      chart.update();
    });
  }

  chart.update = function() {
    // LAYOUT FUNCTIONS
    // Arc for the temporal brushing
    arcBrush = d3.arc()
      .innerRadius(o.sidelength/2-1)
      .outerRadius(o.sidelength/2+30+1) // that odd scale factor times c of tempmap
      .padAngle(0)
      .startAngle(tScale(o.minDate))
      .endAngle(tScale(o.maxDate));


    var brushBand = svg.selectAll("#minimap-arc-brushband-g")
      .data([0])
      .join(
        enter => enter.append("g")
        .attr("id", "minimap-arc-brushband-g")
      );

    brushBand.selectAll(".minimap-arc-brushband")
      .data([0])
      .join(
        enter => enter.append("path")
          .attr("class", "minimap-arc-brushband")
          .attr("stroke", "white")
          .attr("opacity", 0.8)
          .attr("fill", "none")
          .attr("stroke-width", 3)
          .attr("transform", "translate(" + (o.center[0]) + ", " + (o.center[1]) + ")")
      )
      .attr("d", arcBrush);

    // Brushing rect gets its own svg to lie in front of the canvases
    svgBrush = d3.selectAll(".svg-plot-minimap-brush-rect");
    // To allow clipping of the brushing rect
    svgBrush.selectAll("#minimap-clip")
      .data([0])
      .join(
        enter => enter.append("clipPath")       // define a clip path
          .attr("id", "minimap-clip") // give the clipPath an ID
          .append("circle")            // shape it as an ellipse
          .attr("cx", o.center[0])            // position the x-centre
          .attr("cy", o.center[1])            // position the y-centre
          .attr("r", 0.95 * o.sidelength/2)
      )

    // Draw brushing rect
    svgBrush.selectAll("#minimap-brushrect")
      .data([0])
      .join(
        enter => enter.append("rect")
          .attr("id", "minimap-brushrect")
          .attr("fill", "none")
          .attr("stroke", "white")
          .attr("stroke-width", 3)
          .attr("opacity", 0.8)
          .attr("clip-path","url(#minimap-clip)")
      )
      .attr("x", o.center[0] - 0.9 * o.sidelength/2)
      .attr("y", o.center[1] - 0.9 * o.sidelength/2)
      .attr("width", 0.9 * o.sidelength)
      .attr("height", 0.9 * o.sidelength);
  }

  chart.brushSpatial = function(bbox) {
    var upperleft = projection([bbox.minLon, bbox.maxLat]);
    var lowerright = projection([bbox.maxLon, bbox.minLat]);
    d3.select("#minimap-brushrect")
        .attr("x", upperleft[0])
        .attr("y", upperleft[1])
        .attr("width", lowerright[0] - upperleft[0])
        .attr("height", lowerright[1] - upperleft[1]);
  }

  chart.brushTemporal = function(dateRange) {
    o.minDate = dateRange[0];
    o.maxDate = dateRange[1];
    arcBrush
      .startAngle(tScale(o.minDate))
      .endAngle(tScale(o.maxDate));

    d3.select(".minimap-arc-brushband")
        .attr("d", arcBrush);
  };

  chart.sidelength = function(value) {
      if (!arguments.length) return o.sidelength;
      o.sidelength = value;
      return chart;
  };
  chart.fill = function(value) {
      if (!arguments.length) return o.fill;
      o.fill = value;
      return chart;
  };
  chart.stroke = function(value) {
      if (!arguments.length) return o.stroke;
      o.stroke = value;
      return chart;
  };
  chart.strokewidth = function(value) {
      if (!arguments.length) return o.strokewidth;
      o.strokewidth = value;
      return chart;
  };
  chart.scale = function(value) {
      if (!arguments.length) return o.scale;
      o.scale = value;
      return chart;
  };
  chart.centerLon = function(value) {
      if (!arguments.length) return o.centerLon;
      o.centerLon = value;
      return chart;
  };
  chart.centerLat = function(value) {
      if (!arguments.length) return o.centerLat;
      o.centerLat = value;
      return chart;
  };
  chart.center = function(value) {
      if (!arguments.length) return o.center;
      o.center = value;
      return chart;
  };
  chart.paddingOuter = function(value) {
      if (!arguments.length) return o.paddingOuter;
      o.paddingOuter = value;
      return chart;
  };
  chart.minDate = function(value) {
      if (!arguments.length) return o.minDate;
      o.minDate = value;
      return chart;
  };
  chart.maxDate = function(value) {
      if (!arguments.length) return o.maxDate;
      o.maxDate = value;
      return chart;
  };
  chart.filterfunctions = function(value) {
      if (!arguments.length) return o.filterfunctions;
      o.filterfunctions = value;
      return chart;
  };

  return chart;
}
